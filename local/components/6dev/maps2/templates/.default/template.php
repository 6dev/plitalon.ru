				
				
					<div class="mapwr">
						<div id="map-canvas" style="height: 600px;width: 100%;"></div>
					</div>
					<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBs6YNpAJgw4SVXQmo5oVHJZfWBf5grHCw"></script>
		      <script>

						var map;
						var egglabs = new google.maps.LatLng(<?=$arParams['GOOGLE'];?>);
						var mapCoordinates = new google.maps.LatLng(<?=$arParams['GOOGLE'];?>);


						var markers = [];
						var image = new google.maps.MarkerImage(
						  '<?=SITE_TEMPLATE_PATH;?>/images/map-m.png',
						  new google.maps.Size(49,59),
						  new google.maps.Point(0,0),
						  new google.maps.Point(42,56)
						  );

						function addMarker() 
						{
						  markers.push(new google.maps.Marker({
						  position: egglabs,
						  raiseOnDrag: false,
						  icon: image,
						  map: map,
						  draggable: false
						  }));
						      
						}

						function initialize() {
						  var mapOptions = {
						    backgroundColor: "#f5f5f5",
						    zoom: 15,
						    disableDefaultUI: true,
						    center: mapCoordinates,
						    mapTypeId: google.maps.MapTypeId.ROADMAP,
						    styles: [
						      {
						        "featureType": "landscape.natural",
						        "elementType": "geometry.fill",
						        "stylers": [
						          { "color": "#f5f5f5" }
						        ]
						      },
						      {
						            "featureType": "landscape.man_made",
						            "stylers": [
						              { "color": "#ffffff" },
						              { "visibility": "off" }
						            ]
						      },
						      {
						            "featureType": "water",
						            "stylers": [
						               { "color": "#e5e5e5" },
						              { "saturation": 0 }
						            ]
						      },
						      {
						            "featureType": "road.arterial",
						            "elementType": "geometry",
						            "stylers": [
						              { "color": "#161C28" }
						            ]
						      }
						     ,{
						            "elementType": "labels.text.stroke",
						            "stylers": [
						              { "visibility": "off" }
						            ]
						      }
						      ,{
						          "elementType": "labels.text",
						          "stylers": [
						            { "color": "#000000" }
						          ]
						        }
						      ,
						        {
						          "featureType": "transit.station.bus",
						          "stylers": [
						            { "visibility": "off" }
						          ]
						        },
						      {
						        "featureType": 'transit.station',
						        "stylers": [{"visibility": "off"}]
						      },
						        {
						        "featureType": 'transit',
						        "elementType": 'geometry',
						        "stylers": [{"visibility": "off"}]
						      },
						      {
						        "featureType": "road.highway",
						        "elementType": "labels.icon",
						        "stylers": [
						          { "visibility": "off" }
						        ]
						      },
						      {
						        "featureType": "poi",
						        "stylers": [
						          { "visibility": "off" }
						        ]
						      },
						      {
						        "featureType": 'road',
						        "elementType": 'geometry',
						        "stylers": [{"color": '#c9c9c9'}]
						      },
						      {
						        "featureType": 'road',
						        "elementType": 'geometry.stroke',
						        "stylers": [{"color": '#c9c9c9'}]
						      },
						      {
						        "featureType": 'road',
						        "elementType": 'labels.text.fill',
						        "stylers": [{"color": '#000000'}]
						      },
						      {
						        "featureType": 'road.highway',
						        "elementType": 'geometry',
						        "stylers": [{"color": '#c9c9c9'}]
						      },
						      {
						        "featureType": 'road.highway',
						        "elementType": 'geometry.stroke',
						        "stylers": [{"color": '#c9c9c9'}]
						      },
						      {
						        "featureType": 'road.highway',
						        "elementType": 'labels.text.fill',
						        "stylers": [{"color": '#000000'}]
						      }
						    
						    ]
						    
						  };


							map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
							addMarker();
							}
							google.maps.event.addDomListener(window, 'load', initialize);

						</script>
					