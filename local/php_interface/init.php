<?

define('CATALOG_IBLOCK_ID',18);
define('CATALOG_IBLOCK_TYPE','1c_catalog');

?><?

use Bitrix\Sale;
use Bitrix\Sale\Basket;
use Bitrix\Sale\Discount\Gift;
use Bitrix\Sale\Fuser;
use \Bitrix\Main\Mail\Event;
use \Bitrix\Main\IO;
use Bitrix\Currency\CurrencyManager;
use Bitrix\Main\Context;
use Bitrix\Sale\Order;
use Bitrix\Main\Loader;
use Bitrix\Sale\Compatible\DiscountCompatibility;
use Bitrix\Sale\Delivery\Services\Manager as DeliveryManager;
use Bitrix\Sale\PaySystem\Manager as PaySystemManager;

global $DB;

$DB->Query("TRUNCATE TABLE b_catalog_vat"); // удалять НДС при каждом чихе

// Агент
COption::SetOptionString("main", "agents_use_crontab", "Y");

function getDeliveryPriceForProduct($siteId, $userId, $personTypeId, $deliveryId, $paySystemId, $userCityId)
{
	global $SECTION_WODA;
    $result = null;

    \Bitrix\Main\Loader::includeModule('catalog');
    \Bitrix\Main\Loader::includeModule('sale');
	
	$arResult=array();
	
	$products=array();
	 $result = null;
	
$basket2 = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite()); // Текущая корзина

	foreach ($basket2 as $basketItem2) {
	
	
if ($basketItem2->getProductId()>0) {
	
	
	
$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM");
$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID,"INCLUDE_SUBSECTIONS"=>"Y","ID"=>$basketItem2->getProductId());

$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
while($ob = $res->GetNextElement())
{	


    $products[] = 
        array(
            'PRODUCT_ID' => $basketItem2->getProductId(),
            'QUANTITY'   => $basketItem2->getQuantity(),
            // 'NAME'       => 'Товар 1', 
            // 'PRICE' => 500,
            // 'CURRENCY' => 'RUB',
        
    );
	
	}

	
} // Если корзина не пустая

	} // Перебор старой корзины
	
	

	
    /** @var \Bitrix\Sale\Basket $basket */
    $basket = \Bitrix\Sale\Basket::create($siteId);
    foreach ($products as $product) {
        $item = $basket->createItem("catalog", $product["PRODUCT_ID"]);
        unset($product["PRODUCT_ID"]);
        $item->setFields($product);
    }

    /** @var \Bitrix\Sale\Order $order */
    $order = \Bitrix\Sale\Order::create($siteId, $userId);
    $order->setPersonTypeId($personTypeId);
    $order->setBasket($basket);

    /** @var \Bitrix\Sale\PropertyValueCollection $orderProperties */
    $orderProperties = $order->getPropertyCollection();
    /** @var \Bitrix\Sale\PropertyValue $orderDeliveryLocation */
    $orderDeliveryLocation = $orderProperties->getDeliveryLocation();
    $orderDeliveryLocation->setValue($userCityId); // В какой город "доставляем" (куда доставлять).

    /** @var \Bitrix\Sale\ShipmentCollection $shipmentCollection */
    $shipmentCollection = $order->getShipmentCollection();

    $delivery = \Bitrix\Sale\Delivery\Services\Manager::getObjectById($deliveryId);
    /** @var \Bitrix\Sale\Shipment $shipment */
    $shipment = $shipmentCollection->createItem($delivery);

    /** @var \Bitrix\Sale\ShipmentItemCollection $shipmentItemCollection */
    $shipmentItemCollection = $shipment->getShipmentItemCollection();
    /** @var \Bitrix\Sale\BasketItem $basketItem */
    foreach ($basket as $basketItem) {
        $item = $shipmentItemCollection->createItem($basketItem);
        $item->setQuantity($basketItem->getQuantity());
    }

    /** @var \Bitrix\Sale\PaymentCollection $paymentCollection */
    $paymentCollection = $order->getPaymentCollection();
    /** @var \Bitrix\Sale\Payment $payment */
    $payment = $paymentCollection->createItem(
        \Bitrix\Sale\PaySystem\Manager::getObjectById($paySystemId)
    );
    $payment->setField("SUM", $order->getPrice());
    $payment->setField("CURRENCY", $order->getCurrency());

    // $result = $order->save(); // НЕ сохраняем заказ в битриксе - нам нужны только применённые "скидки" и "правила корзины" на заказ.
    // if (!$result->isSuccess()) {
    //     //$result->getErrors();
    // }

    $deliveryPrice = $order->getDeliveryPrice();
    if ($deliveryPrice === '') {
        $deliveryPrice = null;
    }
    $result = $deliveryPrice;
	
	
	$arResult=$result;
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	

    return $arResult;
}





class ArtfactorOrder{

  protected $basket = false;
  protected $order = false;
  protected $profile = false;
  protected $arDeliveryServiceAll = false;

  public function __construct(){
    $this->$siteID = \Bitrix\Main\Context::getCurrent()->getSite();
    $this->order = Bitrix\Sale\Order::create($this->$siteID, $this->getUserId()); 
    $this->order->setField('STATUS_ID', Bitrix\Sale\OrderStatus::getInitialStatus());
  }

  protected function getUserId(){
    global $USER;

    $userId = $USER->GetID();
    if (!$userId)
    {
      $userId = CSaleUser::GetAnonymousUserID();
    }
    return $userId;
  }

  function getProfileID(){
    $dbUserProfiles = CSaleOrderUserProps::GetList(
      array('DATE_UPDATE' => 'DESC'),
      array(
        'USER_ID' => $this->order->getUserId()
      )
    );
    if ($arUserProfiles = $dbUserProfiles->GetNext())
    {
      return $arUserProfiles['ID']; // Возвращаем ID первого профиля. Мне нужен именно первый, но можно переписать так, чтобы брал все
    }
    return null;
  }

  function getProfile(){
    if(!$this->profile){
      $profileID = $this->getProfileID();
      $this->profile = Bitrix\Sale\OrderUserProperties::getProfileValues($profileID); // Получаем значения профиля
    }
    return $this->profile;
  }

  protected function setBasketToOrder(){
    if(!$this->basket){
      $this->basket = Bitrix\Sale\Basket::loadItemsForFUser(\CSaleBasket::GetBasketUserID(), $this->$siteID)->getOrderableItems();
      $this->order->setBasket($this->basket);
    }
  }

  protected function setProfileToOrder(){
    $profile = $this->getProfile();
    $propertyCollection = $this->order->getPropertyCollection();
    $propertyCollection->setValuesFromPost(array('PROPERTIES' => $profile), array()); //Устанавливает значения данного профиля в заказ
  }

  protected function setShipmentToOrder(){
    $shipmentCollection = $this->order->getShipmentCollection();
    $this->initShipment($this->order); 
  }

  function getDeliveries(){
    if(!$this->arDeliveryServiceAll){
      $this->setProfileToOrder();
      $this->setBasketToOrder();
      $this->setShipmentToOrder();
      $currentShipment = $this->getCurrentShipment($this->order);
      $this->arDeliveryServiceAll = Bitrix\Sale\Delivery\Services\Manager::getRestrictedObjectsList($currentShipment);
    }
    return $this->arDeliveryServiceAll;
  }

  /**
   * Initialization of shipment object. Filling with basket items.
   *
   * @param Order $order
   * @return Shipment
   * @throws Main\ArgumentTypeException
   * @throws Main\NotSupportedException
   */
  public function initShipment(Bitrix\Sale\Order $order)
  {
    $shipmentCollection = $order->getShipmentCollection();
    $shipment = $shipmentCollection->createItem();
    $shipmentItemCollection = $shipment->getShipmentItemCollection();
    $shipment->setField('CURRENCY', $order->getCurrency());

    /** @var Sale\BasketItem $item */
    foreach ($order->getBasket() as $item)
    {
      /** @var Sale\ShipmentItem $shipmentItem */
      $shipmentItem = $shipmentItemCollection->createItem($item);
      $shipmentItem->setQuantity($item->getQuantity());
    }

    return $shipment;
  }

  public function getCurrentShipment(Bitrix\Sale\Order $order)
  {
    foreach ($order->getShipmentCollection() as $shipment)
    {
      if (!$shipment->isSystem())
        return $shipment;
    }

    return null;
  }

  
}

function GetMassa($ves)
{
	$t=' г.';
	if ($ves<1000) $rv=$ves;
	if ($ves>1000) { $rv=round($ves/1000,2); $t=' кг.';}
//	if ($ves>1000000) { $rv=round($ves/1000000,2); $t=' т.';}
	
	return number_format($rv, 2, '.', ' ').$t; 
}


function NumberWordEndingsEx($num, $arEnds = false) {
   $lang = LANGUAGE_ID;
   if ($arEnds===false) {
      $arEnds = array('ов', 'ов', '', 'а');
   }
   if ($lang=='ru') {
      if (strlen($num)>1 && substr($num, strlen($num)-2, 1)=='1') {
         return $arEnds[0];
      } else {
         $c = IntVal(substr($num, strlen($num)-1, 1));
         if ($c==0 || ($c>=5 && $c<=9)) {
            return $arEnds[1];
         } elseif ($c==1) {
            return $arEnds[2];
         } else {
            return $arEnds[3];
         }
      }
   } elseif ($lang=='en') {
      if (IntVal($num)>1) {
         return 's';
      }
      return '';
   } else {
      return '';
   }
}




function NumberWordEndingsEx2($num, $arEnds = false) {
   $lang = LANGUAGE_ID;
   if ($arEnds===false) {
      $arEnds = array('й', 'й', 'я', 'и');
   }
   if ($lang=='ru') {
      if (strlen($num)>1 && substr($num, strlen($num)-2, 1)=='1') {
         return $arEnds[0];
      } else {
         $c = IntVal(substr($num, strlen($num)-1, 1));
         if ($c==0 || ($c>=5 && $c<=9)) {
            return $arEnds[1];
         } elseif ($c==1) {
            return $arEnds[2];
         } else {
            return $arEnds[3];
         }
      }
   } elseif ($lang=='en') {
      if (IntVal($num)>1) {
         return 's';
      }
      return '';
   } else {
      return '';
   }
}

function NewImageR($img, $width, $height, $waterk=false)
{
	
	
	

		
	
if ($img<=0) $img=78965; //  Номер картинки с нет


	
	if ($img>0)
	{

$rsFile = CFile::GetByID($img);		
$arFile = $rsFile->Fetch();


	if (($width<=0) || ($height<=0)) {
		
		$height=$arFile['HEIGHT'];
		$width=$arFile['WIDTH'];
		
	}










if (($arFile['WIDTH']>$width) || ($arFile['HEIGHT']>$height)) {

if (($arFile['WIDTH']/$arFile['HEIGHT'])>=($width/$height))
{
	$width_new=$width;
	$height_new=($arFile['HEIGHT']*$width/$arFile['WIDTH']);
	$top=($height-$height_new)/2;
	$left=($width-$width_new)/2;
} else 
{
	$height_new=$height;
	$width_new=($arFile['WIDTH']*$height/$arFile['HEIGHT']);
	$left=($width-$width_new)/2;
	$top=($height-$height_new)/2;
}
} else
{
$width_new =$arFile['WIDTH'];
$height_new = $arFile['HEIGHT'];
$top=($height-$height_new)/2;
$left=($width-$width_new)/2;
}

if ($arFile['CONTENT_TYPE']=="image/png") $fu=$_SERVER["DOCUMENT_ROOT"].'/upload/resize_cache/id'.$img.'h'.$height_new.'_w'.$width_new.'hn_'.$height.'_wn'.$width.'.png';
if ($arFile['CONTENT_TYPE']=="image/jpeg") $fu=$_SERVER["DOCUMENT_ROOT"].'/upload/resize_cache/id'.$img.'h'.$height_new.'_w'.$width_new.'hn_'.$height.'_wn'.$width.'.jpg';
if (!file_exists($fu)) {

$image_p = imagecreatetruecolor($width, $height);
$white = imagecolorallocate($image_p, 255, 255, 255);
imagefill($image_p , 0, 0, $white);

if ($arFile['CONTENT_TYPE']=="image/jpeg") $image = imagecreatefromjpeg($_SERVER["DOCUMENT_ROOT"].CFile::GetPath($img));

// Сохраняем jpg версию

if ($arFile['CONTENT_TYPE']=="image/png") {

$image = imagecreatefrompng($_SERVER["DOCUMENT_ROOT"].CFile::GetPath($img));
$bg = imagecreatetruecolor(imagesx($image), imagesy($image));
imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
imagealphablending($bg, TRUE);
imagecopy($bg, $image, 0, 0, 0, 0, imagesx($image), imagesy($image));
imagedestroy($image);
$quality = 50; // 0 = worst / smaller file, 100 = better / bigger file 
imagejpeg($bg,$fu,$quality);

$image = imagecreatefromjpeg($fu);

} 


imagecopyresampled($image_p, $image, $left, $top, 0, 0, $width_new, $height_new, $arFile['WIDTH'], $arFile['HEIGHT']);

if ($waterk==true) {
	

$watermark = imagecreatefrompng($_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH.'/images/PLITALON_LOGO.png');
imagesavealpha($watermark, false); 

if ($arFile['WIDTH']>135) {

imagecopy($image_p, $watermark, $left+$width_new-135, $top+$height_new-17, 0, 0, 135, 17); } else {

imagecopy($image_p, $watermark, $width-135, $top+$height_new-17, 0, 0, 135, 17);
	
}

}





imagejpeg($image_p,$fu, 100);


}

		
		return str_replace($_SERVER["DOCUMENT_ROOT"],'',$fu);
		
	
		
		
		
		
		
		
		
		
		
	}
}

 
function custom_mail($to, $subject, $message, $additional_headers=''){
	
$t=explode(',',$to);	

foreach ($t as $to ) {

	
	require_once($_SERVER['DOCUMENT_ROOT'].'/local/php_interface/mail/PHPMailerAutoload.php');
	$mail = new PHPMailer;
	$mail->SMTPDebug = false;
	$mail->isSMTP();
	$mail->CharSet  = 'UTF-8';
	$mail->setLanguage('ru');
	
	$cd=explode("\n",$message);
	$cd=$cd[0];
	
	$f=fopen($_SERVER['DOCUMENT_ROOT'].'/mails/'.md5(time()+$subject).'-old.txt','w');
	fwrite($f,$message);
	fclose($f);
	
	$message2=explode('Content-Transfer-Encoding: 8bit',$message);
	
if (count($message2)>2) {
	$message=explode($cd,$message2['2']);
	$message=$message['0'];
}
	
	$f=fopen($_SERVER['DOCUMENT_ROOT'].'/mails/'.md5(time()+$subject).'.txt','w');
	fwrite($f,$message);
	fclose($f);
	

	
	
  	// Set mailer to use SMTP
	$mail->Host = 'smtp.mail.ru';  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true; // Enable SMTP authentication
	$mail->Username = 'info@plitalon.ru'; // SMTP username
	$mail->Password = 'plitalonsale'; // SMTP password
	$mail->SMTPSecure = 'ssl'; // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 465;
	$mail->From = 'info@plitalon.ru';
	$mail->FromName = 'info@plitalon.ru';
	$mail->isHTML(false); // убрать html
	$mail->Subject = $subject;
	$mail->msgHTML($message);
	$mail->addAddress($to);
	if(!$mail->send()) {
//		echo $mail->ErrorInfo;
	}
	$mail->clearAddresses();
	$mail->ClearCustomHeaders();
}
}

?>
