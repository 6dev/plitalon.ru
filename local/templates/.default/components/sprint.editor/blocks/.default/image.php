<? /** @var $block array */ ?><?
$image = Sprint\Editor\Blocks\Image::getImage(
    $block, [
    'width'  => 1024,
    'height' => 768,
    'exact'  => 0,
    //'jpg_quality' => 75
]
);

?><? if ($image): ?>
    <a href="<?=$block['file']['ORIGIN_SRC']?>" data-lightbox="<?=$block['file']['ID']?>" class="imgun" style="margin-bottom: .5rem; display: block;">
        <div class="sp-image imgunwr">
            <img alt="<?= $image['DESCRIPTION'] ?>" src="<?= $image['SRC'] ?>">
        </div>
    </a>
<? endif; ?>
