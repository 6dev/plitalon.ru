<? if ($arParams['ACTIVE']!='N') {
?>
<div class="videowr">
<?
	foreach ($arResult['ITEMS'] as $key => $arItem) {
		
		if ($arItem['PROPERTY_IMG_VALUE']!=''){
			$img=$arItem['PROPERTY_IMG_VALUE'];
		} else{
			$img=$arItem['MODULE_IMG'];
		}	

		?>	

			<div class="el el1">
				<a href="javascript:void(0);" data-toggle="modal" data-target="#myModal_<?=$arItem['ID']; ?>" rel="nofollow noreferrer noopener">
					<div class="wrimg">
						<img src="<?=$img;?>" alt="<?=$arItem['NAME']; ?>">
					</div>
				</a>

				<div class="modal fade bs-example-modal-lg" id="myModal_<?=$arItem['ID']; ?>" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel"><?=$arItem['NAME']; ?></h4>
							</div>
							<div class="modal-body">
								<div class="embed-responsive embed-responsive-16by9">
									<iframe class="embed-responsive-item" src="<? echo $arItem['PROPERTY_LINK_VALUE']; ?>"></iframe>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>


		<?	

	}
?>
</div>
<?
}
?>