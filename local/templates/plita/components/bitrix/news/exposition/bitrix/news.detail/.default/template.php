<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="allbrands">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1 class="title2">
						<span>
							<?=$arResult["NAME"];?>
						</span>
					</h1>
				</div>
			</div>
		</div>
</section>

	<section class="card">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="code_card">
						<div class="links">
						
<?
$next=''; $back='';

$ida=array();

$arFilter = Array("IBLOCK_ID"=>$arResult['IBLOCK_ID'], "ACTIVE"=>"Y","DETAIL_PAGE_URL");
$res = CIBlockElement::GetList(Array('ACTIVE_FROM'=>"ASC","SORT"=>"ASC","ID"=>'ASC'), $arFilter, false, Array("nPageSize"=>10000), $arSelect);
while($ob = $res->GetNextElement())
{
 $arFields = $ob->GetFields();
 $ida[]=$arFields;
}

$i=0;
 foreach ($ida as $j=>$a)
 {
	 if (($arResult['ID']==$a['ID']) && ($arResult['ID']!=$a[$j+1]['ID'])&& ($arResult['ID']!=$a[$j-1]['ID']))
	 { 
		 if ($ida[$j+1]['DETAIL_PAGE_URL']!=$arResult['DETAIL_PAGE_URL']) $next=$ida[$j+1]['DETAIL_PAGE_URL'];
		 if ($ida[$j-1]['DETAIL_PAGE_URL']!=$arResult['DETAIL_PAGE_URL']) $back=$ida[$j-1]['DETAIL_PAGE_URL'];
	 }
	 $i=$i+1;
 }
 

?>						
						
					    	<a href="<? if ($back!='') echo $back;?><? if ($back=='') echo '#back';?>" class="lprev<? if ($back!='') echo ' active';?>">Пред.</a>
							<a href="<? if ($next!='') echo $next;?><? if ($back=='') echo '#next';?>" class="lnext<? if ($next!='') echo ' active';?>">След.</a>
							
							
							
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="exposition">
		<div class="container">
            <?
            $APPLICATION->IncludeComponent(
                "sprint.editor:blocks",
                ".default",
                Array(
                    "ELEMENT_ID" => $arResult["ID"],
                    "IBLOCK_ID" => $arResult["IBLOCK_ID"],
                    "PROPERTY_CODE" => "BLOCKS",
                ),
                $component,
                Array(
                    "HIDE_ICONS" => "Y"
                )
            );


//            if (!empty($arResult['PROPERTIES']['SHOW_BLOCKS']['VALUE'])) {
//
//            } else {
//            ?>
<!--			<div class="row">-->
<!--				<div class="col-md-6">-->
<!--					<p>-->
<!--						<b>--><?//=$arResult["~DETAIL_TEXT"];?>
<!--							-->
<!--						</b>-->
<!--					</p>-->
<!--					<p>-->
<!--						--><?//=$arResult["PROPERTIES"]["TEXT2"]["~VALUE"]["TEXT"];?>
<!--					</p>-->
<!--				</div>-->
<!--				<div class="col-md-6">-->
<!--					<p>-->
<!--						--><?//=$arResult["PROPERTIES"]["TEXT3"]["~VALUE"]["TEXT"];?>
<!--					</p>-->
<!--				</div>-->
<!--				<div class="sliderexpo imgun clearfix">-->
<!--				-->
<?//
//
// $res = CIBlockElement::GetProperty(12, $arResult['ID'], "sort", "asc", array("CODE" => "PICTURES"));
//    while ($ob = $res->GetNext())
//    {
//
//	// Собираем данные по картинке
//
//$resE = CIBlockElement::GetByID($ob['VALUE']);
//$ar_resE = $resE->GetNext();
//
//$ipropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues(13, $ar_resE["ID"]);
//$iprop = $ipropValues->getValues();
//
//// print_r($iprop);
//
//if ($ar_resE['DETAIL_PICTURE']>0) {
//
//	?>
<!--				-->
<!--					<a href="--><?//=CFile::GetPAth($ar_resE['DETAIL_PICTURE']);?><!--" data-lightbox="image-1" class="col-sm-4">-->
<!--						<div class="imgunwr">-->
<!--							<img src="--><?//=CFile::GetPAth($ar_resE['PREVIEW_PICTURE']);?><!--" alt="">-->
<!--						</div>-->
<!--					</a>-->
<!--					-->
<!--	--><?// } ?><!--	-->
<!---->
<?// } ?><!--				-->
<!--					-->
<!--				-->
<!--				</div>-->
<!--			</div>-->
<!--			<div class="row">-->
<!--				<div class="col-sm-12">-->
<!--					<p class="cardtabtitle">-->
<!--						<span>-->
<!--						--><?//=$arResult["PROPERTIES"]["TITLE2"]['~VALUE'];?>
<!--						</span>-->
<!--					</p>-->
<!--					<p>-->
<!--						--><?//=$arResult["PROPERTIES"]["TEXT4"]["~VALUE"]["TEXT"];?>
<!--					</p>-->
<!--				</div>-->
<!--			</div>-->
<!--			<div class="row">-->
<!--				<div class="imgun clearfix">-->
<!--					<div class="col-sm-5">-->
<!--						<p>-->
<!--							--><?//=$arResult["PROPERTIES"]["TEXT5"]["~VALUE"]["TEXT"];?>
<!--						</p>-->
<!--						-->
<!---->
<!--						-->
<!--						<ul>-->
<!--						-->
<!--						--><?//=$arResult["PROPERTIES"]["TEXT6"]["~VALUE"]["TEXT"];?>
<!--						-->
<!--						-->
<!--							-->
<!--						</ul>-->
<!--					</div>-->
<!--					<div class="col-sm-7">-->
<!---->
<?// if ($arResult['DETAIL_PICTURE']['ID']>0) { ?>
<!--						<a href="--><?//=$arResult['DETAIL_PICTURE']['SRC'];?><!--" class="imgunwr" data-lightbox="image-1">-->
<!--					-->
<!--							<img src="--><?//=$arResult['DETAIL_PICTURE']['SRC'];?><!--" alt="--><?//=$arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'];?><!--" title="--><?//=$arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'];?><!--">-->
<!--						</a>-->
<!---->
<?// } ?>
<!--					</div>-->
<!--				</div>-->
<!--			</div>-->
<!--            --><?// } ?>
		</div>
	</section>