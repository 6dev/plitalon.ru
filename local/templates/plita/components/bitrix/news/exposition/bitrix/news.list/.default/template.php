<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="material">
		<div class="container">
			<div class="row">
                <div class="col-md-12">
                    <h1 class="title2">
						<span>
							<?=$APPLICATION->GetTitle();?>
						</span>
                    </h1>
                </div>
				<div class="col-md-12">
					<div class="flex">

					
	<?foreach($arResult["ITEMS"] as $arItem) {

// 

if ($arItem['PREVIEW_PICTURE']['SRC']=='') 
{
// 423 353

$file = CFile::ResizeImageGet(78965, array('width'=>423, 'height'=>353), BX_RESIZE_IMAGE_EXACT, true);                
$arItem['PREVIEW_PICTURE']['SRC']=$file['src'];


} else {

$file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE']['ID'], array('width'=>423, 'height'=>353), BX_RESIZE_IMAGE_EXACT, true);                
$arItem['PREVIEW_PICTURE']['SRC']=$file['src'];


}
		
	

	?>				
					
						<div class="el">
							<div class="elwr">
								<div class="eli">

<a href="<?=$arItem['DETAIL_PAGE_URL'];?>">


<? if ($arItem['PREVIEW_PICTURE']['SRC']!='') { ?>									<img src="<?=$arItem['PREVIEW_PICTURE']['SRC'];?>" title="<?=$arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'];?>" alt="<?=$arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_ALT'];?>">




<? } ?>
</a>
									<p class="eldate">
										<?=str_replace('.','/',$arItem['ACTIVE_FROM']);?>
									</p>
								</div>
								<div class="elval">
									<p class="tit">
										<?=$arItem['NAME'];?>
									</p>
									<p>
										<?=$arItem['PREVIEW_TEXT'];?>
									</p>
									<a href="<?=$arItem['DETAIL_PAGE_URL'];?>" class="matmore">
										Подробнее
									</a>
								</div>
							</div>
						</div>
						
	<? } ?>

						
					</div>
				</div>
			</div>
		</div>
	</section>




<script>

	
	function setHi() {
		
var mx=0;
$('.material').find('.flex').find('.el').each(function(i,elem) {
	
if (mx<$(this).find('.elwr').height()) mx=$(this).find('.elwr').height();
	
});

console.log('максимальная высота: '+mx);

$('.material').find('.flex').find('.el').find('.elwr').each(function(i,elem) {

$(this).height(mx);

});
	}


$(document).ready(function() {
	
	setTimeout(setHi, 1000);
	
});
	
</script>



 <?if($arParams["DISPLAY_BOTTOM_PAGER"]) { ?>




	<br /><?=$arResult["NAV_STRING"]?>


<? } ?>