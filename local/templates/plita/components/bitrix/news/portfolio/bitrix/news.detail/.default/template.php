<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(true);
$APPLICATION->AddChainItem($arResult['NAME'], $arResult['DETAIL_PAGE_URL']);
// $next='#';
// $back='#';
// $arFilter = Array("IBLOCK_ID"=>$arResult['IBLOCK_ID'], "ACTIVE"=>"Y",">ID"=>$arResult['ID'],"DETAIL_PAGE_URL");
// $res = CIBlockElement::GetList(Array('ACTIVE_FROM'=>"ASC"), $arFilter, false, Array("nPageSize"=>50), $arSelect);
// while($ob = $res->GetNextElement())
// {
//  $arFields = $ob->GetFields();
//  $next=$arFields["DETAIL_PAGE_URL"];
// }
// $arFilter = Array("IBLOCK_ID"=>$arResult['IBLOCK_ID'], "ACTIVE"=>"Y","<ID"=>$arResult['ID'],"DETAIL_PAGE_URL");
// $res = CIBlockElement::GetList(Array('ACTIVE_FROM'=>"DESC"), $arFilter, false, Array("nPageSize"=>50), $arSelect);
// while($ob = $res->GetNextElement())
// {
//  $arFields = $ob->GetFields();
//  $back=$arFields["DETAIL_PAGE_URL"];
// }
?>									
<section class="allbrands">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="title2"><span>Портфолио</span></h1>
			</div>
		</div>
	</div>
</section>
<section class="card">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="code_card">
					<div class="links">
						<? if(strlen($arResult['CUR_ELEMENT']['PREV']['NAME']) > 0): ?>
							<a href="<? echo $arResult['CUR_ELEMENT']['PREV']['DETAIL_PAGE_URL']; ?>" class="lprev active"><? echo GetMessage('PREV'); ?></a>
						<? else: ?>
							<a href="javascript:void(0);" class="lprev" rel="nofollow noreferrer noopener"><? echo GetMessage('PREV'); ?></a>
						<? endif; ?>

						<? if(strlen($arResult['CUR_ELEMENT']['NEXT']['NAME']) > 0): ?>				
							<a href="<? echo $arResult['CUR_ELEMENT']['NEXT']['DETAIL_PAGE_URL']; ?>" class="lnext active"><? echo GetMessage('NEXT'); ?></a>
						<? else: ?> 						
							<a href="javascript:void(0);" class="lnext" rel="nofollow noreferrer noopener"><? echo GetMessage('NEXT'); ?></a>
						<? endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="portfsingle card">
	<div class="container">
		<div class="row">
			<div class="portfsingle_block">
				<div class="slider slider-single slickcard">
					<? foreach ($arResult['BIG_IMG'] as $valSrc) { ?>
						<div>
							<a data-lightbox="example-set" href="<? echo $valSrc['REAL_SIZE']['src'] ?>">
								<img src="<? echo $valSrc['src']; ?>" alt="<?=$file['DESCRIPTION'];?>">
							</a>
						</div>  
					<? } ?>
				</div>
				<div class="slider slider-nav navcard">
					<? foreach ($arResult['SMALL_IMG'] as $valPhotoThumb) { ?>
						<div>
							<img src="<? echo $valPhotoThumb['src']; ?>" alt="">
						</div>
					<? } ?>
				</div>  
			</div>

			<div class="col-xs-12">
				<div class="contant">
					<p class="tit"><?=$arResult['PROPERTIES']['NAME']['VALUE'];?></p>
					<?=$arResult["~DETAIL_TEXT"];?>	
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="design">
							<div class="desform">
						  		<? $APPLICATION->IncludeFile("/services/form.php", Array(), Array("MODE"      => "php", "NAME"      => "Форма",));?>			
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
$('.slider-single').slick({
 	slidesToShow: 1,
 	slidesToScroll: 1,
 	arrows: true,
 	fade: false,
 	adaptiveHeight: true,
 	infinite: true,
	useTransform: true,
 	speed: 400,
 	cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
 });

 $('.slider-nav')
.on('init', function(event, slick) {
	$('.slider-nav .slick-slide.slick-current').addClass('is-active');
})
.slick({
	slidesToShow: 3,
	slidesToScroll: 1,
	dots: false,
	focusOnSelect: false,
	infinite: true,
	responsive: [{
		breakpoint: 1024,
		settings: {
			slidesToShow: 3,
			slidesToScroll: 1,
		}
	}, {
		breakpoint: 640,
		settings: {
			slidesToShow: 4,
			slidesToScroll: 4,
	}
	}, {
		breakpoint: 420,
		settings: {
			slidesToShow: 3,
			slidesToScroll: 3,
}
	}]
});

$('.slider-single').on('afterChange', function(event, slick, currentSlide) {
	$('.slider-nav').slick('slickGoTo', currentSlide);
	var currrentNavSlideElem = '.slider-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
	$('.slider-nav .slick-slide.is-active').removeClass('is-active');
	$(currrentNavSlideElem).addClass('is-active');
});

$('.slider-nav').on('click', '.slick-slide', function(event) {
	event.preventDefault();
	var goToSingleSlide = $(this).data('slick-index');

	$('.slider-single').slick('slickGoTo', goToSingleSlide);
});
</script>

<style>
	.portfsingle_block{
		width: 60%;
		margin: 0 auto;
	}


	.buttons__tabs{margin: 0 0 50px;}
	.buttons__tabs a.active{
		border: 1px solid #40a23c;
   		color: #40a23c;
	}
	.buttons__tabs a{
		display: inline-block;
		margin-right: 15px;
		height: 40px;
		line-height: 38px;
		padding: 0 5px;
		border: 1px solid #d0ccce;
		font-size: 25px;
		color: #363535;
		text-decoration: none!important;
		-webkit-transition: .3s;
		transition: .3s;
	}
	@media(max-width: 768px){
		.portfsingle_block{width: 90%;}
	}

</style>