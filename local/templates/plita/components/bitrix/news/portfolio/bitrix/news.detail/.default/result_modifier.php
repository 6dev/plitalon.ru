<?// foreach ($arResult['ITEMS'] as $key => $value) {

	foreach ($arResult['PROPERTIES']['PICS']['VALUE'] as $key => $valPhoto) {
		
		$arFile = CFile::GetFileArray($valPhoto);
		if($arFile){
			$arImgReal = CFile::ResizeImageGet(
	            $arFile,
	            array("width" => $arFile["WIDTH"], "height" => $arFile["HEIGHT"]),
	            BX_RESIZE_IMAGE_EXACT,
	            true,
	            false
	        );

			$arImgBig = CFile::ResizeImageGet(
	            $arFile,
	            array("width" => 746, "height" => 561),
	            BX_RESIZE_IMAGE_EXACT,
	            true,
	            false
	        );
			//$arImgBigs[] = $arImgBig;

	        $arImgSmall = CFile::ResizeImageGet(
	            $arFile,
	            array("width" => 248, "height" => 130),
	            BX_RESIZE_IMAGE_EXACT,
	            true,
	            false
	        );

	        //$arImgSmalls[] = $arImgSmall;

	        $arImgBig['REAL_SIZE'] = $arImgReal;
			$arResult['BIG_IMG'][] = $arImgBig;
			$arResult['SMALL_IMG'][] = $arImgSmall;
		}

	}

	
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "DETAIL_PAGE_URL", "PROPERTY_*");
	$arFilter = Array("IBLOCK_ID"=>$arResult["IBLOCK_ID"], "IBLOCK_SECTION_ID"=>$arResult["IBLOCK_SECTION_ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
	$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>300), $arSelect);
	while($ob = $res->GetNextElement()){ 

		$arFields[] = $ob->GetFields();  

		$arProps = $ob->GetProperties();

		$tip_tovara[] = $arProps['TIP_TOVARA']['VALUE'];
	}

	foreach ($arFields as $key => $valElements){
		if($valElements['ID'] == $arResult['ID']){
			$arResult['CUR_ELEMENT'] = $valElements;

			if($key == 0): 
				$arResult['CUR_ELEMENT']['NEXT'] = $arFields[$key + 1];
				$arResult['CUR_ELEMENT']['PREV'] = end($arFields);
			elseif($valCurSect == end($massSect)):
				$arResult['CUR_ELEMENT']['NEXT'] = $arFields[0];
				$arResult['CUR_ELEMENT']['PREV'] = $arFields[$key - 1];

			else:
				$arResult['CUR_ELEMENT']['NEXT'] = $arFields[$key + 1];
				$arResult['CUR_ELEMENT']['PREV'] = $arFields[$key - 1];
			endif;
		}
	}
	if($massSect[1]): $arResult['FIX_NEXT_PREV'] = true; else:  $arResult['FIX_NEXT_PREV'] = false; endif;

//} ?>