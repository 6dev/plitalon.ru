<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

if ($arResult['ID']<=0) {
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
header('Location: /404.php',301);
} else {



  $arFilter1 = Array('IBLOCK_ID'=>CATALOG_IBLOCK_ID, "!ID"=>$arResult['ID'],"DEPTH_LEVEL"=>2);
  $db_listNext = CIBlockSection::GetList(Array('ID'=>'ASC'), $arFilter1, true);
  $ar_resultNext = $db_listNext->GetNext();

  $arFilter2 = Array('IBLOCK_ID'=>CATALOG_IBLOCK_ID, "!ID"=>$arResult['ID'],"DEPTH_LEVEL"=>2);
  $db_listBack = CIBlockSection::GetList(Array('ID'=>'DESC'), $arFilter2, true);
  $ar_resultBack = $db_listNext->GetNext();

$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_","PROPERTY_KARTINKA_KOLLEKTSII","PROPERTY_KARTINKA_BRENDA","PROPERTY_KOD_BRENDA","IBLOCK_SECTION_ID","PROPERTY_BREND_OPISANIE");
$arFilter = Array("IBLOCK_ID"=>$arResult['IBLOCK_ID'],"SECTION_ID"=>$arResult['ID'], "!PROPERTY_KARTINKA_BRENDA"=>false, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y","INCLUDE_SUBSECTIONS"=>'Y');
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
$KOD_BRENDA='';
$detail='';
while($ob = $res->GetNextElement())
{
 
  $arFields = $ob->GetFields();
  

if ($arFields["PROPERTY_BREND_OPISANIE_VALUE"]!='') $detail=$arFields["PROPERTY_BREND_OPISANIE_VALUE"];
  
  if ($arFields['PROPERTY_KOD_BRENDA_VALUE']!='') $KOD_BRENDA=$arFields['PROPERTY_KOD_BRENDA_VALUE'];
  
  
  
  
  
  
  $MORE_PHOTO = array();
    $res2 = CIBlockElement::GetProperty(CATALOG_IBLOCK_ID, $arFields['ID'], "sort", "asc", array("CODE" => "MORE_PHOTO"));
    while ($ob2 = $res2->GetNext())
    {
		
		$ob2['DESCRIPTION']=mb_strtolower($ob2['DESCRIPTION']);
		
        $MORE_PHOTO[] = $ob2;
    }
	
	
$e=explode(',',mb_strtolower($arFields['PROPERTY_KARTINKA_BRENDA_VALUE']));

$files=$e;



$fp=array();

foreach ($MORE_PHOTO as $file)
{
	
if (in_array($file['DESCRIPTION'].'.jpg',$files)) $fp[]=$file;
if (in_array($file['DESCRIPTION'].'.jpeg',$files)) $fp[]=$file;

// Смотрим все файлы для данного товара
	
}
  
  

  
  
  
  
  
  
  
  
}


?>

<section class="card">
		<div class="container">
			<div class="row">
				<div class="col-md-11">
					<p class="title2">
						<?=$arResult['NAME'];?>
					</p>
					<div class="code_card">
						<div class="links">
				<? if ($ar_resultBack['SECTION_PAGE_URL']!='') { ?>	<a href="<?=$ar_resultBack['SECTION_PAGE_URL'];?>" class="lprev">Пред.</a>   <? } ?>
				<? if ($ar_resultNext['SECTION_PAGE_URL']!='') { ?>			<a href="<?=$ar_resultNext['SECTION_PAGE_URL'];?>" class="lnext active">След.</a> <? } ?>
						</div>
						<? if ($KOD_BRENDA!='') { ?>
						<div class="article">
							<p>код <?=$KOD_BRENDA;?></p>
						</div>
						<? } ?>
					</div>
					<div class="brand_unit">
						<div class="uimg">
							<div class="buntab">
								<div class="uimgwr">
								
			
							 
						<?



$arFilter = Array('IBLOCK_ID'=>CATALOG_IBLOCK_ID, 'ID'=>$arResult['ID']);
  $db = CIBlockSection::GetList(Array('ID'=>'ASC'), $arFilter, true,array("UF_PICS"),true);
  $ar = $db->GetNext();
  


?>				
<?		
							 
							 foreach ($ar['UF_PICS'] as $file) { ?>
	
				
					 <? } ?>	
 
					<? if ($file>0) { ?>	<div><img src="<?=CFIle::GetPath($file);?>" alt=""></div>  <? } ?>
	
								
									
								</div>
							</div>
						</div>
						<div class="ucont">
							<div class="ucontwr">
								
								<p><?=$arResult['DESCRIPTION'];?></p>
								
								<a href="/download/<?=$arResult['CODE'];?>/" target="_blank" class="downgreen">
									Скачать каталог 
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
					<p class="cardtabtitle">
						<span>
							Коллекции бренда
						</span>
					</p>
				</div>
			</div>
		</div>
	</section>

	<section class="catalog_container">

<?

global $arFSectionS;

$is=array();

$arFilter = Array('IBLOCK_ID'=>CATALOG_IBLOCK_ID, 'SECTION_ID'=>$arResult['ID']);
  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
  while($ar_result = $db_list->GetNext())
  {
   $is[]=$ar_result['ID'];
  }

$arFSectionS['ID']=$is;

$APPLICATION->IncludeComponent(
   "bitrix:catalog.section.list", 
   "kolls", 
   array(
      "VIEW_MODE" => "TEXT",
        "SHOW_PARENT_NAME" => "Y",
        "IBLOCK_TYPE" => "",
		"FILTER_NAME"=>"arFSectionS",
        "IBLOCK_ID" => CATALOG_IBLOCK_ID,
        "SECTION_ID" => false,
        "SECTION_CODE" => "",
        "SECTION_URL" => "",
        "COUNT_ELEMENTS" => "Y",
        "TOP_DEPTH" => "3",
        "SECTION_FIELDS" => "",
        "SECTION_USER_FIELDS" => "",
        "ADD_SECTIONS_CHAIN" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_NOTES" => "",
        "CACHE_GROUPS" => "Y"
   ),
   false
);
?>


	</section>
	
<? } ?>