<section class="downloads">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p class="title2">
						<span><?=$arResult['NAME'];?></span>
					</p>
					<div class="units">
					
					<? foreach ($arResult['ITEMS'] as $arItem) { ?>

						<div class="el">
							<div class="uim">
								<img src="<?=$arItem['DETAIL_PICTURE']['SRC'];?>" alt="<?=$arResult['NAME'];?>">
							</div>
							<p class="tit">
								<a class="logfox fancybox" href="<?=$arItem['DETAIL_PICTURE']['SRC'];?>">
									<?=$arItem['NAME'];?>
								</a>
							</p>
						</div>

					<? } ?>

					</div>
				</div>
			</div>
		</div>
	</section>