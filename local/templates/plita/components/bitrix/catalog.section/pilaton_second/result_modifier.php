<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$key = 0;
foreach ($arResult['ITEMS'] as $key => $arItem){ 

	if(strlen($arItem['DETAIL_PICTURE']['SRC']) > 0):

		if($arItem['DETAIL_PICTURE']['HEIGHT'] > $arItem['DETAIL_PICTURE']['WIDTH']):

			$arFileTmp = CFile::ResizeImageGet(
		        $arItem['DETAIL_PICTURE'],
		        array("width" => 158, "height" => 310),
		        BX_RESIZE_IMAGE_PROPORTIONAL,
		        true,
		        false
		    );

		elseif($arItem['DETAIL_PICTURE']['HEIGHT'] < $arItem['DETAIL_PICTURE']['WIDTH']):

			$arFileTmp = CFile::ResizeImageGet(
		        $arItem['DETAIL_PICTURE'],
		        array("width" => 310, "height" => 200),
		        BX_RESIZE_IMAGE_PROPORTIONAL,
		        true,
		        false
		    );

		else:

			$arFileTmp = CFile::ResizeImageGet(
		        $arItem['DETAIL_PICTURE'],
		        array("width" => 310, "height" => 310),
		        BX_RESIZE_IMAGE_PROPORTIONAL,
		        true,
		        false
		    );

		endif;

	    $arResult['ITEMS'][$key]["PREVIEW_PICTURE"]["RESIZESRC"] = $arFileTmp;

	endif; 

    $key = $key + 1;

}


$select = array("ID", "NAME", "SECTION_PAGE_URL", "UF_*");
$sort = array("SORT" => "ASC");

$filter = array('IBLOCK_ID' => $arResult['IBLOCK_ID'], 'SECTION_ID' => $arResult['PATH'][0]['ID'], 'DEPTH_LEVEL' => 2);

$rsRes = CIBlockSection::GetList($sort, $filter, false, $select);

while($arSect = $rsRes->GetNext()){
  	$massSect[] = $arSect;
}




$i = 0;
foreach ($massSect as $key => $valCurSect) {
	if($valCurSect['ID'] == $arResult['ID']){
		$arResult['CUR_SECTION'] = $valCurSect;

		if($key == 0): 
			$arResult['CUR_SECTION']['NEXT'] = $massSect[$key + 1];
			$arResult['CUR_SECTION']['PREV'] = end($massSect);
		elseif($valCurSect == end($massSect)):
			$arResult['CUR_SECTION']['NEXT'] = $massSect[0];
			$arResult['CUR_SECTION']['PREV'] = $massSect[$key - 1];

		else:
			$arResult['CUR_SECTION']['NEXT'] = $massSect[$key + 1];
			$arResult['CUR_SECTION']['PREV'] = $massSect[$key - 1];
		endif;
	}
}


if(strlen($arResult['CUR_SECTION']['UF_PICS'][0]) > 0):
	$arFile = CFile::GetFileArray($arResult['CUR_SECTION']['UF_PICS'][0]);

	$arCurImg = CFile::ResizeImageGet(
        $arFile,
        array("width" => 267, "height" => 200),
        BX_RESIZE_IMAGE_PROPORTIONAL,
        true,
        false
    );

    $arResult['CUR_SECTION']['RESIMG'] =  $arCurImg;

endif;
?>
<? if($massSect[1]): $arResult['FIX_NEXT_PREV'] = true; else:  $arResult['FIX_NEXT_PREV'] = false; endif;?>
