<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arFilter = array('IBLOCK_ID' => $arResult['ORIGINAL_PARAMETERS']['IBLOCK_ID'], 'DEPTH_LEVEL' => 2);
$rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), $arFilter);
while ($arSect = $rsSect->GetNext()){
	$sectionBrand[] = $arSect;
}


$key = 0;
foreach ($arResult['ITEMS'] as $key => $arItem){ 

	foreach ($sectionBrand as $valBrand) {
		if($arItem['PROPERTIES']['BREND']['VALUE'] == trim($valBrand['NAME'])){
			$brandName = $valBrand;
		}
	}
	$arResult["ITEMS"][$key]["BRANDS"] = $brandName;

	if(strlen($arItem['DETAIL_PICTURE']['SRC']) > 0):

		if($arItem['DETAIL_PICTURE']['HEIGHT'] > $arItem['DETAIL_PICTURE']['WIDTH']):

			$arFileTmp = CFile::ResizeImageGet(
		        $arItem['DETAIL_PICTURE'],
		        array("width" => 158, "height" => 310),
		        BX_RESIZE_IMAGE_PROPORTIONAL,
		        true,
		        false
		    );

		elseif($arItem['DETAIL_PICTURE']['HEIGHT'] < $arItem['DETAIL_PICTURE']['WIDTH']):

			$arFileTmp = CFile::ResizeImageGet(
		        $arItem['DETAIL_PICTURE'],
		        array("width" => 310, "height" => 200),
		        BX_RESIZE_IMAGE_PROPORTIONAL,
		        true,
		        false
		    );

		else:

			$arFileTmp = CFile::ResizeImageGet(
		        $arItem['DETAIL_PICTURE'],
		        array("width" => 310, "height" => 310),
		        BX_RESIZE_IMAGE_PROPORTIONAL,
		        true,
		        false
		    );

		endif;

	    $arResult['ITEMS'][$key]["PREVIEW_PICTURE"]["RESIZESRC"] = $arFileTmp;

	endif; 

    $key = $key + 1;


}



if($arResult['DEPTH_LEVEL'] == 1):
	$arFilter = Array("IBLOCK_ID"=>$arResult["IBLOCK_ID"], "ID" => $arResult["ORIGINAL_PARAMETERS"]["GLOBAL_FILTER"]["ID"], "SECTION_ID" => $arResult["ORIGINAL_PARAMETERS"]["SECTION_ID"], "INCLUDE_SUBSECTIONS" => "Y", "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
else:
	$arFilter = Array("IBLOCK_ID"=>$arResult["IBLOCK_ID"], "ID" => $arResult["ORIGINAL_PARAMETERS"]["GLOBAL_FILTER"]["ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
endif;

$arSelect = Array("ID", "NAME", "IBLOCK_SECTION_ID");

$res = CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
while($ob = $res->GetNextElement())
{
	$massIdSect = $ob->GetFields();
	$arMass[] = $massIdSect["IBLOCK_SECTION_ID"];
}
$massIdSect = array_unique($arMass);

foreach ($massIdSect as $key => $value){
	$newmassIdSect[] = $value;
}

$arResult["SECT"] = $newmassIdSect;

?>
