<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

// if(CModule::IncludeModule('kombox.filter')){
// 	$seo_filter_text = Kombox\Filter\Seo::ShowText("");
// }

$this->setFrameMode(true);


if (!empty($arResult['NAV_RESULT']))
{
	$navParams =  array(
		'NavPageCount' => $arResult['NAV_RESULT']->NavPageCount,
		'NavPageNomer' => $arResult['NAV_RESULT']->NavPageNomer,
		'NavNum' => $arResult['NAV_RESULT']->NavNum
	);
}
else
{
	$navParams = array(
		'NavPageCount' => 1,
		'NavPageNomer' => 1,
		'NavNum' => $this->randString()
	);
}

$showTopPager = false;
$showBottomPager = false;
$showLazyLoad = false;

if ($arParams['PAGE_ELEMENT_COUNT'] > 0 && $navParams['NavPageCount'] > 1)
{
	$showTopPager = $arParams['DISPLAY_TOP_PAGER'];
	$showBottomPager = $arParams['DISPLAY_BOTTOM_PAGER'];
	$showLazyLoad = $arParams['LAZY_LOAD'] === 'Y' && $navParams['NavPageNomer'] != $navParams['NavPageCount'];
}

$templateLibrary = array('popup', 'ajax', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = array('CONFIRM' => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

$positionClassMap = array(
	'left' => 'product-item-label-left',
	'center' => 'product-item-label-center',
	'right' => 'product-item-label-right',
	'bottom' => 'product-item-label-bottom',
	'middle' => 'product-item-label-middle',
	'top' => 'product-item-label-top'
);

$discountPositionClass = '';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
	{
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$labelPositionClass = '';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
	{
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$arParams['~MESS_BTN_BUY'] = $arParams['~MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_BUY');
$arParams['~MESS_BTN_DETAIL'] = $arParams['~MESS_BTN_DETAIL'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_DETAIL');
$arParams['~MESS_BTN_COMPARE'] = $arParams['~MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_COMPARE');
$arParams['~MESS_BTN_SUBSCRIBE'] = $arParams['~MESS_BTN_SUBSCRIBE'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_SUBSCRIBE');
$arParams['~MESS_BTN_ADD_TO_BASKET'] = $arParams['~MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_ADD_TO_BASKET');
$arParams['~MESS_NOT_AVAILABLE'] = $arParams['~MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCS_TPL_MESS_PRODUCT_NOT_AVAILABLE');
$arParams['~MESS_SHOW_MAX_QUANTITY'] = $arParams['~MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCS_CATALOG_SHOW_MAX_QUANTITY');
$arParams['~MESS_RELATIVE_QUANTITY_MANY'] = $arParams['~MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCS_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['~MESS_RELATIVE_QUANTITY_FEW'] = $arParams['~MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCS_CATALOG_RELATIVE_QUANTITY_FEW');

$arParams['MESS_BTN_LAZY_LOAD'] = $arParams['MESS_BTN_LAZY_LOAD'] ?: Loc::getMessage('CT_BCS_CATALOG_MESS_BTN_LAZY_LOAD');

$generalParams = array(
	'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
	'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
	'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
	'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
	'MESS_SHOW_MAX_QUANTITY' => $arParams['~MESS_SHOW_MAX_QUANTITY'],
	'MESS_RELATIVE_QUANTITY_MANY' => $arParams['~MESS_RELATIVE_QUANTITY_MANY'],
	'MESS_RELATIVE_QUANTITY_FEW' => $arParams['~MESS_RELATIVE_QUANTITY_FEW'],
	'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
	'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
	'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
	'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
	'ADD_PROPERTIES_TO_BASKET' => $arParams['ADD_PROPERTIES_TO_BASKET'],
	'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
	'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'],
	'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
	'COMPARE_PATH' => $arParams['COMPARE_PATH'],
	'COMPARE_NAME' => $arParams['COMPARE_NAME'],
	'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
	'PRODUCT_BLOCKS_ORDER' => $arParams['PRODUCT_BLOCKS_ORDER'],
	'LABEL_POSITION_CLASS' => $labelPositionClass,
	'DISCOUNT_POSITION_CLASS' => $discountPositionClass,
	'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
	'SLIDER_PROGRESS' => $arParams['SLIDER_PROGRESS'],
	'~BASKET_URL' => $arParams['~BASKET_URL'],
	'~ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
	'~BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE'],
	'~COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
	'~COMPARE_DELETE_URL_TEMPLATE' => $arResult['~COMPARE_DELETE_URL_TEMPLATE'],
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
	'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
	'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY'],
	'MESS_BTN_BUY' => $arParams['~MESS_BTN_BUY'],
	'MESS_BTN_DETAIL' => $arParams['~MESS_BTN_DETAIL'],
	'MESS_BTN_COMPARE' => $arParams['~MESS_BTN_COMPARE'],
	'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
	'MESS_BTN_ADD_TO_BASKET' => $arParams['~MESS_BTN_ADD_TO_BASKET'],
	'MESS_NOT_AVAILABLE' => $arParams['~MESS_NOT_AVAILABLE']
);

$obName = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $this->GetEditAreaId($navParams['NavNum']));
$containerName = 'container-'.$navParams['NavNum'];
?>

<? if (!empty($arResult['ITEMS'])){ 

	$arSkuTemplate = array();
	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));


	$areaIds = array();
	$page = $APPLICATION->GetCurPageParam();
	$pageNotParams = $APPLICATION->GetCurPage();

	$sortPrice = strripos($page, "catalog_PRICE_10");
	$sortMetod = strripos($page, "method=asc");

	$sortFilt = strripos($_SERVER['REQUEST_URI'], "filter");
	$sortPagen = strripos($_SERVER['REQUEST_URI'], "PAGEN");

	$strPage = $_SERVER['REQUEST_URI'];

	$newPAGE = preg_replace("/\?.+/", "", $strPage);

	if(($sortPrice == true) && ($sortMetod == true)){
		$getUrl = 'catalog_PRICE_10&method=desc';
	} else{
		$getUrl = 'catalog_PRICE_10&method=asc';
	}

	// if($sortFilt == true || $sortPagen == true){
	// 	if(($sortPrice == true) && ($sortMetod == true)){
	// 		$getUrl = $_SERVER['REQUEST_URI'].'&sort=catalog_PRICE_10&method=desc';
	// 	} else{
	// 		$getUrl = $_SERVER['REQUEST_URI'].'&sort=catalog_PRICE_10&method=asc';
	// 	}
	// } else{
	// 	
	// }
	?>

	<div class="filterblock">
		<div class="info">
			<div class="infoval" role="tablist">
				<ul class="infoval-tabs" id="myTab">
					<li>
						<? if (count($arResult['SECT']) > 0): ?>
							<a href="#tabCollection" aria-controls="tabCollection" role="tab" data-toggle="tab" class="infofoods infokol">
								<? echo count($arResult['SECT']); ?> коллекций
							</a>
						<? endif; ?>
					</li>
					<li class="active">
						<a href="#tabItems" aria-controls="tabItems" role="tab" data-toggle="tab" class="infofoods infoitem">
							<? if (is_object($arResult['NAV_RESULT']))  $num_elements = $arResult['NAV_RESULT']->NavRecordCount; echo $num_elements; ?> товар(ов)
						</a>
					</li>
				</ul>
			</div>
			<div class="sortingfilter">
				<div class="el el1">
					<p>
						Сортировать по:

						<a href="<?=CKomboxFilter::GetCurPageParam('sort='.$getUrl, array('sort'));?>">цене</a>
				
					</p>
				</div>
				<div class="el el2">
		
					<p class="selfil selfil1">
						<a <?if ($_GET["sort"] == "propertysort_NEW"):?><?endif;?>href="<?=$newPAGE?>?sort=propertysort_NEW&method=desc" class="sort" id="news">Новинки</a>
					</p>
					<p class="selfil selfil2">
						<a <?if ($_GET["sort"] == "propertysort_HIT"):?><?endif;?>href="<?=$newPAGE?>?sort=propertysort_HIT&method=desc" class="sort" id="hit">Хиты продаж</a>
					</p>
					<p class="selfil selfil3">
						<a <?if ($_GET["sort"] == "propertysort_POPS"):?><?endif;?>href="<?=$newPAGE?>?sort=propertysort_POPS&method=desc" class="sort" id="skidka">Популярные</a>
					</p>
				</div>
			</div>
		</div>
	</div>
	<section class="catalog_container wow animated active" data-wow-offset="200" id="tabItems">
		<div class="container">
			<div class="row">
				<div id="catalog_items" class="col-md-12 contkoll">
					<div class="flex_val popular">
						<?		
						foreach ($arResult['ITEMS'] as $item){
							$this->AddEditAction($item['ID'], $item['EDIT_LINK'], $strElementEdit);
							$this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
							$strMainID = $this->GetEditAreaId($item['ID']);

						?>
							<div class="unitcard1wr" id="<? echo $strMainID; ?>">
								<div class="unitcard1">
									<div class="uimg">
										<div class="imgwr">
											<a href="<? echo $item['DETAIL_PAGE_URL']; ?>">
												<? if(strlen($item['PREVIEW_PICTURE']['RESIZESRC']['src']) > 0): ?>
													<img src="<? echo $item['PREVIEW_PICTURE']['RESIZESRC']['src']; ?>" alt="">
												<? else: ?>
													<img src="/local/templates/plita/components/bitrix/catalog.section/pilaton_section/images/no_photo.png" alt="">
												<? endif; ?>
											</a>
										</div>
										<div class="uimg-abs">
											<div class="inform clearfix">
												<? if($item['PROPERTIES']['HIT']['VALUE'] == 'Y'): ?>
													<p class="hit"><span><?=$item['PROPERTIES']['HIT']['NAME']?></span></p>
												<? endif; ?>
												<?  if($item['PROPERTIES']['NEW']['VALUE'] == 'Y'): ?>
													<p class="new"><span><?=$item['PROPERTIES']['NEW']['NAME']?></span></p>
												<? endif; ?>
												<a href="javascript:void(0);" class="liked" index-id2="<? echo $item['ID']; ?>" rel="nofollow noreferrer noopener" style="background-image: url(/local/templates/plita/images/liked.png); background-repeat: no-repeat;"></a>
											</div>
										</div>
									</div>
									<div class="unval">
										<p class="tit2">
											<a href="<? echo $item['DETAIL_PAGE_URL']; ?>"><? echo $item['NAME']; ?></a>
										</p>
										<div class="model">
	                                        <a href="<?=$item['BRANDS']['SECTION_PAGE_URL'];?>" tabindex="0"><?=$item['BRANDS']['NAME']; ?></a>
	                                    </div>
										<div class="cost2">
											<? if(strlen($item['PRICES']['Проба Цена продажи']['VALUE_VAT']) > 0): ?>
												<b><? echo $item['PRICES']['Проба Цена продажи']['ROUND_VALUE_VAT']; ?></b> руб.<?='/'. $item['ITEM_MEASURE']['TITLE'];?>
											<? else: ?>
												<b>Под заказ</b>
											<? endif; ?>
										</div>

										<? if(($item['PROPERTIES']['SROK_POSTAVKI']['VALUE'] != 'Снято с производства')  && (strlen($item['PRICES']['Проба Цена продажи']['VALUE_VAT']) > 0)): ?>
											<div class="cardcalc">
												<div class="cartcalc">
													<? if(strlen($item['PROPERTIES']['MINIMALNAYA_KRATNOST_ZAKAZA']['~VALUE']) > 0):
														$kratnostZakaza = str_replace(',','.', $item['PROPERTIES']['MINIMALNAYA_KRATNOST_ZAKAZA']['~VALUE']);
													endif;?>

													<button class="ccalc-minus">-</button>
													<input type="text" dd="<? echo $kratnostZakaza; ?>" value="<? echo $kratnostZakaza; ?>">
													<button class="ccalc-plus">+</button>
												</div>
												<button class="cardun1-button cardgreen" title="<? $item['NAME']; ?>" index-id="<? echo $item['ID']; ?>">В корзину</button>
											</div>
										<? endif; ?>

										
									</div>
									<div class="dopinfocard">
										<div class="moreinfo">
											<? foreach ($item['DISPLAY_PROPERTIES'] as $PropValue) { ?>
												<p><b><? echo $PropValue['NAME']; ?>:</b><span><? echo $PropValue['VALUE']; ?></span></p>
											<? } ?>
										</div>
									</div>
								</div>
							</div>
						<? } ?>
					</div>
				</div>
				<?if ($showBottomPager)
				{
					?>
					<div data-pagination-num="<?=$navParams['NavNum']?>">
						<!-- pagination-container -->
						<?=$arResult['NAV_STRING']?>
						<!-- pagination-container -->
					</div>
					<?
				}?>

				<div class="section_text">
					<?

					echo $seo_filter_text;
					?>
				</div>
			</div>
		</div>
	</section>

<? } ?>

<div role="tabpanel" id="tabCollection" style="display: none;">

<?
$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list", 
	"collection_filter", 
	array(
		"PARENT_ID" => $arResult["SECT"],
		"VIEW_MODE" => "TEXT",
		"SHOW_PARENT_NAME" => "Y",
		"IBLOCK_TYPE" => "1c_catalog",
		"IBLOCK_ID" => "18",
		"SECTION_ID" => "",
		"SECTION_CODE" => "",
		"SECTION_URL" => "",
		"COUNT_ELEMENTS" => "Y",
		"TOP_DEPTH" => "3",
		"SECTION_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "arrFilterSect",
		"SECTION_USER_FIELDS" => array(
			0 => "UF_PICS",
			1 => "UF_HIT",
			2 => "UF_NEW",
			3 => "UF_BR",
			4 => "UF_KH",
			5 => "",
		),
		"ADD_SECTIONS_CHAIN" => "Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000",
		"CACHE_NOTES" => "",
		"CACHE_GROUPS" => "Y",
		"COMPONENT_TEMPLATE" => "collection_filter",
		"COUNT_ELEMENTS_FILTER" => "CNT_ACTIVE",
		"CACHE_FILTER" => "N",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>

</div>

<style>
.unitcard1 .tit2{height: 50px;}#catalog_items{margin-left: -15px;}
#tabItems{display: none;}
#tabItems.active{display: block;}
#tabCollection.active{display: block !important;}

.s_price_a:after, .s_price:after{
	content: '';
    display: inline-block;
    width: 8px;
    height: 8px;
    margin-left: 6px;
    background-image: url(/local/templates/plita/images/ico_sort_visokaya_tcena.svg);
}
.s_price:after{
	transform: rotateX(180deg);
}
</style>

<script>
	$(document).ready(function(){
		$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
			localStorage.setItem('activeTab', $(e.target).attr('href'));
		});
		var activeTab = localStorage.getItem('activeTab');
		if(activeTab){
			$('#myTab a[href="' + activeTab + '"]').tab('show');
		}
	});
</script>