<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

function minKeys($array) {
	$min = 100000;
	foreach($array as $key=>$item) {
		if($item<$min) {
			$min = $item;
			$minKey = $key;
		}
	}

	if($min<10000) {
		return [
			'KEY' => $minKey,
			'VALUE' => $min
		];
	}
}
$key = 0;
foreach ($arResult['ITEMS'] as $key => $arItem){ 

	if(strlen($arItem['DETAIL_PICTURE']['SRC']) > 0):

		if($arItem['DETAIL_PICTURE']['HEIGHT'] > $arItem['DETAIL_PICTURE']['WIDTH']):

			$arFileTmp = CFile::ResizeImageGet(
		        $arItem['DETAIL_PICTURE'],
		        array("width" => 158, "height" => 310),
		        BX_RESIZE_IMAGE_PROPORTIONAL,
		        true,
		        false
		    );

		elseif($arItem['DETAIL_PICTURE']['HEIGHT'] < $arItem['DETAIL_PICTURE']['WIDTH']):

			$arFileTmp = CFile::ResizeImageGet(
		        $arItem['DETAIL_PICTURE'],
		        array("width" => 310, "height" => 200),
		        BX_RESIZE_IMAGE_PROPORTIONAL,
		        true,
		        false
		    );

		else:

			$arFileTmp = CFile::ResizeImageGet(
		        $arItem['DETAIL_PICTURE'],
		        array("width" => 310, "height" => 310),
		        BX_RESIZE_IMAGE_PROPORTIONAL,
		        true,
		        false
		    );

		endif;

	    $arResult['ITEMS'][$key]["PREVIEW_PICTURE"]["RESIZESRC"] = $arFileTmp;

	endif; 

    $key = $key + 1;

}

$select = array("ID", "NAME", "SECTION_PAGE_URL", "UF_*");
$sort = array("SORT" => "ASC");

$filter = array('IBLOCK_ID' => $arResult['IBLOCK_ID'], 'SECTION_ID' => $arResult['IBLOCK_SECTION_ID'], 'DEPTH_LEVEL' => 3);

$rsRes = CIBlockSection::GetList($sort, $filter, false, $select);

while($arSect = $rsRes->GetNext()){
  	$massSect[] = $arSect;
}

$i = 0;
foreach ($massSect as $key => $valCurSect) {
	if($valCurSect['ID'] == $arResult['ID']){
		$arResult['CUR_SECTION'] = $valCurSect;

		if($key == 0): 
			$arResult['CUR_SECTION']['NEXT'] = $massSect[$key + 1];
			$arResult['CUR_SECTION']['PREV'] = end($massSect);
		elseif($valCurSect == end($massSect)):
			$arResult['CUR_SECTION']['NEXT'] = $massSect[0];
			$arResult['CUR_SECTION']['PREV'] = $massSect[$key - 1];

		else:
			$arResult['CUR_SECTION']['NEXT'] = $massSect[$key + 1];
			$arResult['CUR_SECTION']['PREV'] = $massSect[$key - 1];
		endif;
	}
}
if($massSect[1]): $arResult['FIX_NEXT_PREV'] = true; else:  $arResult['FIX_NEXT_PREV'] = false; endif;

if(strlen($arResult['CUR_SECTION']['UF_PICS'][0]) > 0):
	foreach ($arResult['CUR_SECTION']['UF_PICS'] as $valPhoto) {
		$arFile = CFile::GetFileArray($valPhoto);

		$arCurImgBig = CFile::ResizeImageGet(
	        $arFile,
	        array("width" => 746, "height" => 561),
	        BX_RESIZE_IMAGE_EXACT,
	        true,
	        false
	    );
	    $arCurImgTumb = CFile::ResizeImageGet(
	        $arFile,
	        array("width" => 248, "height" => 130),
	        BX_RESIZE_IMAGE_EXACT,
	        true,
	        false
	    );

	    $arWaterMark = Array(
		    array(
		        "name" => "watermark",
		        "position" => "bottomright", // Положение
		        "type" => "image",
		        "size" => "real",
		        "file" => $_SERVER["DOCUMENT_ROOT"].'/local/templates/plita/images/water.png', // Путь к картинке
		        "fill" => "exact",
		    )
		);
		$arFileTmpReal = CFile::ResizeImageGet(
		    $arFile,
		    array("width" => $arFile['WIDTH'], "height" => $arFile['HEIGHT']),
		    BX_RESIZE_IMAGE_EXACT,
		    true,
		    $arWaterMark
		);

	    $arCurImgBig['REAL_SIZE'] = $arFileTmpReal;
   		$arResult['CUR_SECTION']['RESIMG']['BIG'][] =  $arCurImgBig;
   		$arResult['CUR_SECTION']['RESIMG']['THUMB'][] = $arCurImgTumb;
	}
endif;


$minPriceOther = array();
$arSort = array('catalog_PRICE_10' => 'ASC');
$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_*", "CATALOG_GROUP_10");
$arFilter = Array("IBLOCK_ID"=>$arResult["IBLOCK_ID"], "IBLOCK_SECTION_ID"=>$arResult["ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList($arSort, $arFilter, false, Array("nPageSize"=>300), $arSelect);
while($ob = $res->GetNextElement()){ 

	$arFields = $ob->GetFields();  
		
 	$arProps = $ob->GetProperties();

 	$arFields['PROP'] = $arProps;

 // 	$elementPrice = CPrice::GetList(array(),array("PRODUCT_ID" => $arFields['ID']));
	// if ($ar_res_price = $elementPrice->Fetch()){
 //    	$arFields['PRICE'] = $ar_res_price;
	// }

 	$masElement[] = $arFields;

 	$poverhnost[] = $arProps['POVERKHNOST']['VALUE'];
    $format[] = $arProps['FORMAT']['VALUE'];
    $tolshina[] = $arProps['TOLSHCHINA']['VALUE'];
    $material[] = $arProps['MATERIAL']['VALUE'];
    $srok[] = $arProps['SROK_POSTAVKI']['VALUE'];

    $tip_tovara[] = $arProps['TIP_TOVARA']['VALUE'];

    if(strlen($arFields['CATALOG_PRICE_10']) > 0):
    	if($arProps['TIP_TOVARA']['VALUE'] == "Плитка"):
			$massPrice[$arFields['ID']] = $arFields['CATALOG_PRICE_10'];
		else:
			$massOtherPrice[$arFields['ID']] = $arFields['CATALOG_PRICE_10'];
		endif;
	endif;

			$arMeasure[$arFields['ID']] = $arFields['CATALOG_MEASURE'];
			$arType[$arFields['ID']] = $arFields['PROPERTY_TIP_TOVARA_VALUE'];

    // $finPrice = $arFields['CATALOG_PRICE_10'];
   	// $minPriceOther[] = $finPrice;
}


if($massPrice){
		$newMin = minKeys($massPrice);
		$minPrice = min($massPrice);
		$minPrice = $newMin['VALUE'];

	$arResult["CUR_SECTION"]["MIN_PRICE"] = round($minPrice);
		$arResult["CUR_SECTION"]["MEASURE"] = $arMeasure[$newMin['KEY']];
} else{
	$minPrices = min($massOtherPrice);
		$newMin = minKeys($massOtherPrice);
	$arResult["CUR_SECTION"]["MIN_PRICE"] = round($minPrices);
		$arResult["CUR_SECTION"]["MEASURE"] = $arMeasure[$newMin['KEY']];
}
	

//print_r($massPrice);

// Получаем ед. изм. товаров для коллекции

// $arMeasure = \Bitrix\Catalog\ProductTable::getCurrentRatioWithMeasure($masElement[0]["ID"]);   

// print_r($arMeasure);

// ////////////////////////////////////////

$poverhnostDelDublikat = array_unique($poverhnost);
$formatDelDublikat = array_unique($format);
$tolshinaDelDublikat = array_unique($tolshina);
$materialDelDublikat = array_unique($material);
$srokDelDublikat = array_unique($srok);

$tiptovaraDelDublikat = array_unique($tip_tovara);

// Сортировка

sort($tolshinaDelDublikat);
reset($tolshinaDelDublikat);
while (list($key, $val) = each($tolshinaDelDublikat)) {}

sort($formatDelDublikat);
reset($formatDelDublikat);
while (list($key, $val) = each($formatDelDublikat)) {}

// Получаем свойства товара для раздела 3 уровня с учетом пагинации.

$arResult["CUR_SECTION"]["POVERHNOST"] = $poverhnostDelDublikat;
$arResult["CUR_SECTION"]["FORMAT"] = $formatDelDublikat;
$arResult["CUR_SECTION"]["TOLSHCHINA"] = $tolshinaDelDublikat;
$arResult["CUR_SECTION"]["MATERIAL"] = $materialDelDublikat;
$arResult["CUR_SECTION"]["SROK_POSTAVKI"] = $srokDelDublikat;

$arResult["CUR_SECTION"]["TIP_TOVARA"] = $tiptovaraDelDublikat;


// Получаем минимальную цену у товаров с учетом пагинации.

// foreach ($masElement as $key => $valPrice_type_Plitka){
// 	if($valPrice_type_Plitka['PROP']['TIP_TOVARA']['VALUE'] == 'Плитка'){
// 		$priceFin = $valPrice_type_Plitka['CATALOG_PRICE_10'];
// 		$minPrice[] = $priceFin;
// 	}
// }
// if($minPrice){} else{$minPrice = $minPriceOther;}

// foreach ($masElement as $key => $valPrice_type_Plitka){
// 	if(($valPrice_type_Plitka['PROP']['TIP_TOVARA']['VALUE'] == 'Плитка') || ($valPrice_type_Plitka['PROP']['TIP_TOVARA']['VALUE'] == 'Инструмент') || ($valPrice_type_Plitka['PROP']['TIP_TOVARA']['VALUE'] == 'Затирка')){
// 		if(strlen($valPrice_type_Plitka['CATALOG_PRICE_10']) > 0){
// 			if($valPrice_type_Plitka['CATALOG_CURRENCY_10'] == 'EUR'){

// 				// $arFilter = array("CURRENCY" => "EUR"); $by = "date"; $order = "desc";

// 				// $db_rate = CCurrencyRates::GetList($by, $order, $arFilter);
// 				// while($ar_rate = $db_rate->Fetch())
// 				// {
// 				//     echo $ar_rate["RATE"]."<br>";
// 				// }
// 				$val = $valPrice_type_Plitka['CATALOG_PRICE_10'];
// 				$minPrice[] = CCurrencyRates::ConvertCurrency($val, "EUR", "RUB");

// 			} else{
// 				$minPrice[] = $valPrice_type_Plitka['CATALOG_PRICE_10'];
// 			}
// 		}
// 		else{
// 			if($valPrice_type_Plitka['CATALOG_CURRENCY_10'] == 'EUR'){


// 				$val = $valPrice_type_Plitka['CATALOG_PRICE_10'];
// 				$minPrice[] = CCurrencyRates::ConvertCurrency($val, "EUR", "RUB");
// 				// $arFilter = array("CURRENCY" => "EUR"); $by = "date"; $order = "desc";

// 				// $db_rate = CCurrencyRates::GetList($by, $order, $arFilter);
// 				// while($ar_rate = $db_rate->Fetch())
// 				// {
// 				//     echo $ar_rate["RATE"]."<br>";
// 				// }

// 			} else{
// 				$minPrice[] = $valPrice_type_Plitka['CATALOG_PRICE_10'];
// 			}
// 		}
// 	}
// }

// $minPrice = min($minPrice);
// $arResult["CUR_SECTION"]["MIN_PRICE"] = round($minPrice);


// if(!isset($minPrice)){
// 	foreach ($masElement as $valPrice_type_Plitkas){
// 		$priceFins = $valPrice_type_Plitkas['CATALOG_PRICE_10'];
// 		$minPrices[] = $priceFins;
// 		$minPrices = min($minPrices);
// 		$arResult["CUR_SECTION"]["MIN_PRICE"] = round($minPrices);	
// 	}
// }


?>
