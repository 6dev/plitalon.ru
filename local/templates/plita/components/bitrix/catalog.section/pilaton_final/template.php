<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 *
 *  _________________________________________________________________________
 * |	Attention!
 * |	The following comments are for system use
 * |	and are required for the component to work correctly in ajax mode:
 * |	<!-- items-container -->
 * |	<!-- pagination-container -->
 * |	<!-- component-end -->
 */

$this->setFrameMode(true);


if (!empty($arResult['NAV_RESULT']))
{
	$navParams =  array(
		'NavPageCount' => $arResult['NAV_RESULT']->NavPageCount,
		'NavPageNomer' => $arResult['NAV_RESULT']->NavPageNomer,
		'NavNum' => $arResult['NAV_RESULT']->NavNum
	);
}
else
{
	$navParams = array(
		'NavPageCount' => 1,
		'NavPageNomer' => 1,
		'NavNum' => $this->randString()
	);
}

$showTopPager = false;
$showBottomPager = false;
$showLazyLoad = false;

if ($arParams['PAGE_ELEMENT_COUNT'] > 0 && $navParams['NavPageCount'] > 1)
{
	$showTopPager = $arParams['DISPLAY_TOP_PAGER'];
	$showBottomPager = $arParams['DISPLAY_BOTTOM_PAGER'];
	$showLazyLoad = $arParams['LAZY_LOAD'] === 'Y' && $navParams['NavPageNomer'] != $navParams['NavPageCount'];
}

$templateLibrary = array('popup', 'ajax', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);

$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
$elementDeleteParams = array('CONFIRM' => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

$positionClassMap = array(
	'left' => 'product-item-label-left',
	'center' => 'product-item-label-center',
	'right' => 'product-item-label-right',
	'bottom' => 'product-item-label-bottom',
	'middle' => 'product-item-label-middle',
	'top' => 'product-item-label-top'
);

$discountPositionClass = '';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
	{
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$labelPositionClass = '';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
	{
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$arParams['~MESS_BTN_BUY'] = $arParams['~MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_BUY');
$arParams['~MESS_BTN_DETAIL'] = $arParams['~MESS_BTN_DETAIL'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_DETAIL');
$arParams['~MESS_BTN_COMPARE'] = $arParams['~MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_COMPARE');
$arParams['~MESS_BTN_SUBSCRIBE'] = $arParams['~MESS_BTN_SUBSCRIBE'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_SUBSCRIBE');
$arParams['~MESS_BTN_ADD_TO_BASKET'] = $arParams['~MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCS_TPL_MESS_BTN_ADD_TO_BASKET');
$arParams['~MESS_NOT_AVAILABLE'] = $arParams['~MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCS_TPL_MESS_PRODUCT_NOT_AVAILABLE');
$arParams['~MESS_SHOW_MAX_QUANTITY'] = $arParams['~MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCS_CATALOG_SHOW_MAX_QUANTITY');
$arParams['~MESS_RELATIVE_QUANTITY_MANY'] = $arParams['~MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCS_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['~MESS_RELATIVE_QUANTITY_FEW'] = $arParams['~MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCS_CATALOG_RELATIVE_QUANTITY_FEW');

$arParams['MESS_BTN_LAZY_LOAD'] = $arParams['MESS_BTN_LAZY_LOAD'] ?: Loc::getMessage('CT_BCS_CATALOG_MESS_BTN_LAZY_LOAD');

$generalParams = array(
	'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
	'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
	'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
	'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
	'MESS_SHOW_MAX_QUANTITY' => $arParams['~MESS_SHOW_MAX_QUANTITY'],
	'MESS_RELATIVE_QUANTITY_MANY' => $arParams['~MESS_RELATIVE_QUANTITY_MANY'],
	'MESS_RELATIVE_QUANTITY_FEW' => $arParams['~MESS_RELATIVE_QUANTITY_FEW'],
	'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
	'USE_PRODUCT_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
	'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
	'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
	'ADD_PROPERTIES_TO_BASKET' => $arParams['ADD_PROPERTIES_TO_BASKET'],
	'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
	'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'],
	'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
	'COMPARE_PATH' => $arParams['COMPARE_PATH'],
	'COMPARE_NAME' => $arParams['COMPARE_NAME'],
	'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
	'PRODUCT_BLOCKS_ORDER' => $arParams['PRODUCT_BLOCKS_ORDER'],
	'LABEL_POSITION_CLASS' => $labelPositionClass,
	'DISCOUNT_POSITION_CLASS' => $discountPositionClass,
	'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
	'SLIDER_PROGRESS' => $arParams['SLIDER_PROGRESS'],
	'~BASKET_URL' => $arParams['~BASKET_URL'],
	'~ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
	'~BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE'],
	'~COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
	'~COMPARE_DELETE_URL_TEMPLATE' => $arResult['~COMPARE_DELETE_URL_TEMPLATE'],
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
	'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
	'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY'],
	'MESS_BTN_BUY' => $arParams['~MESS_BTN_BUY'],
	'MESS_BTN_DETAIL' => $arParams['~MESS_BTN_DETAIL'],
	'MESS_BTN_COMPARE' => $arParams['~MESS_BTN_COMPARE'],
	'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
	'MESS_BTN_ADD_TO_BASKET' => $arParams['~MESS_BTN_ADD_TO_BASKET'],
	'MESS_NOT_AVAILABLE' => $arParams['~MESS_NOT_AVAILABLE']
);

$obName = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $this->GetEditAreaId($navParams['NavNum']));
$containerName = 'container-'.$navParams['NavNum'];
?>


<? if (!empty($arResult['ITEMS'])){ 

	$arSkuTemplate = array();
	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
	$areaIds = array();


	if($arResult['CUR_SECTION']['MEASURE'] == '5'){
        $ratio = "/шт";
    } else{
        $ratio = "/м²";
    }
    ?>

	<section class="card">
		<div class="container">
			<div class="row">
				<div class="print_logo">
					<div class="col-xs-6">
						<img src="https://plitalon.ru/local/templates/plita/images/logo.png" alt="">
					</div>
					<div class="col-xs-6">
						<p>+7965-100-00-90</p>
					</div>
				</div>
				<div class="col-md-11">
					<h1><p class="title2"><? echo $arResult['NAME']; ?></p></h1>
					<div class="code_card">
						<div class="links">
							<? if($arResult['FIX_NEXT_PREV'] == true): ?>
								<? if(strlen($arResult['CUR_SECTION']['PREV']['NAME']) > 0): ?>
									<a href="<? echo $arResult['CUR_SECTION']['PREV']['SECTION_PAGE_URL']; ?>" class="lprev active"><? echo GetMessage('PREV'); ?></a>
								<? else: ?>
									<a href="javascript:void(0);" class="lprev" rel="nofollow noreferrer noopener"><? echo GetMessage('PREV'); ?></a>
								<? endif; ?>

								<? if(strlen($arResult['CUR_SECTION']['NEXT']['NAME']) > 0): ?>				
									<a href="<? echo $arResult['CUR_SECTION']['NEXT']['SECTION_PAGE_URL']; ?>" class="lnext active"><? echo GetMessage('NEXT'); ?></a>
								<? else: ?> 						
									<a href="javascript:void(0);" class="lnext" rel="nofollow noreferrer noopener"><? echo GetMessage('NEXT'); ?></a>
								<? endif; ?>
							<? endif; ?>
						</div>
						<div class="article"></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-7">
					<div class="slider slider-single slickcard">
						<? foreach ($arResult['CUR_SECTION']['RESIMG']['BIG'] as $valPhoto) { ?>
							<div>
								<a data-lightbox="example-set" href="<? echo $valPhoto['REAL_SIZE']['src'] ?>">
									<img src="<? echo $valPhoto['src']; ?>" alt="<?=$file['DESCRIPTION'];?>">
								</a>
							</div>  
						<? } ?>
					</div>
					<div class="slider slider-nav navcard">
						<? foreach ($arResult['CUR_SECTION']['RESIMG']['THUMB'] as $valPhotoThumb) { ?>
							<div>
								<img src="<? echo $valPhotoThumb['src']; ?>" alt="">
							</div>
						<? } ?>
					</div> 

					<? if(($arResult['CUR_SECTION']['UF_HIT'] == 1) || ($arResult['CUR_SECTION']['UF_NEW'] == 1)): ?>
                        <div class="inform coll-detail clearfix">
                            <? if($arResult['CUR_SECTION']['UF_HIT'] == 1){?>
                                <p class="hit"><span>Хит</span></p>
                            <?}?>
                            <? if($arResult['CUR_SECTION']['UF_NEW'] == 1){?>
                                <p class="new"><span>Новинка</span></p>
                            <?}?>
                        </div>
                    <? endif; ?> 
				</div>


				<div class="col-lg-5">
					<div class="specification-wr">
						<div class="specification">
							<p class="cost">
								<span><? echo GetMessage('PRICE_FROM'); ?></span> <strong><? echo $arResult['CUR_SECTION']['MIN_PRICE']; ?> руб.<?=$ratio;?></strong>
								<span class="det_el_like" style="width: auto; float: right;"><a href="javascript:void(0);" class="liked" index-id2="<?=$arResult['CUR_SECTION']['ID'];?>" rel="nofollow noreferrer noopener" style="height: 28px;"></a></span>
							</p>

							<div class="contents">
								<button class="btnmod" data-toggle="modal" data-target="#searchchep"><? echo GetMessage('SHOW_DISC'); ?></button>
							</div>
							<div class="contents">
								<? if(strlen($arResult['PATH'][1]['SECTION_PAGE_URL'] > 0)): ?>
									<p>
										<span><? echo GetMessage('PROIZ'); ?></span>
										<span><a style="color:#40a23c; text-decoration: underline;" href="<? echo $arResult['PATH'][1]['SECTION_PAGE_URL']; ?>"><? echo $arResult['PATH'][1]['NAME']; ?></a></span>	
									</p>
								<? endif; ?>
								<p>
									<span style="float: left;"><? echo GetMessage('POVERH'); ?></span>
									<span style="color:#000000; float: right;">
										<? foreach ($arResult['CUR_SECTION']['POVERHNOST'] as $valPoverhnost) { ?>
											<? if($valPoverhnost == end($arResult['CUR_SECTION']['POVERHNOST'])): ?>
												<? echo $valPoverhnost; ?>
											<? else: ?>
												<? echo $valPoverhnost . ', '; ?>
												
											<? endif; ?>
										<? } ?>
									</span>			
								</p> 	
								<p>
									<span style="float: left;"><? echo GetMessage('FORMAT'); ?></span>
									<span style="color:#000000; float: right;">
										<? 
										$f = 0;
										foreach ($arResult['CUR_SECTION']['FORMAT'] as $valFormat) { ?>
											<? if($f == 5): echo $valFormat; break; endif;?>
			
												<? echo $valFormat . ', '; ?>
											
										<? $f = $f + 1; } ?>
									</span>			
								</p> 	
								<p>
									<span style="float: left;"><? echo GetMessage('TOLSHINA'); ?></span>
									<span style="color:#000000; float: right;">
										<? foreach ($arResult['CUR_SECTION']['TOLSHCHINA'] as $valTol) { ?>
											<? if($valTol == end($arResult['CUR_SECTION']['TOLSHCHINA'])): ?>
												<? echo $valTol; ?>
											<? else: ?>
												<? echo $valTol . ', '; ?>
											<? endif; ?>
										<? } ?>
									</span>			
								</p> 	
								<p>
									<span style="float: left;"><? echo GetMessage('MATERIAL'); ?></span>
									<span style="color:#000000; float: right;">
										<? foreach ($arResult['CUR_SECTION']['MATERIAL'] as $valMat) { ?>
											<? if($valMat == end($arResult['CUR_SECTION']['MATERIAL'])): ?>
												<? echo $valMat; ?>
											<? else: ?>
												<? echo $valMat . ', '; ?>
											<? endif; ?>
										<? } ?>
									</span>	
								</p> 	
								<!-- <p>
									<span style="float: left;"><?// echo GetMessage('SROK_POSTAVKI'); ?></span>
									<span style="color:#000000; float: right;">
										<? //foreach ($arResult['CUR_SECTION']['SROK_POSTAVKI'] as $valPostav) { ?>
											<?// if(end($arResult['CUR_SECTION']['SROK_POSTAVKI']) == $valPostav): ?>
												<?// echo $valPostav; ?>
											<? //else: ?>
												<? //echo $valPostav . ', '; ?>
											<? //endif; ?>
										<?// } ?>
									</span>			
								</p> -->
							</div>

							<div class="contents socials">
								<span style="display:block; margin: 0 0 5px"><? echo GetMessage('SUBSCRIBE'); ?></span>
								<div class="share42init"></div>
								<script type="text/javascript" src="/share42/share42.js"></script>
							</div>
							<div class="downloads">
								<a href="/download/<? echo $arResult['PATH'][1]['CODE']; ?>/"><? echo GetMessage('PDF'); ?></a>
								<a href="javascript:void(0);" onclick="PrintElem('#printblock'); return false;"><? echo GetMessage('PRINT'); ?></a>
							</div>

							<div id="pageControl"></div>
						</div>
					</div>

				</div>						
			</div>

			<div class="row">
				<div class="col-md-12">
					<div class="card-text"><? echo $arResult['~DESCRIPTION']; ?></div>
				</div>
			</div>


			<div class="row">
				<div class="col-md-12">
					<p class="cardtabtitle"><span>Товары коллекции</span></p>
					<div class="card-tabs">
						<? if(count($arResult["CUR_SECTION"]["TIP_TOVARA"]) > 1): ?>
							<div class="buttons__tabs">
								<a href="<? echo $arResult['SECTION_PAGE_URL']; ?>#pageControl" class="<? if((isset($_GET['filtr'])) || !empty($_GET['filtr'])): else: ?>active<? endif; ?>">Все</a>
								<? 
								$i = 1;
								foreach ($arResult["CUR_SECTION"]["TIP_TOVARA"] as $valTipTovara){ 

									$getUrl = "filtr=" . $valTipTovara;
									?>
									<a href="<?echo $APPLICATION->GetCurPageParam($getUrl, array('filtr'));?>#pageControl" <?if($_GET['filtr'] == $valTipTovara):?> class="active"<? endif; ?> ><? echo $valTipTovara; ?></a>

									<? if ($_GET['filtr'] == $valTipTovara){

										$GLOBALS['arrFilters'] = array("IBLOCK_SECTION_ID"=> $arResult['ID'], "=PROPERTY_TIP_TOVARA_VALUE" => $valTipTovara);
									}
									else{
										$GLOBALS['arrFilters'] = array("IBLOCK_SECTION_ID"=> $arResult['ID']);
									}?>
								<? $i = $i + 1; }

								//if (isset($_GET['filtr']) && !empty($_GET['filtr'])){ $category = $_GET['category'];}
										
								?>
							</div>
							<div class="tab-content">
								<? if((isset($_GET['filtr'])) || !empty($_GET['filtr'])):

									$GLOBALS['arrFilters'] = array("IBLOCK_SECTION_ID"=> $arResult['ID'], "=PROPERTY_TIP_TOVARA_VALUE" => $_GET['filtr']);
									
								else:
									$GLOBALS['arrFilters'] = array("IBLOCK_SECTION_ID"=> $arResult['ID']);
								endif;
								

								$APPLICATION->IncludeComponent(
									"bitrix:catalog.section",
									"pilaton_product",
									array(
										"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
										"IBLOCK_ID" => $arParams["IBLOCK_ID"],
										"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
										"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
										"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
										"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
										"PROPERTY_CODE" => (isset($arParams["LIST_PROPERTY_CODE"]) ? $arParams["LIST_PROPERTY_CODE"] : []),
										"PROPERTY_CODE_MOBILE" => $arParams["LIST_PROPERTY_CODE_MOBILE"],
										"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
										"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
										"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
										"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
										"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
										"BASKET_URL" => $arParams["BASKET_URL"],
										"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
										"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
										"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
										"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
										"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
										"FILTER_NAME" => "arrFilters",
										"CACHE_TYPE" => $arParams["CACHE_TYPE"],
										"CACHE_TIME" => $arParams["CACHE_TIME"],
										"CACHE_FILTER" => $arParams["CACHE_FILTER"],
										"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
										"SET_TITLE" => $arParams["SET_TITLE"],
										"MESSAGE_404" => $arParams["~MESSAGE_404"],
										"SET_STATUS_404" => $arParams["SET_STATUS_404"],
										"SHOW_404" => $arParams["SHOW_404"],
										"FILE_404" => $arParams["FILE_404"],
										"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
										"PAGE_ELEMENT_COUNT" => 40,
										"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
										"PRICE_CODE" => $arParams["~PRICE_CODE"],
										"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
										"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

										"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
										"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
										"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
										"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
										"PRODUCT_PROPERTIES" => (isset($arParams["PRODUCT_PROPERTIES"]) ? $arParams["PRODUCT_PROPERTIES"] : []),

										"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
										"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
										"PAGER_TITLE" => $arParams["PAGER_TITLE"],
										"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
										"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
										"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
										"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
										"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
										"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
										"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
										"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
										"LAZY_LOAD" => $arParams["LAZY_LOAD"],
										"MESS_BTN_LAZY_LOAD" => $arParams["~MESS_BTN_LAZY_LOAD"],
										"LOAD_ON_SCROLL" => $arParams["LOAD_ON_SCROLL"],

										"OFFERS_CART_PROPERTIES" => (isset($arParams["OFFERS_CART_PROPERTIES"]) ? $arParams["OFFERS_CART_PROPERTIES"] : []),
										"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
										"OFFERS_PROPERTY_CODE" => (isset($arParams["LIST_OFFERS_PROPERTY_CODE"]) ? $arParams["LIST_OFFERS_PROPERTY_CODE"] : []),
										"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
										"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
										"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
										"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
										"OFFERS_LIMIT" => (isset($arParams["LIST_OFFERS_LIMIT"]) ? $arParams["LIST_OFFERS_LIMIT"] : 0),

										"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
										"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
										"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
										"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
										"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
										'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
										'CURRENCY_ID' => $arParams['CURRENCY_ID'],
										'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
										'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],

										'LABEL_PROP' => $arParams['LABEL_PROP'],
										'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
										'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
										'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
										'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
										'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
										'PRODUCT_ROW_VARIANTS' => $arParams['LIST_PRODUCT_ROW_VARIANTS'],
										'ENLARGE_PRODUCT' => $arParams['LIST_ENLARGE_PRODUCT'],
										'ENLARGE_PROP' => isset($arParams['LIST_ENLARGE_PROP']) ? $arParams['LIST_ENLARGE_PROP'] : '',
										'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
										'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
										'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',

										'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
										'OFFER_TREE_PROPS' => (isset($arParams['OFFER_TREE_PROPS']) ? $arParams['OFFER_TREE_PROPS'] : []),
										'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
										'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
										'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
										'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
										'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
										'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
										'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
										'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
										'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
										'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
										'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
										'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
										'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
										'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
										'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),

										'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
										'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
										'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),

										'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
										"ADD_SECTIONS_CHAIN" => Y,
										'ADD_TO_BASKET_ACTION' => $basketAction,
										'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
										'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
										'COMPARE_NAME' => $arParams['COMPARE_NAME'],
										'USE_COMPARE_LIST' => 'Y',
										'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
										'COMPATIBLE_MODE' => (isset($arParams['COMPATIBLE_MODE']) ? $arParams['COMPATIBLE_MODE'] : ''),
										'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : '')
									),
									$component
								);
								?>
							</div>
						<? else: ?>
								<?
								$GLOBALS['arrFilters'] = array("IBLOCK_SECTION_ID"=> $arResult['ID']);

								$APPLICATION->IncludeComponent(
										"bitrix:catalog.section",
									"pilaton_product",
									array(
										"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
										"IBLOCK_ID" => $arParams["IBLOCK_ID"],
										"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
										"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
										"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
										"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
										"PROPERTY_CODE" => (isset($arParams["LIST_PROPERTY_CODE"]) ? $arParams["LIST_PROPERTY_CODE"] : []),
										"PROPERTY_CODE_MOBILE" => $arParams["LIST_PROPERTY_CODE_MOBILE"],
										"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
										"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
										"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
										"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
										"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
										"BASKET_URL" => $arParams["BASKET_URL"],
										"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
										"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
										"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
										"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
										"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
										"FILTER_NAME" => "arrFilters",
										"CACHE_TYPE" => $arParams["CACHE_TYPE"],
										"CACHE_TIME" => $arParams["CACHE_TIME"],
										"CACHE_FILTER" => $arParams["CACHE_FILTER"],
										"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
										"SET_TITLE" => $arParams["SET_TITLE"],
										"MESSAGE_404" => $arParams["~MESSAGE_404"],
										"SET_STATUS_404" => $arParams["SET_STATUS_404"],
										"SHOW_404" => $arParams["SHOW_404"],
										"FILE_404" => $arParams["FILE_404"],
										"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
										"PAGE_ELEMENT_COUNT" => 40,
										"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
										"PRICE_CODE" => $arParams["~PRICE_CODE"],
										"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
										"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

										"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
										"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
										"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
										"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
										"PRODUCT_PROPERTIES" => (isset($arParams["PRODUCT_PROPERTIES"]) ? $arParams["PRODUCT_PROPERTIES"] : []),

										"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
										"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
										"PAGER_TITLE" => $arParams["PAGER_TITLE"],
										"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
										"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
										"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
										"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
										"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
										"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
										"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
										"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
										"LAZY_LOAD" => $arParams["LAZY_LOAD"],
										"MESS_BTN_LAZY_LOAD" => $arParams["~MESS_BTN_LAZY_LOAD"],
										"LOAD_ON_SCROLL" => $arParams["LOAD_ON_SCROLL"],

										"OFFERS_CART_PROPERTIES" => (isset($arParams["OFFERS_CART_PROPERTIES"]) ? $arParams["OFFERS_CART_PROPERTIES"] : []),
										"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
										"OFFERS_PROPERTY_CODE" => (isset($arParams["LIST_OFFERS_PROPERTY_CODE"]) ? $arParams["LIST_OFFERS_PROPERTY_CODE"] : []),
										"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
										"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
										"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
										"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
										"OFFERS_LIMIT" => (isset($arParams["LIST_OFFERS_LIMIT"]) ? $arParams["LIST_OFFERS_LIMIT"] : 0),

										"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
										"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
										"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
										"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
										"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
										'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
										'CURRENCY_ID' => $arParams['CURRENCY_ID'],
										'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
										'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],

										'LABEL_PROP' => $arParams['LABEL_PROP'],
										'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
										'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
										'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
										'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
										'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
										'PRODUCT_ROW_VARIANTS' => $arParams['LIST_PRODUCT_ROW_VARIANTS'],
										'ENLARGE_PRODUCT' => $arParams['LIST_ENLARGE_PRODUCT'],
										'ENLARGE_PROP' => isset($arParams['LIST_ENLARGE_PROP']) ? $arParams['LIST_ENLARGE_PROP'] : '',
										'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
										'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
										'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',

										'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
										'OFFER_TREE_PROPS' => (isset($arParams['OFFER_TREE_PROPS']) ? $arParams['OFFER_TREE_PROPS'] : []),
										'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
										'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
										'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
										'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
										'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
										'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
										'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
										'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
										'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
										'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
										'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
										'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
										'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
										'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
										'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),

										'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
										'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
										'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),

										'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
										"ADD_SECTIONS_CHAIN" => Y,
										'ADD_TO_BASKET_ACTION' => $basketAction,
										'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
										'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
										'COMPARE_NAME' => $arParams['COMPARE_NAME'],
										'USE_COMPARE_LIST' => 'Y',
										'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
										'COMPATIBLE_MODE' => (isset($arParams['COMPATIBLE_MODE']) ? $arParams['COMPATIBLE_MODE'] : ''),
										'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : '')
									),
									$component
								);
								?>
						<? endif; ?>
					</div>
				</div>
			</div>
		</div>

	</section>
<?}?>


<script type="text/javascript">
$('.slider-single').slick({
 	slidesToShow: 1,
 	slidesToScroll: 1,
 	arrows: true,
 	fade: false,
 	adaptiveHeight: true,
 	infinite: true,
	useTransform: true,
 	speed: 400,
 	cssEase: 'cubic-bezier(0.77, 0, 0.18, 1)',
 });

 $('.slider-nav')
.on('init', function(event, slick) {
	$('.slider-nav .slick-slide.slick-current').addClass('is-active');
})
.slick({
	slidesToShow: 3,
	slidesToScroll: 1,
	dots: false,
	focusOnSelect: false,
	infinite: true,
	responsive: [{
		breakpoint: 1024,
		settings: {
			slidesToShow: 3,
			slidesToScroll: 1,
		}
	}, {
		breakpoint: 640,
		settings: {
			slidesToShow: 4,
			slidesToScroll: 4,
	}
	}, {
		breakpoint: 420,
		settings: {
			slidesToShow: 3,
			slidesToScroll: 3,
}
	}]
});

$('.slider-single').on('afterChange', function(event, slick, currentSlide) {
	$('.slider-nav').slick('slickGoTo', currentSlide);
	var currrentNavSlideElem = '.slider-nav .slick-slide[data-slick-index="' + currentSlide + '"]';
	$('.slider-nav .slick-slide.is-active').removeClass('is-active');
	$(currrentNavSlideElem).addClass('is-active');
});

$('.slider-nav').on('click', '.slick-slide', function(event) {
	event.preventDefault();
	var goToSingleSlide = $(this).data('slick-index');

	$('.slider-single').slick('slickGoTo', goToSingleSlide);
});
</script>

<script type="text/javascript">
	
	function PrintElem(elem)
    {
        Popup($(elem).html());
    }


    function Popup(data){


		var url = window.location.href + '?';

		url=url.substr(0,url.indexOf('#'));
		url=url.substr(0,url.indexOf('?'))+'?print=Y';

        var mywindow = window.open(url,'','height='+$(window).height()+',width='+$(window).width());

        mywindow.focus(); // necessary for IE >= 10
        mywindow.print();
	
		setTimeout(function(){
	
       		mywindow.close();
	
	
		},1000);

       	return true;
    }

</script>