<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

require_once($_SERVER['DOCUMENT_ROOT'].'/local/ajax/filter.php');


use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

global $DD; 

$p=explode('?',$_SERVER['REQUEST_URI']);
$p=explode('/',$p[0]);
$pl=count($p);

$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");

if (((($pl==3) || ($pl==4))  && ($p[1]=='catalog') ) || ($arResult['ID']>0)) 
{
	
if ($DD<=0) {
	
$DD=1;
	
$arFilterAll=$arFilterMain1;

$res = CIBlockElement::GetList($arOrder, $arFilterAll, false, false, $arSelect);
$co_All=$res->SelectedRowsCount();	


$s=0;

$arFilterAllSS=$arFilterAll;

  // со значением свойства SRC, начинающееся с https://
  $arFilterS = Array('IBLOCK_ID'=>CATALOG_IBLOCK_ID, 'DEPTH_LEVEL'=>3);
  $db_list = CIBlockSection::GetList(Array("CATALOG_PRICE_6"=>'asc'), $arFilterS, true);
 while($ar_result = $db_list->GetNext())
  {
	  
$arFilterAllSS['SECTION_ID']=$ar_result['ID'];
$res22 = CIBlockElement::GetList(array('CATALOG_PRICE_6'=>'ASC'), $arFilterAllSS,  false, Array("nPageSize"=>1), false, $arSelect);
if ($res22->SelectedRowsCount()>0) $s=$s+1;	   
	  
  }

$activeElements=$s;













$p=explode('?',$_SERVER['REQUEST_URI']);
$p=explode('/',$p[0]);
	
// 	if ($_COOKIE['koll']
	
?>

<section class="catalog-filters wow fadeIn" data-wow-offset="200">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p class="title title-green">
						<img src="<?=SITE_TEMPLATE_PATH;?>/images/catalogic.png" alt="" style="width: 31px; height: 31px;">
						каталог
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="filterblock">
						<div class="info">
							<div class="infoval">
								<a href="#view_collection" class="infofoods infokol <? if ($_COOKIE['koll']=='1') echo 'active';?>"><?=number_format($activeElements, 0, '', ' ');?> коллекци<?=NumberWordEndingsEx2($activeElements);?></a>
								<a href="#catalog" class="infofoods infoitem <? if ($_COOKIE['koll']!='1') echo 'active';?>"><?=number_format($co_All, 0, '', ' ');?> товар<?=NumberWordEndingsEx($co_All);?></a>
							</div>
							<div class="filterreset">
								<button  OnCLick="window.location.href='/catalog/'" type="reset" class="reset">
									Очистить фильтр
								</button>
							</div>
						</div>
					</div>
					<div class="filter-units clearfix">

						<div class="col-md-3 col-sm-6">
							<div class="unit">
								<div class="utit">
									<p class="tit">
										Каталог
									</p>
									<p class="input">Выберите значение</p>
								</div>
								<div class="selecter js-opener">
									<div class="window-result">
										<p>
											Выбрано: <span></span>
										</p>
										<a href="" OnClick="GetForm(10000); return false;">Показать</a>
									</div>
									<div class="boxik">
									
									
									
<?
$arFilter = array('IBLOCK_ID'=>CATALOG_IBLOCK_ID,'DEPTH_LEVEL'=>1);
  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
  while($ar_result = $db_list->GetNext())
  {			

$arFilterS=$arFilterMain1;

$arFilterS['SECTION_ID']=$ar_result['ID'];

$res = CIBlockElement::GetList(array('CATALOG_PRICE_6'=>'ASC'), $arFilterS,  false, Array("nPageSize"=>1), false, $arSelect);
$co_P=$res->SelectedRowsCount();


?>						
									
									
									
									
									
										<p class="labwr">
											<input OnCLick="if (window.location.href.split('/')[4]!='<?=$ar_result['CODE'];?>') GetForm(10000,10);" <? if (($_REQUEST['SECTION_ID']==$ar_result['ID']) || ($p[2]==$ar_result["CODE"])) echo 'checked';?> code="<?=$ar_result['CODE'];?>" type="radio" class="radio" id="radio<?=$ar_result['ID'];?>" name="SECTION_ID" value="<?=$ar_result['ID'];?>" />
											<label  OnCLick="if (window.location.href.split('/')[4]!='<?=$ar_result['CODE'];?>') GetForm(10000,10);" for="radio<?=$ar_result['ID'];?>">
												<?=$ar_result['NAME'];?> ( <span class="count"><?=$co_P;?></span> )
											</label>
										</p>
										
  <? } ?>									
							
									
									</div>
								</div>
							</div>
						</div>

<?

$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("ACTIVE"=>"Y", "IBLOCK_ID"=>CATALOG_IBLOCK_ID,"FILTRABLE"=>"Y"));
while ($prop_fields = $properties->GetNext())
{
	
	
	if (($prop_fields['PROPERTY_TYPE']=="L") && ($prop_fields['LIST_TYPE']=="L")) {
		
		
		

		
		
		
		
		
		
		
		
	
?>
						<div class="col-md-3 col-sm-6">
							<div class="unit">
								<div class="utit">
									<p class="tit">
										<?=$prop_fields['NAME'];?>
									</p>
									<p class="input">Выберите значение</p>
								</div>
								<div class="selecter js-opener">
									<div class="window-result">
										<p>
											Выбрано: <span></span>
										</p>
										<a href=""  OnClick="GetForm(10000); return false;" >Показать</a>
									</div>
									<div class="boxik">
									
									
	<?									
$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID, "CODE"=>$prop_fields['CODE']));
while($enum_fields = $property_enums->GetNext())
{			

$checked='';			

// Найти колличество элементов 


$arFilterP=$arFilterMain1;
if  (!in_array($enum_fields['ID'],$arFilterP["PROPERTY_".$prop_fields['CODE']])) $arFilterP["PROPERTY_".$prop_fields['CODE']][$enum_fields['ID']]=$enum_fields['ID'];
	
$res = CIBlockElement::GetList(array('CATALOG_PRICE_6'=>'ASC'), $arFilterP,  false, Array("nPageSize"=>1), false, $arSelect);
$co_P=$res->SelectedRowsCount();
	


		
?>				

					
									
	<?
	
	
	if (in_array($enum_fields['ID'],$_REQUEST['PROPERTY_'.$prop_fields['CODE']])) $checked='checked';?>								
									
			<? if ($co_P>0) { ?>						
									
									
										<p class="labwr">
											<input type="radio" <?=$checked;?> class="radio" id="radio<?=$prop_fields['ID'];?><?=$enum_fields['ID'];?>"  value="<?=$enum_fields['ID'];?>" name="PROPERTY_<?=$prop_fields['CODE'];?>[<?=$enum_fields['ID'];?>]" />
			<label  OnClick="if ($(this).parent('.labwr').find('.radio').attr('checked')==undefined) {
				
				var id=$(this).parent('.labwr').find('.radio').attr('id');
				
	if ($.cookie(id)<=0){ 
$.cookie(id,10,{ expires: 7, path: '/'});
	} else {
$.cookie(id,0,{ expires: 7, path: '/'});
	}		
				
		
console.log('Чекбокс меняем id = '+id+' checked: '+$.cookie(id));

if ($.cookie(id)==10) {
	$(this).parent('.labwr').find('.radio').attr('checked',false);
	return false;
}


		
			} if ($(this).parent('.labwr').find('.radio').attr('checked')) { $(this).parent('.labwr').find('.radio').attr('checked',false); return false; } "  for="radio<?=$prop_fields['ID'];?><?=$enum_fields['ID'];?>"> 
											
												<?=$enum_fields['VALUE'];?> ( <span class="count"><?=$co_P;?></span> )
											</label>
										</p>
									
									
	<? } ?>	<? } ?>											
									
									</div>
								</div>
							</div>
						</div>
						
<? } ?>						
						
						

						

		<?
		
if ((($prop_fields['PROPERTY_TYPE']=="S") || ($prop_fields['PROPERTY_TYPE']=="N") ) && ($prop_fields['ID']!=136) && ($prop_fields['ID']!=137) )  {					

	?>					

						<div class="col-md-3 col-sm-6">
							<div class="unit">
								<div class="utit">
									<p class="tit">
										<?=$prop_fields['NAME'];?>
									</p>
									<p class="input">Выберите значение</p>
								</div>
								<div class="selecter js-opener">
									<div class="window-result">
										<p>
											Выбрано: <span></span>
										</p>
										<a href="">Показать</a>
									</div>

									<div class="boxik bt1">
										<div class="rangeflex">
											<div class="el el1">
												<p>
													Толщина (см)
												</p>
											</div>
											<div class="el el2">
												<span class="catformcostl"></span>
													<div id="slider-range4" class="slrange"></div>
												<span class="catformcostr"></span>
												<input type="hidden">
											</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>

<? } ?>	




<? if ((($prop_fields['PROPERTY_TYPE']=="S") || ($prop_fields['PROPERTY_TYPE']=="N") ) && ($prop_fields['ID']==136)  )  { ?>						



<div class="col-md-3 col-sm-6">
							<div class="unit">
								<div class="utit">
									<p class="tit">
										Размер
									</p>
									<p class="input">Выберите значение</p>
								</div>
								<div class="selecter js-opener">
									<div class="window-result">
										<p>
											Выбрано: <span></span>
										</p>
										<a href="">Показать</a>
									</div>

									<div class="boxik bt1">
										<div class="rangeflex">
											<div class="el el1">
												<p>
													<?=$prop_fields['NAME'];?>
												</p>
											</div>
											<div class="el el2">
												<span class="catformcostl"></span>
													<div id="slider-range1" class="slrange"></div>
												<span class="catformcostr"></span>
												<input type="hidden">
											</div>
										</div>
									</div>
									
								


<? } ?>


<? if ((($prop_fields['PROPERTY_TYPE']=="S") || ($prop_fields['PROPERTY_TYPE']=="N") ) && ($prop_fields['ID']==137)  )  { ?>

<div class="boxik bt1">
										<div class="rangeflex">
											<div class="el el1">
												<p>
													<?=$prop_fields['NAME'];?>
												</p>
											</div>
											<div class="el el2">
												<span class="catformcostl"></span>
													<div id="slider-range2" class="slrange"></div>
												<span class="catformcostr"></span>
												<input type="hidden">
											</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>


						
<? } ?>


<? } ?>




	<div class="col-md-3 col-sm-6">
							<div class="unit">
								<div class="utit">
									<p class="tit">
										Бренд
									</p>
									<p class="input">Выберите значение</p>
								</div>
								<div class="selecter js-opener">
									<div class="window-result">
										<p>
											Выбрано: <span></span>
										</p>
										<a href="">Показать</a>
									</div>
									<div class="boxik">
									
									
									
<?

$si=array();
$i=array();

$arFilter = array('IBLOCK_ID'=>CATALOG_IBLOCK_ID,'DEPTH_LEVEL'=>2);
  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
  while($ar_result = $db_list->GetNext())
  {	

if (!in_array($ar_result['NAME'],$i)) {
	$i[]=$ar_result['NAME'];
	$si[]=$ar_result;
}

  }
  
foreach ($si as $n)  {



$arFilter = array('IBLOCK_ID'=>CATALOG_IBLOCK_ID,'DEPTH_LEVEL'=>2,'ID'=>$n);
  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
  while($ar_result = $db_list->GetNext())
  {							

				




?>						
									
									
									
									
									
										<p class="labwr">
											<input type="radio" class="radio" id="radio<?=$ar_result['ID'];?>" name="SECTION_ID2" value="<?=$ar_result['ID'];?>" />
											<label for="radio<?=$ar_result['ID'];?>">
												<?=$ar_result['NAME'];?> ( <span class="count">0</span> )
											</label>
										</p>
										
  <? } ?>	
<? } ?>	  
										
									
									</div>
								</div>
							</div>
						</div>










						

					

						<div class="col-md-3 col-sm-6">
							<div class="unit">
								<div class="utit">
									<div class="rwrcost">
										<p class="tit">
											Цена
										</p>
										<div class="flexrangecost">
											<input type="text" class="catformcostl">
											<input type="text" class="catformcostr">
											<div id="slider-range3"></div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-3 col-sm-6">
							<div class="unit">
								<button class="btnsendfilter" OnClick="GetForm(10000);">
									показать
								</button>
							</div>
						</div>

					</div>

					<!-- end filter-units -->
					
					
						<?
							$url=count(explode('&',$_SERVER['REQUEST_URI']));
							$b1=0;$b2=0;$b3=0;$b4=0;
							
							if (($url>1) && ($_GET['PROPERTY_HIT']!='Y')) { $ph2=$_SERVER['REQUEST_URI'].'&PROPERTY_HIT=Y'; $b2=0;}
							if (($url>1) && ($_GET['PROPERTY_HIT']=='Y')) { $ph2=str_replace('&PROPERTY_HIT=Y','',$_SERVER['REQUEST_URI']); $b2=10;};
			
							if (($url>1) && ($_GET['PROPERTY_NEW']!='Y')) { $ph1=$_SERVER['REQUEST_URI'].'&PROPERTY_NEW=Y'; $b1=0;}
							if (($url>1) && ($_GET['PROPERTY_NEW']=='Y')) { $ph1=str_replace('&PROPERTY_NEW=Y','',$_SERVER['REQUEST_URI']); $b1=10;};
							
							if (($url>1) && ($_GET['PROPERTY_SALE']!='Y')) { $ph3=$_SERVER['REQUEST_URI'].'&PROPERTY_SALE=Y'; $b3=0;}
							if (($url>1) && ($_GET['PROPERTY_SALE']=='Y')) { $ph3=str_replace('&PROPERTY_SALE=Y','',$_SERVER['REQUEST_URI']); $b3=10;};
							
							if (($url>1) && ($_GET['SORT_PRICE']!='asc')) { $ph4=$_SERVER['REQUEST_URI'].'&SORT_PRICE=asc'; $b4=0;}
							if (($url>1) && ($_GET['SORT_PRICE']=='asc')) { $ph4=str_replace('&SORT_PRICE=asc','',$_SERVER['REQUEST_URI']); $b4=10;};
							
							
							?>
					

					<div class="sortingfilter">
						<div class="el el1">
							<p>
								Сортировать по:
								<a class="sort" id='price' href="#price" <? if ($b4>0) { ?> style="font-weight: bold;"<? } ?> >цене</a>
							</p>
						</div>
						<div class="el el2">
							<p class="selfil selfil1">
						
								<a <? if ($_COOKIE['new']=='Y') echo 'style="font-weight:900"';?> OnClick="if ($.cookie('new')!='Y') { $(this).css('font-weight',900); $.cookie('new','Y',{ expires: 7, path: '/'}); } else { $(this).css('font-weight',100); $.cookie('new','N',{ expires: 7, path: '/'}); } GetForm(10000); return false;"  href="#new" class="sort" id='news' <? if ($b1>0) { ?> style="font-weight: bold;"<? } ?>>Новинки</a>
							</p>
							<p class="selfil selfil2">
								<a <? if ($_COOKIE['hit']=='Y') echo 'style="font-weight:900"';?>   OnClick="if ($.cookie('hit')!='Y') { $(this).css('font-weight',900); $.cookie('hit','Y',{ expires: 7, path: '/'}); } else { $(this).css('font-weight',100); $.cookie('hit','N',{ expires: 7, path: '/'}); } GetForm(10000); return false;"  href="#hit"  class="sort" id='hit' <? if ($b2>0) { ?> style="font-weight: bold;"<? } ?>>Хиты продаж</a>
							</p>
							<p class="selfil selfil3">
								<a <? if ($_COOKIE['sale']=='Y') echo 'style="font-weight:900"';?>   OnClick="if ($.cookie('sale')!='Y') { $(this).css('font-weight',900);  $.cookie('sale','Y',{ expires: 7, path: '/'}); } else { $(this).css('font-weight',100);  $.cookie('sale','N',{ expires: 7, path: '/'}); } GetForm(10000); return false;"  href="#sale"  class="sort" id='skidka' <? if ($b3>0) { ?> style="font-weight: bold;"<? } ?>>Со скидкой</a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>




	<section class="catalog_container wow" data-wow-offset="200">
		<div class="container">
			<div class="row">
				<div id="catalog_items" class="col-md-12 contkoll" <? if ($_COOKIE['koll']<=0) { ?> style="display:block;" <? } ?><? if ($_COOKIE['koll']>0) { ?> style="display:none;" <? } ?>>




<i> <?
 




?>
</i>

<?

$ELEMENT_SORT_FIELD='id';

if ($_GET['SORT_PRICE']!='') $ELEMENT_SORT_FIELD='CATALOG_PRICE_6';

?>




<?

print_r($arFilterMain1);
echo '<br><br>';

require_once($_SERVER['DOCUMENT_ROOT'].'/local/ajax/catalog.php');

?>








<?
/*
	$APPLICATION->IncludeComponent(
    "bitrix:catalog.section",
    "kolls",
    Array(
	"DISPLAY_TOP_PAGER" => "Y",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_SHOW_ALL" => "Y",
        "ACTION_VARIABLE" => "action",
		"FILTER_NAME"=>"arFilterMain1",
        "ADD_PICT_PROP" => "MORE_PHOTO",
        "ADD_PROPERTIES_TO_BASKET" => "Y",
        "ADD_TO_BASKET_ACTION" => "ADD",
        "BASKET_URL" => "/personal/basket.php",
        "BRAND_PROPERTY" => "BRAND_REF",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "N",
        "COMPARE_NAME" => "CATALOG_COMPARE_LIST",
        "COMPARE_PATH" => "",
        "COMPATIBLE_MODE" => "N",
        "CONVERT_CURRENCY" => "Y",
        "CURRENCY_ID" => "RUB",
        "DATA_LAYER_NAME" => "популярно",
        "DETAIL_URL" => "",
		"SECTION_ID"=>$arFilterMain1['SECTION_ID'],
        "DISCOUNT_PERCENT_POSITION" => "bottom-right",
        "DISPLAY_COMPARE" => "N",
        "ELEMENT_COUNT" => $COUNT_TOP,
		"ELEMENT_SORT_FIELD" => $ELEMENT_SORT_FIELD,
        "ELEMENT_SORT_ORDER" => "asc",
        "HIDE_NOT_AVAILABLE" => "L",
        "HIDE_NOT_AVAILABLE_OFFERS" => "L",
        "IBLOCK_ID" => CATALOG_IBLOCK_ID,
        "IBLOCK_TYPE" => CATALOG_IBLOCK_TYPE,
        "LABEL_PROP" => array("SALELEADER"),
        "LABEL_PROP_MOBILE" => array(),
        "LABEL_PROP_POSITION" => "top-left",
        "LINE_ELEMENT_COUNT" => "",
        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
        "MESS_BTN_BUY" => "Купить",
        "MESS_BTN_COMPARE" => "Сравнить",
        "MESS_BTN_DETAIL" => "Подробнее",
        "MESS_NOT_AVAILABLE" => "Нет в наличии",
        "MESS_RELATIVE_QUANTITY_FEW" => "мало",
        "MESS_RELATIVE_QUANTITY_MANY" => "много",
        "MESS_SHOW_MAX_QUANTITY" => "Наличие",
        "OFFERS_CART_PROPERTIES" => array("COLOR_REF","SIZES_SHOES","SIZES_CLOTHES"),
        "OFFERS_FIELD_CODE" => array("",""),
        "OFFERS_LIMIT" => 12,
        "OFFERS_PROPERTY_CODE" => array(
			0 => "POVERKHNOST",
			1 => "FORMAT",
			2 => "TOLSHCHINA",
		),
        "OFFERS_SORT_FIELD" => "sort",
        "OFFERS_SORT_FIELD2" => "id",
        "OFFERS_SORT_ORDER" => "asc",
        "OFFERS_SORT_ORDER2" => "desc",
        "OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
        "OFFER_TREE_PROPS" => array("COLOR_REF","SIZES_SHOES"),
        "PARTIAL_PRODUCT_PROPERTIES" => "N",
        "PRICE_CODE" => array(
			0 => "BASE",
			1 => "OLD",
			2 => "EURO",
			3 => "OLDEURO",
		),
        "PRICE_VAT_INCLUDE" => "Y",
        "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
        "PRODUCT_DISPLAY_MODE" => "Y",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRODUCT_PROPERTIES" => array("NEWPRODUCT"),
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "PRODUCT_QUANTITY_VARIABLE" => "",
        "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
        "PRODUCT_SUBSCRIPTION" => "Y",
        "PROPERTY_CODE" => array("MANUFACTURER","MATERIAL",""),
        "PROPERTY_CODE_MOBILE" => array(),
        "RELATIVE_QUANTITY_FACTOR" => "5",
        "ROTATE_TIMER" => "30",
        "SECTION_URL" => "",
        "SEF_MODE" => "N",
        "SEF_RULE" => "",
        "SHOW_CLOSE_POPUP" => "N",
        "SHOW_DISCOUNT_PERCENT" => "Y",
        "SHOW_MAX_QUANTITY" => "M",
        "SHOW_OLD_PRICE" => "Y",
        "SHOW_PAGINATION" => "Y",
        "SHOW_PRICE_COUNT" => "1",
        "SHOW_SLIDER" => "Y",
        "SLIDER_INTERVAL" => "3000",
        "SLIDER_PROGRESS" => "N",
        "TEMPLATE_THEME" => "blue",
        "USE_ENHANCED_ECOMMERCE" => "Y",
        "USE_PRICE_COUNT" => "N",
        "USE_PRODUCT_QUANTITY" => "Y",
        "VIEW_MODE" => "SECTION"
    )
);

*/

?>

</div>

<div class="col-md-12 contkolln" <? if ($_COOKIE['koll']>0) { ?> style="display:block;" <? } ?><? if ($_COOKIE['koll']<=0) { ?> style="display:none;" <? } ?>>


<?

$arFilterAllSS=$arFilterMain1;
$as=array();
  $arFilterS = Array('IBLOCK_ID'=>CATALOG_IBLOCK_ID, 'DEPTH_LEVEL'=>3);
  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilterS, true);
 while($ar_result = $db_list->GetNext())
  {
	  
$arFilterAllSS['SECTION_ID']=$ar_result['ID'];
$res22 = CIBlockElement::GetList(array('CATALOG_PRICE_6'=>'ASC'), $arFilterAllSS,  false, Array("nPageSize"=>1), false, $arSelect);
if ($res22->SelectedRowsCount()>0) $as[]=$ar_result['ID'];	   
$arD=10;  
  }

global $arFSectionS;
 
global $arD;


$arFSectionS=$as;

$APPLICATION->IncludeComponent(
   "bitrix:catalog.section.list", 
   "kolls", 
   array(
      "VIEW_MODE" => "TEXT",
        "SHOW_PARENT_NAME" => "Y",
        "IBLOCK_TYPE" => "",
		"FILTER_NAME"=>"arFSectionS",
        "IBLOCK_ID" => CATALOG_IBLOCK_ID,
        "SECTION_ID" => false,
        "SECTION_CODE" => "",
        "SECTION_URL" => "",
        "COUNT_ELEMENTS" => "Y",
        "TOP_DEPTH" => "3",
        "SECTION_FIELDS" => "",
        "SECTION_USER_FIELDS" => "",
        "ADD_SECTIONS_CHAIN" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_NOTES" => "",
        "CACHE_GROUPS" => "Y"
   ),
   false
);
?>

			
					
	



	
					
					
				</div>
				
				
				
				
				
			</div>
		</div>
	</section>


<?

$navResult->NavShowAll=10;

$APPLICATION->IncludeComponent('bitrix:system.pagenavigation', '', array(
    'NAV_RESULT' => $navResult,
));
?>


	<section class="contentblock wow fadeIn" data-wow-offset="200">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p>
					
<?

$textd='';
  
  if ($arFilterMain1['SECTION_ID']>0) {
  
  $arFilter = Array('IBLOCK_ID'=>CATALOG_IBLOCK_ID, 'ID'=>$arFilterMain1['SECTION_ID']);
  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
  while($ar_result = $db_list->GetNext())
  {
   
   $textd=$textd.$ar_result['NAME'];
  }
  }
  
  foreach ($arFilterMain1 as $k=>$p)
  {
	  $pp=explode('PROPERTY_',$k);
	  if (count($pp)>1) {
		  $pp=$pp[1];
		  
	// Ищем для него название свойства 

$properties = CIBlockProperty::GetList(Array("sort"=>"asc", "name"=>"asc"), Array("CODE"=>$pp,"ACTIVE"=>"Y", "IBLOCK_ID"=>CATALOG_IBLOCK_ID));
while ($prop_fields = $properties->GetNext())
{
if ($prop_fields['NAME']!='') $textd=$textd.', '.$prop_fields['NAME'].':';
	
	// Ищем варианты значений для
	
	$vv=0;
	foreach ($p as $v) {
		$vv=$vv+1;
	$property_enums = CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID, "CODE"=>$pp,"ID"=>$v));
while($enum_fields = $property_enums->GetNext())
{
	
if ($enum_fields['VALUE']!='')	{
	if ($vv==count($p)) $textd=$textd.$enum_fields['VALUE'];
	if ($vv<count($p))  $textd=$textd.$enum_fields['VALUE'].', ';
}
	
}

	}
	
	
	
	
	
}
	
		  
	  }
  }


?>					
					
						<?=$textd;?> <?=\COption::GetOptionString( "askaron.settings", "UF_TEXT");?> 
					</p>
			
				</div>
			</div>
		</div>
	</section>
	
	
<script>

function GetForm(aaa,bbb)
{
	
	console.log('Старт формы поиска');
	
	
	// Смотри  в свойстве сколько вариантов 
	
	if (window.location.href.split('/')[4]=='')  {
		
		// $('.boxik').not(':first').remove();
		// $('.selecter').not(':first').remove();
		
		$('p.input').not(':first').css('color','#d0ccce;');
		
	} else {
		
	$('p.input').not(':first').css('color','#000000;');	
		
	}

 // Пройтись по все форме запросив 
  
var url='';  
  
  
  
  
  
 
  $('.radio:checked').each(function(i,elem) {

var  val=$(this).val();
var  nm=$(this).attr('name');

if (nm!='SECTION_ID') { 
	
if (nm=='SECTION_ID2') nm='SECTION_ID';	
	
	url=url+nm+'='+val+'&';
}

});

console.log('Чекбоксы: '+url);

// Толщина

var n_max=Number($("#slider-range4").parent('.el').find('.catformcostr').html());
var n_min=Number($("#slider-range4").parent('.el').find('.catformcostl').html());
var max_o=$( "#slider-range4").slider( 'option' , 'max');
var min_o=$( "#slider-range4").slider( 'option' , 'min');

if ((n_min!=min_o) && (n_min>0))  url=url+"FROM_TOLSHCHINA="+$("#slider-range4").parent('.el').find('.catformcostl').html()+'&';
 if ((n_max!=max_o) && (n_max>0)) url=url+"TO_TOLSHCHINA="+$("#slider-range4").parent('.el').find('.catformcostr').html()+'&';

// Ширина

var n_max=Number($("#slider-range1").parent('.el').find('.catformcostr').html());
var n_min=Number($("#slider-range1").parent('.el').find('.catformcostl').html());
var max_o=$( "#slider-range1").slider( 'option' , 'max');
var min_o=$( "#slider-range1").slider( 'option' , 'min');

if ((n_min!=min_o) && (n_min>0))  url=url+"FROM_SHIRINA="+$("#slider-range1").parent('.el').find('.catformcostl').html()+'&';
 if ((n_max!=max_o) && (n_max>0)) url=url+"TO_SHIRINA="+$("#slider-range1").parent('.el').find('.catformcostr').html()+'&';
 
 
 
 
 // Длина

var n_max=Number($("#slider-range2").parent('.el').find('.catformcostr').html());
var n_min=Number($("#slider-range2").parent('.el').find('.catformcostl').html());
var max_o=$( "#slider-range2").slider( 'option' , 'max');
var min_o=$( "#slider-range2").slider( 'option' , 'min');

if ((n_min!=min_o) && (n_min>0))  url=url+"FROM_DLINA="+$("#slider-range2").parent('.el').find('.catformcostl').html()+'&';
 if ((n_max!=max_o) && (n_max>0)) url=url+"TO_DLINA="+$("#slider-range2").parent('.el').find('.catformcostr').html()+'&';
 
 
  // Цена

var n_max=Number($("#slider-range3").parent('.flexrangecost').find('.catformcostr').val());
var n_min=Number($("#slider-range3").parent('.flexrangecost').find('.catformcostl').val());
var max_o=$( "#slider-range3").slider( 'option' , 'max');
var min_o=$( "#slider-range3").slider( 'option' , 'min');

if ((n_min!=min_o) && (n_min>0))  url=url+"FROM_PRICE="+$("#slider-range3").parent('.flexrangecost').find('.catformcostl').val()+'&';
if ((n_max!=max_o) && (n_max>0)) url=url+"TO_PRICE="+$("#slider-range3").parent('.flexrangecost').find('.catformcostr').val()+'&';
 
 
 
 
 
 
 
 
 
 


// Цена
//var n_max=Number($("#slider-range3").parent('.flexrangecost').find('.catformcostr').val());
//var n_min=Number($("#slider-range3").parent('.flexrangecost').find('.catformcostl').val());
//var max_o=$( "#slider-range3").slider( 'option' , 'max');
//var min_o=$( "#slider-range3").slider( 'option' , 'min');

//if ((n_min!=min_o) && (n_min>0))  url=url+"FROM_PRICE="+$("#slider-range3").parent('.flexrangecost').find('.catformcostl').val()+'&';
//if ((n_max!=max_o) && (n_max>0))   url=url+"TO_PRICE="+$("#slider-range3").parent('.flexrangecost').find('.catformcostr').val()+'&';

console.log('Ссылка'+url);




if (bbb>0)
{
	url='';
}

var si=url.substr(url.length-1,1);
if (si=='&') url=url.substr(0,url.length-1);
 
if (aaa>1000)
{
	
	var pa=$('[name=SECTION_ID]:checked').attr('code');
	
<?
$p=explode('?',$_SERVER['REQUEST_URI']);
$p=explode('/',$p[0]);
?> 
	
	
	if ((pa!='') && (pa!=undefined)) 
	{	
	pa=pa+'/';
	
	if (url!='') {
		window.history.pushState("object or string", "Title", '/<?=$p[1];?>/'+pa+'?'+url);

$.get( "/local/ajax/catalog.php?"+url).done(function(data) {
    $('#catalog_items').html(data);
	GetC();
  });
 
	}
	if (url=='') window.history.pushState("object or string", "Title", '/<?=$p[1];?>/'+pa);
	} else {
	if (url!='') {
	window.history.pushState("object or string", "Title", '/<?=$p[1];?>/'+'?'+url);
	$.get( "/local/ajax/catalog.php?"+url).done(function(data) {
    $('#catalog_items').html(data);
	GetC();
  });
  
	}
	if (url=='') window.history.pushState("object or string", "Title", '/<?=$p[1];?>/');	
	}
	 
 	
	 
	
}


   
$.get( "/local/ajax/get.php?filter=Y&"+url+"&aaa="+aaa, function(data) {
})
  .done(function(data2) {  
  


console.log(data2);

var obj = jQuery.parseJSON(data2);

$('.window-result').find('span').html(obj.COUNT); // Общее колличество


$('.infoitem').html(obj.COUNT+" "+obj.COUNT_TEXT);
$('.infokol').html(obj.COUNT2+" "+obj.COUNT_TEXT2);







$.each(obj.SECTIONS2,function(index,value){
	
if (Number(value)<=0)
{
	console.log('77777');
$('[for=radio'+index+']').find('.count').html(0);
}

if (Number(value)>0) {
	console.log('12345'+value);
$('[for=radio'+index+']').find('.count').html(value);
}

});






$.each(obj.SECTIONS,function(index,value){
	
if (Number(value)<=0)
{
	
$('[name=SECTION_ID]').eq(index).parent('.labwr').find('.count').html(0);

}
if (Number(value)>0) {
	
$('[name=SECTION_ID]').eq(index).parent('.labwr').find('.count').html(value);
	
}

});



$.each(obj.COUNTS,function(index,value){

if (Number(value)<=0)
{
	
$('#radio'+index).parent('.labwr').addClass('nothis');
$('[for=radio'+index+']').find('.count').html(0)

}
if (Number(value)>0) {
	
	$('#radio'+index).parent('.labwr').removeClass('nothis');	
	$('[for=radio'+index+']').find('.count').html(value);
	
}
	

});






  });
  
  
 

}

$( ".catalog-filters .filter-units .radio").click(function() {
GetForm(1);
});


 




 $(window).on('load', function () {
  // Run code
 
setTimeout(function(){
	
	
GetForm(1);		




$(':checked').click();






  
  

}, 500); 
  
  
});


 

$( ".infofoods").click(function() {
	
	
$( ".infofoods").removeClass('active');
$(this).addClass('active');

if ($(this).hasClass('infokol')) {
	
	$('.contkolln').show();
	$('.contkoll').hide();	
	
	$.cookie('koll','1',{ expires: 7, path: '/'});
}
if ($(this).hasClass('infoitem')) {
	
	
	$('.contkoll').show();
	$('.contkolln').hide();
	
	$.cookie('koll','0',{ expires: 7, path: '/'});
}


	
	return false;
	
});

$( ".sort").click(function() {
var ida=$(this).attr('id');

if ((ida=="price") || $.cookie('price')=='DESC') {

	
	$.cookie('price',"ASC",{ expires: 7, path: '/'});
} else if ((ida=="price") && $.cookie('price')=='ASC') {
	$.cookie('price',"DESC",{ expires: 7, path: '/'});
	

}


});

// Код цифровых слайдеров

function sliderRangeCustom(){
  $( "#slider-range1" ).slider({
    range: true,
    min: 0,
    max: <?=$C1;?>,
    values: [ <?=$_REQUEST['FROM_SHIRINA'];?>, <?=$C11;?> ],
    slide: function( event, ui ) {
      $( "#slider-range1" ).closest('.unit').find( ".input" ).text( 
    'Ширина от '+ ui.values[ 0 ]+ ' до ' + ui.values[ 1 ]+'мм.');

      $( "#slider-range1" ).closest('.rangeflex').
      find( ".catformcostl" ).text(ui.values[ 0 ]);
      $( "#slider-range1" ).closest('.rangeflex').
      find( ".catformcostr" ).text(ui.values[ 1 ]);
    }
  });
  $( "#slider-range1" ).closest('.rangeflex').
  find( ".catformcostl" ).text( $( "#slider-range1" ).slider( "values", 0 ));
  $( "#slider-range1" ).closest('.rangeflex').
  find( ".catformcostr" ).text( $( "#slider-range1" ).slider( "values", 1 ));

  $( "#slider-range2" ).slider({
    range: true,
    min: 0,
    max: <?=$C2;?>,
    step:1,
    values: [ <?=$_REQUEST['FROM_DLINA'];?>, <?=$C22;?> ],
    slide: function( event, ui ) {
      $( "#slider-range2" ).closest('.unit').find( ".input" ).text( 
    'Длина от '+ ui.values[ 0 ]+ ' до ' + ui.values[ 1 ]+'мм.');

      $( "#slider-range2" ).closest('.rangeflex').find( ".catformcostl" ).
      text(ui.values[ 0 ]);
      $( "#slider-range2" ).closest('.rangeflex').find( ".catformcostr" ).
      text(ui.values[ 1 ]);
    }
  });
  $( "#slider-range2" ).closest('.rangeflex').
  find( ".catformcostl" ).text( $( "#slider-range2" ).slider( "values", 0 ));
  $( "#slider-range2" ).closest('.rangeflex').
  find( ".catformcostr" ).text( $( "#slider-range2" ).slider( "values", 1 ));


  $( "#slider-range3" ).slider({
    range: true,
    min: 0,
    max: <?=round($C3);?>,
    step:1,
    values: [ 0, <?=round($C33);?> ],
    slide: function( event, ui ) {


      $( "#slider-range3" ).closest('.flexrangecost').find( ".catformcostl" ).
      val(ui.values[ 0 ]);
      $( "#slider-range3" ).closest('.flexrangecost').find( ".catformcostr" ).
      val(ui.values[ 1 ]);
    }
  });
  $( "#slider-range3" ).closest('.flexrangecost').
  find( ".catformcostl" ).val( $( "#slider-range3" ).slider( "values", 0 ));
  $( "#slider-range3" ).closest('.flexrangecost').
  find( ".catformcostr" ).val( $( "#slider-range3" ).slider( "values", 1 ));

  $( "#slider-range4" ).slider({
    range: true,
    min: 0,
    max: <?=$C4;?>,
    step:1,
    values: [ <?=$_REQUEST['FROM_TOLSHCHINA'];?>, <?=$C44;?> ],
    slide: function( event, ui ) {
      $( "#slider-range4" ).closest('.unit').find( ".input" ).text( 
    'Толщина‚от '+ ui.values[ 0 ]+ ' до ' + ui.values[ 1 ]+' мм');

      $( "#slider-range4" ).closest('.rangeflex').find( ".catformcostl" ).
      text(ui.values[ 0 ]);
      $( "#slider-range4" ).closest('.rangeflex').find( ".catformcostr" ).
      text(ui.values[ 1 ]);
    }
  });
  $( "#slider-range4" ).closest('.rangeflex').
  find( ".catformcostl" ).text( $( "#slider-range4" ).slider( "values", 0 ));
  $( "#slider-range4" ).closest('.rangeflex').
  find( ".catformcostr" ).text( $( "#slider-range4" ).slider( "values", 1 ));


  $( "#slider-range5" ).slider({
    range: true,
    min: 0,
    max: 80,
    step:1,
    values: [ 0, 80 ],
    slide: function( event, ui ) {
      $( "#slider-range5" ).closest('.unit').find( ".input" ).text( 
    'РўРѕР»С‰РёРЅР° РѕС‚ '+ ui.values[ 0 ]+ ' РґРѕ ' + ui.values[ 1 ]);

      $( "#slider-range5" ).closest('.rangeflex').find( ".catformcostl" ).
      text(ui.values[ 0 ]);
      $( "#slider-range5" ).closest('.rangeflex').find( ".catformcostr" ).
      text(ui.values[ 1 ]);
    }
  });
  $( "#slider-range5" ).closest('.rangeflex').
  find( ".catformcostl" ).text( $( "#slider-range5" ).slider( "values", 0 ));
  $( "#slider-range5" ).closest('.rangeflex').
  find( ".catformcostr" ).text( $( "#slider-range5" ).slider( "values", 1 ));

}



sliderRangeCustom();


$( "#slider-range3" ).slider({
    change: function( event, ui ) { 
console.log('Выбор в слайдере 5');
GetForm();
	}
});

$( "#slider-range4" ).slider({
    change: function( event, ui ) { 
console.log('Выбор в слайдере 4');
GetForm();
	}
});


$( "#slider-range1" ).slider({
    change: function( event, ui ) { 
console.log('Выбор в слайдере 1');
GetForm();
	}
});

$( "#slider-range2" ).slider({
    change: function( event, ui ) { 
console.log('Выбор в слайдере 2');
GetForm();
	}
});


</script>
	

<?

} // Если $DD

} else {
	

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
header('Location: /404.php',301);

}

	
?>