<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$key = 0;
foreach ($arResult['ITEMS'] as $key => $arItem){ 

	if(strlen($arItem['DETAIL_PICTURE']['SRC']) > 0):

		if($arItem['DETAIL_PICTURE']['HEIGHT'] > $arItem['DETAIL_PICTURE']['WIDTH']):

			$arFileTmp = CFile::ResizeImageGet(
		        $arItem['DETAIL_PICTURE'],
		        array("width" => 158, "height" => 310),
		        BX_RESIZE_IMAGE_PROPORTIONAL,
		        true,
		        false
		    );

		elseif($arItem['DETAIL_PICTURE']['HEIGHT'] < $arItem['DETAIL_PICTURE']['WIDTH']):

			$arFileTmp = CFile::ResizeImageGet(
		        $arItem['DETAIL_PICTURE'],
		        array("width" => 310, "height" => 200),
		        BX_RESIZE_IMAGE_PROPORTIONAL,
		        true,
		        false
		    );

		else:

			$arFileTmp = CFile::ResizeImageGet(
		        $arItem['DETAIL_PICTURE'],
		        array("width" => 310, "height" => 310),
		        BX_RESIZE_IMAGE_PROPORTIONAL,
		        true,
		        false
		    );

		endif;

	    $arResult['ITEMS'][$key]["PREVIEW_PICTURE"]["RESIZESRC"] = $arFileTmp;

	endif; 

    $key = $key + 1;

}

?>