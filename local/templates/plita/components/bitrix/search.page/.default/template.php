<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>

<?if(isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):
	?>
	<div class="search-language-guess">
		<?echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#"=>'<a href="'.$arResult["ORIGINAL_QUERY_URL"].'">'.$arResult["REQUEST"]["ORIGINAL_QUERY"].'</a>'))?>
	</div><br /><?
endif;?>

<?if($arResult["REQUEST"]["QUERY"] === false && $arResult["REQUEST"]["TAGS"] === false):?>
<?elseif($arResult["ERROR_CODE"]!=0):?>
	<p><?=GetMessage("SEARCH_ERROR")?></p>
	<?ShowError($arResult["ERROR_TEXT"]);?>
	<p><?=GetMessage("SEARCH_CORRECT_AND_CONTINUE")?></p>
	<br /><br />
	<p><?=GetMessage("SEARCH_SINTAX")?><br /><b><?=GetMessage("SEARCH_LOGIC")?></b></p>
	<table border="0" cellpadding="5">
		<tr>
			<td align="center" valign="top"><?=GetMessage("SEARCH_OPERATOR")?></td><td valign="top"><?=GetMessage("SEARCH_SYNONIM")?></td>
			<td><?=GetMessage("SEARCH_DESCRIPTION")?></td>
		</tr>
		<tr>
			<td align="center" valign="top"><?=GetMessage("SEARCH_AND")?></td><td valign="top">and, &amp;, +</td>
			<td><?=GetMessage("SEARCH_AND_ALT")?></td>
		</tr>
		<tr>
			<td align="center" valign="top"><?=GetMessage("SEARCH_OR")?></td><td valign="top">or, |</td>
			<td><?=GetMessage("SEARCH_OR_ALT")?></td>
		</tr>
		<tr>
			<td align="center" valign="top"><?=GetMessage("SEARCH_NOT")?></td><td valign="top">not, ~</td>
			<td><?=GetMessage("SEARCH_NOT_ALT")?></td>
		</tr>
		<tr>
			<td align="center" valign="top">( )</td>
			<td valign="top">&nbsp;</td>
			<td><?=GetMessage("SEARCH_BRACKETS_ALT")?></td>
		</tr>
	</table>
<?elseif(count($arResult["SEARCH"])>0):?>
	<?if($arParams["DISPLAY_TOP_PAGER"] != "N") echo $arResult["NAV_STRING"]?>
	<br /><hr />
	<?
	$i=0;
	
	$ids=array();
	$is=array();
	
	
	
	$arS1=array();
$arS2=array();

foreach ($arResult['SEARCH']	as $arS) { 

if ($arS["ITEM_ID"]>0) {
	
	$arS1[]=$arS;
	
} else {
	$arS2[]=$arS;
}

}
	

	foreach($arS2 as $arItem) {
	
$res = CIBlockSection::GetByID(str_replace('S','',$arItem["ITEM_ID"]));
$ar_res = $res->GetNext();

$is[]=str_replace('S','',$arItem["ITEM_ID"]);



	}
	


?>







<?
	
	
	foreach($arS1 as $arItem) {?>
	
	
	
	<?
if ($arItem["ITEM_ID"]>0) {

$res = CIBlockElement::GetByID($arItem["ITEM_ID"]);
$ar_res = $res->GetNext();

if ($ar_res['ID']==$arItem["ITEM_ID"])  {	

$ids[]=$arItem["ITEM_ID"];

} }

?>
	
	
	
	
<? } ?>	

<?

if ($is[0]>0) { ?>

<section class="catalog_container" id="koll">

<h2 class="title2">Коллекции  и бренды</h2>
<br>

<?

global $arFSectionS;

$arFSectionS['ID']=$is;



$APPLICATION->IncludeComponent(
   "bitrix:catalog.section.list", 
   "kolls", 
   array(
      "VIEW_MODE" => "TEXT",
        "SHOW_PARENT_NAME" => "Y",
        "IBLOCK_TYPE" => "",
		"FILTER_NAME"=>"arFSectionS",
        "IBLOCK_ID" => CATALOG_IBLOCK_ID,
        "SECTION_ID" => false,
        "SECTION_CODE" => "",
        "SECTION_URL" => "",
        "COUNT_ELEMENTS" => "Y",
        "TOP_DEPTH" => "3",
        "SECTION_FIELDS" => "",
        "SECTION_USER_FIELDS" => "",
        "ADD_SECTIONS_CHAIN" => "Y",
        "CACHE_TYPE" => "A",
        "CACHE_TIME" => "36000000",
        "CACHE_NOTES" => "",
        "CACHE_GROUPS" => "Y"
   ),
   false
); ?>

</section>

<? } ?>
	
	<section class="catalog_container" id="tovv">	
	
<h2 class="title2">Товары</h2>	
<br>
	
	<?
	
	$arFilterMain1=array();
	global $arFilterMain1;
	
	
$arFilterMain1=array();
$arFilterMain1['SECTION_ID']=false;
$arFilterMain1['INCLUDE_SUBSECTIONS']='Y';
$arFilterMain1['ID']=$ids;

	
	
global $COUNT_TOP;
$COUNT_TOP=40;

global $PAGE_TOP;
$PAGE_TOP=1;

	
	$APPLICATION->IncludeComponent(
    "bitrix:catalog.section",
    "kolls",
    Array(
	"DISPLAY_TOP_PAGER" => "Y",
	"DISPLAY_BOTTOM_PAGER" => "Y",
	"PAGER_SHOW_ALL" => "Y",
	"SHOW_ALL_WO_SECTION" => "Y",
        "ACTION_VARIABLE" => "action",
		"FILTER_NAME"=>"arFilterMain1",
        "ADD_PICT_PROP" => "MORE_PHOTO",
        "ADD_PROPERTIES_TO_BASKET" => "Y",
        "ADD_TO_BASKET_ACTION" => "ADD",
        "BASKET_URL" => "/personal/basket.php",
        "BRAND_PROPERTY" => "BRAND_REF",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "N",
        "COMPARE_NAME" => "CATALOG_COMPARE_LIST",
        "COMPARE_PATH" => "",
        "COMPATIBLE_MODE" => "N",
        "CONVERT_CURRENCY" => "Y",
        "CURRENCY_ID" => "RUB",
        "DATA_LAYER_NAME" => "популярно",
        "DETAIL_URL" => "",
		"SECTION_ID"=>false,
        "DISCOUNT_PERCENT_POSITION" => "bottom-right",
        "DISPLAY_COMPARE" => "N",
        "ELEMENT_COUNT" => $COUNT_TOP,
		"ELEMENT_SORT_FIELD" => $ELEMENT_SORT_FIELD,
        "ELEMENT_SORT_ORDER" => "asc",
        "HIDE_NOT_AVAILABLE" => "L",
        "HIDE_NOT_AVAILABLE_OFFERS" => "L",
        "IBLOCK_ID" => CATALOG_IBLOCK_ID,
        "IBLOCK_TYPE" => CATALOG_IBLOCK_TYPE,
        "LABEL_PROP" => array("SALELEADER"),
        "LABEL_PROP_MOBILE" => array(),
        "LABEL_PROP_POSITION" => "top-left",
        "LINE_ELEMENT_COUNT" => "",
        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
        "MESS_BTN_BUY" => "Купить",
        "MESS_BTN_COMPARE" => "Сравнить",
        "MESS_BTN_DETAIL" => "Подробнее",
        "MESS_NOT_AVAILABLE" => "Нет в наличии",
        "MESS_RELATIVE_QUANTITY_FEW" => "мало",
        "MESS_RELATIVE_QUANTITY_MANY" => "много",
        "MESS_SHOW_MAX_QUANTITY" => "Наличие",
        "OFFERS_CART_PROPERTIES" => array("COLOR_REF","SIZES_SHOES","SIZES_CLOTHES"),
        "OFFERS_FIELD_CODE" => array("",""),
        "OFFERS_LIMIT" => 12,
        "OFFERS_PROPERTY_CODE" => array(
			0 => "POVERKHNOST",
			1 => "FORMAT",
			2 => "TOLSHCHINA",
		),
        "OFFERS_SORT_FIELD" => "sort",
        "OFFERS_SORT_FIELD2" => "id",
        "OFFERS_SORT_ORDER" => "asc",
        "OFFERS_SORT_ORDER2" => "desc",
        "OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
        "OFFER_TREE_PROPS" => array("COLOR_REF","SIZES_SHOES"),
        "PARTIAL_PRODUCT_PROPERTIES" => "N",
        "PRICE_CODE" => array(
			0 => "BASE",
			1 => "OLD",
			2 => "EURO",
			3 => "OLDEURO",
		),
        "PRICE_VAT_INCLUDE" => "Y",
        "PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
        "PRODUCT_DISPLAY_MODE" => "Y",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRODUCT_PROPERTIES" => array("NEWPRODUCT"),
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "PRODUCT_QUANTITY_VARIABLE" => "",
        "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
        "PRODUCT_SUBSCRIPTION" => "Y",
        "PROPERTY_CODE" => array("MANUFACTURER","MATERIAL",""),
        "PROPERTY_CODE_MOBILE" => array(),
        "RELATIVE_QUANTITY_FACTOR" => "5",
        "ROTATE_TIMER" => "30",
        "SECTION_URL" => "",
        "SEF_MODE" => "N",
        "SEF_RULE" => "",
        "SHOW_CLOSE_POPUP" => "N",
        "SHOW_DISCOUNT_PERCENT" => "Y",
        "SHOW_MAX_QUANTITY" => "M",
        "SHOW_OLD_PRICE" => "Y",
        "SHOW_PAGINATION" => "Y",
        "SHOW_PRICE_COUNT" => "1",
        "SHOW_SLIDER" => "Y",
        "SLIDER_INTERVAL" => "3000",
        "SLIDER_PROGRESS" => "N",
        "TEMPLATE_THEME" => "blue",
        "USE_ENHANCED_ECOMMERCE" => "Y",
        "USE_PRICE_COUNT" => "N",
        "USE_PRODUCT_QUANTITY" => "Y",
        "VIEW_MODE" => "SECTION"
    )
);
	

	
	?>
	
	
	
</section>	




	

<?echo $arResult["NAV_STRING"]?>
	
	

<?else:?>
	<?ShowNote(GetMessage("SEARCH_NOTHING_TO_FOUND"));?>
<?endif;?>
</div>