<section class="allbrands">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p class="title2">
						<span>
							Мой заказ №<?=$arResult['ID'];?>, создан <?=$arResult['DATE_INSERT_FORMATED'];?>
						</span>
					</p>
				</div>
			</div>
		</div>
	</section>

	<section class="myorder">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<a href="/personal/order/" class="linkprev">
						Вернуться в список заказов
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">

					<div class="order-unit">
						<p class="lktit">
							Информация о заказе
						</p>
						<div class="val">
						

						
							<div class="flex">
								
								
								
								<div class="bl2">
									<p class="tit">
										<span>
										<?
											$arStatus = CSaleStatus::GetByID($arResult['STATUS_ID']);
										?>
											Текущий статус, от <?=$arResult['DATE_STATUS_FORMATED'];?>
										</span>
									</p>
									<p>
										<?=$arStatus['NAME']?>
									</p>
								</div>
								<div class="bl3">
									<p class="tit">
										<span>
											Сумма
										</span>
									</p>
									<p>
										<?=number_format($arResult['PRICE'], 0, '.', ' ');?> руб.
									</p>
								</div>
				
<? if (($arResult['STATUS_ID']!='NP') && ($arResult['STATUS_ID']!='N')) { ?>							
								<div class="bl4">
									<a href="/personal/?ORDER_ID=<?=$arResult['ID'];?>&repeat=Y"><button class="btngreen">
										Повторить заказ
									</button></a>
								</div>
<? } ?>		
				
<? if ($arResult['STATUS_ID']=='NP') { ?>							
								<div class="bl4">
									<button class="btngreen">
										Оплатить заказ
									</button>
								</div>
								
								<? } ?>		
<? if ($arResult['STATUS_ID']=='N') { ?>							
								
								<div class="bl4 nobut">
									<button class="btngreen">
										Оплатить заказ
									</button>
									<p class="butinf">После обработки заказа диспетчером<br>Вы сможете оплатить его.</p>
								</div>
								
<? } ?>				
								
								
								
								
								
								
							</div>
							
	<? ///////// ?>						
							
				



<? 
$i=0;
$co=count($arResult[ORDER_PROPS]);
foreach ($arResult[ORDER_PROPS] as $pr)
				{
					$i=$i+1;
					?>
					
<? if ($i%4==1) echo '<div class="flex noflex" style="display:none;">';?>
					
					

								<div class="bl2">
									<p class="tit">
										<span>
											<?=$pr['NAME'];?>
										</span>
									</p>
									<p>
										<?=$pr['VALUE'];?>
									</p>						
									
								</div>	

<? if ($i%4==0) echo '</div>';?>
								
<? } ?>									

								
								
								
								
								
							</div>
				
<? ////////// ?>
							
							<div class="valmore">
								<a href="#" OnClick="$('.noflex').show(); $(this).hide(); return false;">
									подробнее
								</a>
							</div>
						</div>
					</div>

					<div class="order-unit">
						<p class="lktit">
							Адрес доставки
						</p>
						<div class="val">
							<p>
				<?
				$addr="";
				
				foreach ($arResult[ORDER_PROPS] as $pr)
				{
					if ($pr['CODE']=='ADDR')  $addr=$pr['VALUE'];
				}
				
				?>
							
							
								<?=$addr;?>
							</p>
						</div>
					</div>

					<div class="order-unit">
						<p class="lktit">
							Параметры оплаты
						</p>
						
						
					
						
						<div class="val val1">
							<p>
								Заказ №<?=$arResult['ID'];?>, от <?=$arResult['DATE_INSERT_FORMATED'];?>, <a href="#">Ожидается оплата</a>
							</p>
							<p class="info">
								Сумма заказа: <?=number_format($arResult['PRICE'], 0, '.', ' ');?> руб.
							</p>
						</div>
						
						
							<? foreach ( $arResult['PAYMENT'] as $pay) { ?>
							

						
						<div class="val val2">
							<p>
								Счет №<?=$pay['ACCOUNT_NUMBER'];?> от <?=$pay['DATE_BILL_FORMATTED'];?>, <?=$pay[PAY_SYSTEM_NAME];?>
							</p>
							<p class="info">Сумма к оплате по счету: <?=$pay['PRICE_FORMATED'];?></p>
						</div>
						
						<? } ?>
						
						<div class="order-unit">
						<p class="lktit">
							Параметры отгрузки
						</p>
						
						
						<? foreach ($arResult['SHIPMENT'] as $shipment) { ?>
												
						<div class="val val3">
							<p>
								Отгрузка №<?=$shipment['ID'];?>, стоимость доставки <?=number_format($shipment['PRICE_DELIVERY'], 0, '.', ' ');?> руб.
							</p>
							<p class="info">
								Служба доставки: <?=$shipment['DELIVERY']['NAME'];?>
							</p>
							<p class="info">
								Статус доставки: <?=$shipment['STATUS_NAME'];?>
							</p>
							<p class="info">
								Отгрузка:
								<? if ($shipment['DEDUCTED']=='N') echo 'Не отгружено';?>
								<? if ($shipment['DEDUCTED']=='Y') echo 'Отгружено';?>
							</p>

						</div>
						
						<? } ?>
						
					</div>
					</div>

					<div class="order-unit">
						<p class="lktit">
							Содержимое заказа
						</p>
					</div>

					<div class="cart">
						<div class="scrwr">
							<table class="cart-table table-striped table-responsive">
								<thead>
									<tr>
										<th class="thw1">Наименование товара/код</th>
										<th class="thw">Цена</th>
										<th class="thw">Колличество</th>
										<th class="thw">Сумма</th>
									</tr>
								</thead>
								<tbody>


<? foreach ($arResult['BASKET']  as $arItem) { ?>

<?
$res = CIBlockElement::GetProperty(CATALOG_IBLOCK_ID, $arItem['PRODUCT_ID'], "sort", "asc", array("CODE" => "SROK_POSTAVKI"));
if ($ob = $res->GetNext()) $SROK_POSTAVKI = $ob;
 
$res = CIBlockElement::GetProperty(CATALOG_IBLOCK_ID, $arItem['PRODUCT_ID'], "sort", "asc", array("CODE" => "MINIMALNAYA_KRATNOST_ZAKAZA"));
if ($ob = $res->GetNext()) $MINIMALNAYA_KRATNOST_ZAKAZA = $ob; 

$res = CIBlockElement::GetProperty(CATALOG_IBLOCK_ID, $arItem['PRODUCT_ID'], "sort", "asc", array("CODE" => "STRANA"));
if ($ob = $res->GetNext()) $STRANA = $ob; 


$res = CIBlockElement::GetProperty(CATALOG_IBLOCK_ID, $arItem['PRODUCT_ID'], "sort", "asc", array("CODE" => "KOD_TOVARA"));
if ($ob = $res->GetNext()) $KOD_TOVARA = $ob; 

$res = CIBlockElement::GetByID($arItem['PRODUCT_ID']);
$ar_res = $res->GetNext();

   $file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_EXACT, true);  
   
  
   
   
   ?>

									<tr>
										<td>
											<div class="df">
												<div class="dfimg">
													<a href="<?=$ar_res['DETAIL_PAGE_URL'];?>"><img src="<?=NewImageR($arItem['DETAIL_PICTURE'],150,150);?>" alt=""></a>
												</div>
												<div class="dcont">
													<p class="t1">
														<?=$ar_res['NAME'];?> <span><? if ($STRANA['VALUE']!='') echo '('.$STRANA['VALUE_ENUM'].')';?></span>
													</p>
													<p class="t2">
														<? if ($KOD_TOVARA['VALUE']!='') { ?>		код <?=$KOD_TOVARA['VALUE'];?> <? } ?>
													</p>
													<p class="t2 mt4">
														<?=$SROK_POSTAVKI['VALUE_ENUM'];?>
													</p>
												</div>
											</div>
										</td>
										<td>
											<p><?=$arItem['PRICE_FORMATED'];?>/<?=$arItem['MEASURE_TEXT'];?></p>
										</td>
										<td>
											<?=$arItem['QUANTITY'];?> <?=$arItem['MEASURE_TEXT'];?>
										</td>
										<td>
											<p><?=$arItem['FORMATED_SUM'];?></p>
											<p class="t3">
												<?=GetMassa($arItem['WEIGHT']*$arItem['QUANTITY']);?>
											</p>
										</td>
									</tr>
									
									
<? } ?>				

									

								</tbody>
							</table>
						</div>
						<div class="cartresult">
							<div class="info">
								<p>
									Вес заказа:
									<strong> <?=GetMassa($arResult['ORDER_WEIGHT']);?></strong>
								</p>
								<p>
									Стоимость товара:
									<strong><?=$arResult['PRODUCT_SUM_FORMATED'];?></strong>
								</p>
							</div>
							<div class="info">
								<p>
									Стоимость доставки:
									<strong><?=$arResult['PRICE_DELIVERY_FORMATED'];?></strong>
								</p>
							</div>
							<div class="info infobig">
								<p>
									Итого к оплате:
									<strong><?=$arResult['PRICE_FORMATED'];?></strong>
								</p>
							</div>
						<? if ($arResult['STATUS_ID']=='N') { ?>
						
							<div class="text-right nobtn">
								<button class="btngreen">
									оплатить заказ
								</button>
								<p class="butinf">После обработки заказа диспетчером<br>Вы сможете оплатить его.</p>
							</div>				

						<? } ?>							
						
						<? if ($arResult['STATUS_ID']=='NP') { ?>
							
							
							
								<div class="text-right">
								<button class="btngreen">
									оплатить заказ
								</button>
							</div>
							
						<? } ?>	
							
							
							
							
						</div>
					</div>

				</div>
				<div class="col-md-12">
					<a href="/personal/order/" class="linkprev">
						Вернуться в список заказов
					</a>
				</div>
			</div>
		</div>
	</section>
	
