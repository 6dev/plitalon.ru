<? 
$i = 0;

foreach($arResult["SECTIONS"] as $key => $arSection):
	if(($arSection["DEPTH_LEVEL"] == 3) && ($arSection["UF_KH"] == 1)):

		$arFile = CFile::GetFileArray($arSection["UF_PICS"][0]);
		if($arFile){
			$arImgSect = CFile::ResizeImageGet(
	            $arFile,
	            array("width" => 308, "height" => 250),
	            BX_RESIZE_IMAGE_EXACT,
	            true,
	            false
	        );
		} else{
			$arImgSect['src'] = '/local/templates/plita/images/no_img.jpg';
		}
		$arSection["RES_IMG"] = $arImgSect;

		$arResult["FILT_ITEMS"][$i] = $arSection;

		$arMass[] = $arSection['ID'];

		$arSort = array('catalog_PRICE_10' => 'ASC');
		$arSelect = Array("PREVIEW_PICTURE", "DETAIL_PICTURE", "CATALOG_GROUP_10");
		$arFilter = Array("IBLOCK_ID"=>18, "SECTION_ID"=>$arSection["ID"], "ACTIVE"=>"Y", "PROPERTY_TIP_TOVARA_VALUE" => array("Плитка", "Инструмент", "Затирка"));
		$res = CIBlockElement::GetList($arSort, $arFilter, false, Array("nTopCount"=>14), $arSelect);

		$massPrice = array();

		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();

			$arFile = CFile::GetFileArray($arFields["PREVIEW_PICTURE"]);

			if($arFile){

				$arImgEl = CFile::ResizeImageGet(
		            $arFile,
		            array("width" => 34, "height" => 30),
		            BX_RESIZE_IMAGE_EXACT,
		            true,
		            false
		        );

		    }else{
		    	$arFile = CFile::GetFileArray($arFields["DETAIL_PICTURE"]);

		    	$arImgEl = CFile::ResizeImageGet(
		            $arFile,
		            array("width" => 34, "height" => 30),
		            BX_RESIZE_IMAGE_EXACT,
		            true,
		            false
		        );
		    }

			$arFields['RES_IMG'] = $arImgEl;
			if(strlen($arImgEl['src']) > 0):
				$arResult["FILT_ITEMS"][$i]['ELEMENT'][] = $arFields;
			endif;
			
			if(strlen($arFields['CATALOG_PRICE_10']) > 0):
				$massPrice[] = $arFields['CATALOG_PRICE_10'];
			endif;
			// if($arFields['PROPERTY_TIP_TOVARA'] == 'Плитка'){
			// 	$minPrice[] = $arFields['CATALOG_PRICE_10'];
			// } else{
			// 	$minPriceOther[] = $arFields['CATALOG_PRICE_10'];
			// }
			$pp = $pp + 1;

		}

		//print_r($arFields);

		$minPrice = min($massPrice);
		//$arSection["CUR_PRICE"] = round($massPrice);

		$arResult["FILT_ITEMS"][$i]["CUR_PRICE"] = round($minPrice);
		//$arResult["FILT_ITEMS"][$key]["CUR_PRICE"] = $arResult["FILT_ITEMS"][0]['ELEMENT']['catalog_PRICE_10'];
		// if($minPrice){
		// 	$minPrice = min($minPrice);
		// 	$arResult["CUR_SECTION"]["MIN_PRICE"] = round($minPrice);
		// } else{
		// 	$minPriceOther = min($minPriceOther);
		// 	$arResult["CUR_SECTION"]["MIN_PRICE"] = round($minPriceOther);
		// }

		$i = $i + 1;
	endif;
endforeach;

foreach($arResult["SECTIONS"] as $keys => $arSectionTwo):
	if($arSectionTwo["DEPTH_LEVEL"] == 2){
		$arSectTwo[] = $arSectionTwo;
	}
endforeach;

foreach($arResult["FILT_ITEMS"] as $kk => $arNewSect):
	foreach ($arSectTwo as $valSection) {
		if($arNewSect['IBLOCK_SECTION_ID'] == $valSection['ID']){
			$arResult["FILT_ITEMS"][$kk]['PARENT_SECTION'] = $valSection;
		}
	}
endforeach;

?>
