<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);



?>
<section class="allbrands">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title2">
                        <span>
                            Бренды
                        </span>
                    </h1>
                    <div id="myTab" class="brand-tabs">
                      <span><a href="/brands/#tall" class="active">Все бренды</a></span>
                      
<?

$path=explode('?',$_SERVER['REQUEST_URI']);
$path=explode('/',$path[0]);

$IDE=0;

$props=array();
$props[]=array("CODE"=>all,'ID'=>'');



$arFilter = Array('IBLOCK_ID'=>CATALOG_IBLOCK_ID, 'GLOBAL_ACTIVE'=>'Y', 'DEPTH_LEVEL'=>1);
  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
  while($ar_result = $db_list->GetNext())
  {


?>

<span><a href="/brands/#t<?=$ar_result ['CODE'];?>"><?=$ar_result ['NAME'];?></a></span>

  
<? 

$props[]=$ar_result;


    }








 
  
   
  $WORKS=array('0,1,2,3,4,5,6,7,8,9','A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z','А,Б,В,Г,Д,Е,Ж,З,И,К,Л,М,Н,О,П,Р,С,Т,У,Ф,Э,Я');




?>  
                      

                    </div>
                    <div class="alphabet">
                        <div class="alph1">
                            <div>
                                <p class="tit">
                                    По алфавиту:
                                </p>
                            </div>
                            
                            <div class="wordss">
                            
    <?

$i=0;
foreach ($WORKS as $WORK) { 
$i=$i+1;
$e=explode(',',$WORK);

foreach ($e as $ee) {
    
    $cl='';
    
    if ($_REQUEST['work']==$ee) $cl=' active';
    
    echo '<a href="#work" idw="'.mb_strtolower($ee).'" class="work'.$cl.'">'.$ee.'</a> '."\r\n";
    
    
    
    
}

if ($i<count($WORKS)) echo '<span>&nbsp;</span>';

} ?>
                            
                            
                        
                            </div>
                        </div>

                        <div class="alph2">
                            <div>
                                <p class="tit">
                                    По стране:
                                </p>
                            </div>
                            <div class="alpfsel">
                                <select name="city" id="city">
                                    <option value="">Все страны</option>
                                    
    

<?
$property_enums = CIBlockPropertyEnum::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID, "CODE"=>"STRANA"));
while($enum_fields = $property_enums->GetNext())
{
    
    
    // Смотрим колличество товаров в этой стране 
    
$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","DETAIL_PAGE_URL","PROPERTY_STRANA");
$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID, "ACTIVE"=>"Y", "INCLUDE_SUBSECTIONS"=>"Y","PROPERTY_STRANA_VALUE"=>$enum_fields['VALUE']);
$res3 = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>10000), $arSelect);   
$co3=$res3->SelectedRowsCount();    
    
    
if ($co3>0) echo '<option value="'.$enum_fields['ID'].'">'.$enum_fields['VALUE'].'</option>';   
    

    
    
    
}
?>
    
                                    
                                    
                                
                                
                                </select>
                            </div>
                            <div class="alph-reset">
                                <button class="btnres resetb">
                                    Сбросить
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content">


<?
$j=0;
 foreach ($props as $prop) { 
 $j=$j+1;
 ?>

            
                      <div id="t<?=$prop['CODE'];?>" class="tab-pane fade in <? if ($j==1) echo 'active';?>">
                        <div class="flex">
<?
$BRANDS=array();
$CITY='';



$arFilter = Array('IBLOCK_ID'=>CATALOG_IBLOCK_ID, 'SECTION_ID'=>$prop['ID'],"INCLUDE_SUBSECTIONS"=>'Y','DEPTH_LEVEL'=>2);


if ($prop['ID']<=0) unset($arFilter['SECTION_ID']);

  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
  while($ar_result = $db_list->GetNext())
  {
     if (!in_array($ar_result['ID'],$BRANDS)) $BRANDS[]=$ar_result['ID'];  
  }












foreach ($BRANDS as $br) {
    
    
$CITY=array();



 $arFilter = Array('IBLOCK_ID'=>CATALOG_IBLOCK_ID,'ID'=>$br);
  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true,array('UF_*'),true);
  $ar_result22 = $db_list->GetNext();











$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","DETAIL_PAGE_URL","PROPERTY_STRANA");
$arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK_ID, "ACTIVE"=>"Y","SECTION_ID"=>$br,"INCLUDE_SUBSECTIONS"=>"Y","!PROPERTY_STRANA_VALUE"=>false);
$res3 = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>10000), $arSelect);
while($ob3 = $res3->GetNextElement())
{
 $arFields3 = $ob3->GetFields();
  
 if (!in_array($arFields3['PROPERTY_STRANA_ENUM_ID'],$CITY)) $CITY[]=$arFields3['PROPERTY_STRANA_ENUM_ID'];
 
}












$ct='';

foreach ($CITY as $c)
{
    $ct=$ct.'city'.$c.' ';
}
    



?>

                            <a href="<?=$ar_result22['SECTION_PAGE_URL'];?>" class="el work<?=mb_strtolower($ar_result22['CODE'][0]);?> <?=$ct;?>">
                                <div class="imgwr">
                                
                     <?
if ($ar_result22['UF_PICS'][0]>0) { ?>

            <?       foreach ($ar_result22['UF_PICS'] as $file) { ?>
                     
                    <? if ($file>0) { ?>    <img src="<?=NewImageR($file,370,175);?>">  
                    

 <? } ?>    

                     <? break(1); } ?>  

         
 
 <? } else { ?>
                    
                    
                    
                    <img src="<?=NewImageR('no',370,175);?>">
                    
                     <? } ?>    

                     
                                    
                                    
                                </div>
                                <p class="tex">
                                    <?=$ar_result22['NAME'];?>
                                </p>
                            </a>

<? } ?>                     

                        </div>
                      </div>
                      
<? } ?>               
                     
                    </div>
                </div>
            </div>
        </div>
    </section>

<script>

function GetBrands()
{
$('.work').each(function(index,value){
    
    var idw=$(this).attr('idw');
    if ($('.work'+idw).length<=0) $(this).addClass('hwork');
        
    
    
    
});
}


GetBrands();
 
$('.resetb').click(function(){
    
    $('.flex').find('.el').show();
    $('.work.active').removeClass('active');
    
GetBrands();    
    
});

</script>



    