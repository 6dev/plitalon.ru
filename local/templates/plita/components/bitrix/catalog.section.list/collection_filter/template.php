<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


?>

<section class="catalog_container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="flex_val popular">
                    
                    <?
                    $i = 0;
                    foreach($arResult["FILT_ITEMS"] as $arSection){
                        $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
                        $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'))); 

                        if($arSection['MEASURE'] == '5'){
                            $ratio = "/шт";
                        } else{
                            $ratio = "/м²";
                        }

                        ?>
                        <div class="unitcard1wr">
                            <div class="unitcard1" id="<?=$this->GetEditAreaId($arSection['ID']);?>">
                                <div class="uimg">
                                    <div class="imgwr">
                                        <a href="<? echo $arSection['SECTION_PAGE_URL'] ?>" tabindex="<? echo $i; ?>">
                                            <img src="<? echo $arSection['RES_IMG']['src'] ?>" alt="">
                                            <? if(($arSection['UF_HIT'] == 1) || ($arSection['UF_NEW'] == 1)): ?>
                                                <div class="inform coll-inform clearfix">
                                                    <? if($arSection['UF_HIT'] == 1){?>
                                                        <p class="hit"><span>Хит</span></p>
                                                    <?}?>
                                                    <? if($arSection['UF_NEW'] == 1){?>
                                                        <p class="new"><span>Новинка</span></p>
                                                    <?}?>
                                                </div>
                                            <? endif; ?>
                                        </a>
                                    </div>
                                    <div class="uimg-abs">
                                        <div class="inform clearfix">
                                            <a href="javascript:void(0);" class="liked" index-id2="<? echo $arSection['ID'] ?>" tabindex="<? echo $i; ?>" style="background-image: url(/local/templates/plita/images/liked.png); background-repeat: no-repeat;}"></a>
                                        </div>
                                        <div class="cost">
                                            <span>от <strong><? echo $arSection['CUR_PRICE']; ?></strong> руб.<?=$ratio?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="unval">
                                    <div class="tit">
                                        <a href="<? echo $arSection['SECTION_PAGE_URL'] ?>" tabindex="<? echo $i; ?>">
                                            <? echo $arSection['NAME'] ?>  
                                        </a>
                                    </div>
                                    <div class="model">
                                        <a href="<? echo $arSection['PARENT_SECTION']['SECTION_PAGE_URL']; ?>" tabindex="<? echo $i; ?>"><? echo $arSection['PARENT_SECTION']['NAME']; ?></a>
                                    </div>

                                    <div class="example">
                                        <? foreach ($arSection['ELEMENT'] as $valEl){?>
                                            <span style="background-image: url(<? echo $valEl['RES_IMG']['src']; ?>);"></span>
                                        <? } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? $i = $i + 1; } ?>
                </div>
            </div>
        </div>
    </div>
</section>
