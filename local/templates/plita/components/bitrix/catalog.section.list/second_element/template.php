<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="catalog_container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="flex_val popular">
                    
                    <?
                    $i = 0;
                    foreach($arResult["FILT_ITEMS"] as $arSection){
                        $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
                        $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'))); ?>
                        <div class="unitcard1wr">
                            <div class="unitcard1" id="<?=$this->GetEditAreaId($arSection['ID']);?>">
                                <div class="uimg">
                                    <div class="imgwr">
                                        <a href="<? echo $arSection['SECTION_PAGE_URL'] ?>" tabindex="<? echo $i; ?>">
                                            <img src="<? echo $arSection['RES_IMG']['src'] ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="uimg-abs">
                                        <div class="inform clearfix">
                                            <a href="javascript:void(0);" class="liked" index-id2="<? echo $arSection['ID'] ?>" tabindex="<? echo $i; ?>" style="background-image: url(/local/templates/plita/images/liked.png); background-repeat: no-repeat;}"></a>
                                        </div>
                                        <div class="cost">
                                            <span>от <strong><? echo round(min($arSection['MIN_PRICE'])); ?></strong> руб./м²</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="unval">
                                    <div class="tit">
                                        <a href="<? echo $arSection['SECTION_PAGE_URL'] ?>" tabindex="<? echo $i; ?>">
                                            <? echo $arSection['NAME'] ?>  
                                        </a>
                                    </div>
                                    <div class="model">
                                        <a href="<? echo $aeSection['PARENT_SECTION']['SECTION_PAGE_URL']; ?>" tabindex="<? echo $i; ?>"><? echo $arSection['PARENT_SECTION']['NAME']; ?></a>
                                    </div>

                                    <div class="example">
                                        <? foreach ($arSection['ELEMENT'] as $valEl){?>
                                            <span style="background-image: url(<? echo $valEl['RES_IMG']['src']; ?>);"></span>
                                        <? } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? $i = $i + 1; } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- <pre>
    <?// print_r($arSection); ?>
</pre> -->