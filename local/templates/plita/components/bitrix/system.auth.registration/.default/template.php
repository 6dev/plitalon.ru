<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 */ ?>
 

<section class="login">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p class="title2">
						Регистрация
					</p>
					<div class="login-val registr-val">
						<div action="" class="form" name="formreg">
							<div class="wrap">
							
	<?			
CModule::IncludeModule('sale');	
CModule::IncludeModule('catalog');	
$db_ptype = CSalePersonType::GetList(Array("SORT" => "ASC"),array("LID"=>'s1'));
$i=0;
while ($ptype = $db_ptype->Fetch())
{
	$i=$i+1;
	

	?>
							
							
								<div class="wprad">				
									<input type="radio"<? if ($i==1) echo 'checked ';?>class="radio" id="radio<?=$ptype['ID'];?>" name="PersonType" value="<?=$ptype['ID'];?>"/>
									<label for="radio<?=$ptype['ID'];?>">
										<?=$ptype['NAME'];?>
									</label>
								</div>
								
<? } ?>								
								
								
							</div>
							
							</div>
							
							<?
							
							$db_ptype = CSalePersonType::GetList(Array("SORT" => "ASC"),array("LID"=>'s1'));
							$i=0;
						while ($ptype = $db_ptype->Fetch()) {
                        $i=$i+1;
						?>
							  
						<div class="personaltype_<?=$ptype['ID'];?> personaltypes"<? if ($i==1) { echo 'style="display:block;"'; } else { echo 'style="display:none;"'; }?>>	
			
<form class="form" name="formreg" style="border-top: 0px solid #40a23c;">			
							
						<?

$dbProperties = \Bitrix\Sale\Internals\OrderPropsTable::getList(array('select' => array('CODE',"NAME",'ID'),'filter'=>array("PERSON_TYPE_ID"=>$ptype['ID'])));
while ($props = $dbProperties->fetch()) {
	
$typetext='text';	

if ($props['CODE']=="EMAIL") $typetext='email';
	
	if ($props['CODE']!="LOCATION") {
		
	$arOrderProps = CSaleOrderProps::GetByID($props['ID']);
	
	
	
if (($props['CODE']!='valheight') && ($props['CODE']!='ADDR') && ($props['CODE']!='FILE') && ($props['CODE']!='timenoch') && ($props['CODE']!='FAX')) {
		
?>
							
							<label for=""><? if ($arOrderProps['REQUIED']=="Y") echo '*';?><?=$props['NAME'];?></label>
							<input <?if ($arOrderProps['REQUIED']=="Y") echo ' required ';?> type="<?=$typetext;?>" name="<?=$props['CODE'];?>"<? if ($props['REQUIED']=="Y") echo ' required ';?>>
<? } } }?>

							<label for="">*Пароль (мин. 6 символов)</label>
							<div class="psswr">
								<input type="password" required name="password">
								<button OnClick="$('[name=password]').attr('type','text'); return false;" class="btnpsw">
									<img src="<?=SITE_TEMPLATE_PATH;?>/images/password.png" alt="">
								</button>
							</div>
							<label for="">*Подтверждение пароля</label>
							<div class="psswr">
								<input type="password" required name="password2">
								<button OnClick="$('[name=password2]').attr('type','text'); return false;"  class="btnpsw">
									<img src="<?=SITE_TEMPLATE_PATH;?>/images/password.png" alt="">
								</button>
							</div>
							
							<label for="">
								Отправляя форму вы соглашаетесь <a href="">на обработку персональных данных</a>
							</label>
							<button class="btnformsend">
								Регистрация
							</button>
							
</form>							
							
						</div>

						<? } ?>						
							
							
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>