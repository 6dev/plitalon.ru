<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
foreach($arResult["ITEMS"] as $arItem):
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
endforeach;
?>
<div class="banner">
    <a href="<? echo $arResult['ITEMS'][0]['PROPERTIES']['HREF']['VALUE']; ?>" title="<? echo $arResult['ITEMS'][0]['NAME']; ?> - перейти" style="margin: 0 0 20px;" id="<?=$this->GetEditAreaId($arResult['ITEMS'][0]['ID']);?>">
        <img src="<? echo $arResult['ITEMS'][0]['PREVIEW_PICTURE']['SRC']; ?>" alt="<? echo $arResult['ITEMS'][0]['NAME']; ?>">
    </a>
    <a href="<? echo $arResult['ITEMS'][1]['PROPERTIES']['HREF']['VALUE']; ?>" title="<? echo $arResult['ITEMS'][1]['NAME']; ?> - перейти" id="<?=$this->GetEditAreaId($arResult['ITEMS'][1]['ID']);?>">
        <img src="<? echo $arResult['ITEMS'][1]['PREVIEW_PICTURE']['SRC']; ?>" alt="<? echo $arResult['ITEMS'][1]['NAME']; ?>">
    </a>
</div>
<div class="banner hidden-sm hidden-xs">
    <a href="<? echo $arResult['ITEMS'][2]['PROPERTIES']['HREF']['VALUE']; ?>" title="<? echo $arResult['ITEMS'][2]['NAME']; ?> - перейти" id="<?=$this->GetEditAreaId($arResult['ITEMS'][2]['ID']);?>">
        <img src="<? echo $arResult['ITEMS'][2]['PREVIEW_PICTURE']['SRC']; ?>" alt="<? echo $arResult['ITEMS'][2]['NAME']; ?>">
    </a>
</div>