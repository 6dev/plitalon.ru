<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (count($arResult["ITEMS"]) < 1)
	return;
?>
<section class="aboutmaterial wow fadeIn" data-wow-offset="200">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p class="title"><? echo $arParams["PAGER_TITLE"]; ?></p>
				<p class="linkmore">
					<a href="/about_materials/">Посмотреть все</a>
				</p>
				<div class="flex">

					<? foreach($arResult["ITEMS"] as $arItem):?>
						<? 
							$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
							$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('NEWS_ELEMENT_DELETE_CONFIRM')));
						?>
				
						<div class="unitcard3" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
							<div class="unitcard3-wrap">
								<div class="uimg">
									<a href="<?=$arItem['DETAIL_PAGE_URL'];?>">
										<img src="<?=$arItem['PREVIEW_PICTURE']['RESIZE']['src'];?>" alt="">
									</a>
								</div>
								<p class="tit">
									<span class="date">
											<?=str_replace('.','/',$arItem['ACTIVE_FROM']);?>
									</span>
									<?=$arItem['NAME'];?>
								</p>
							</div>
						</div>
					<? endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</section>