<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (count($arResult["ITEMS"]) < 1)
	return;
?>
<div class="f1">
    <div class="container">
    	<div class="row">
        	<div class="col-md-12">
          		<div class="flex">
					<?
					$i=0;
					foreach ($arResult['ITEMS'] as $arItem){
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('NEWS_ELEMENT_DELETE_CONFIRM')));
						$i=$i+1; ?>
   						<a href="<?=$arItem['PROPERTIES']['LINK']['VALUE'];?>" class="el el<?=$i;?>" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
              				<div class="elimg"></div>
			              	<p class="tit"><?=$arItem['NAME'];?></p>
              				<p class="tex"><?=$arItem['PREVIEW_TEXT'];?></p>
            			</a>
            		<? } ?>
            	</div>
            </div>
        </div>
    </div>
</div>