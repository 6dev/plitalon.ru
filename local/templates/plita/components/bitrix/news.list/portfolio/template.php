<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<section class="design portfolio">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="units3 row">
					<? $p=0;
					foreach($arResult["ITEMS"] as $arItem) {
						$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
						$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
						$p = $p + 1; ?>
						<div class="el col-xs-12 col-sm-4" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
							<div class="elwr">
								<div class="imgwr">
									<div class="portslick<?=$p;?> portslick">
										<? foreach ($arItem['BIG_IMG'] as $valSrc) { ?>
											<div>
												<a href="<?=$arItem['DETAIL_PAGE_URL'];?>">											
													<img src="<? echo $valSrc['src']; ?>" alt="">
												</a>
											</div>
										<? } ?>								
									</div>
								</div>
								<div class="portnav portnav<?=$p;?>">
									<? foreach ($arItem['SMALL_IMG'] as $valSmallSrc) { ?>
										<div>
											<img src="<? echo $valSmallSrc['src']; ?>" alt="">
										</div>
									<? } ?>										
								</div>
								<div class="tit">
									<a href="<?=$arItem['DETAIL_PAGE_URL'];?>"><p><?=$arItem['PROPERTIES']['TITLE2']['VALUE'];?></p>
									<p><?=$arItem['NAME'];?></p></a>
								</div>
							</div>
						</div>
					<?  } ?>					
				</div>
			</div>
		</div>
	</div>
</section>