<? foreach ($arResult['ITEMS'] as $key => $value) {

	foreach ($value['PROPERTIES']['PICS']['VALUE'] as $valPhoto) {
		
		$arFile = CFile::GetFileArray($valPhoto);
		if($arFile){
			$arImgBig = CFile::ResizeImageGet(
	            $arFile,
	            array("width" => 403, "height" => 323),
	            BX_RESIZE_IMAGE_EXACT,
	            true,
	            false
	        );
			//$arImgBigs[] = $arImgBig;

	        $arImgSmall = CFile::ResizeImageGet(
	            $arFile,
	            array("width" => 128, "height" => 102),
	            BX_RESIZE_IMAGE_EXACT,
	            true,
	            false
	        );

	        //$arImgSmalls[] = $arImgSmall;

			$arResult['ITEMS'][$key]['BIG_IMG'][] = $arImgBig;
			$arResult['ITEMS'][$key]['SMALL_IMG'][] = $arImgSmall;
		}

	}


} ?>