<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<!-- <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> -->
<!--     <ol class="carousel-indicators">
    	<?
    	//$j = 0;
    //	foreach($arResult["ITEMS"] as $arVal):?>
        	<li data-target="#carousel-example-generic" data-slide-to="<?//=$j;?>" <? //if($j == 0): ?>class="active"<?// endif; ?>></li>
        <?// $j = $j + 1; endforeach; ?>
    </ol> -->

    <div class="owl-carousel">
    	<?
    	$i = 0;
    	foreach($arResult["ITEMS"] as $arItem):
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
        	<div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <a href="<? echo $arItem['PROPERTIES']['HREF']['VALUE']; ?>" title="<? echo $arItem['NAME']; ?>">
    	            <img src="<? echo $arItem['PREVIEW_PICTURE']['SRC']; ?>" alt="<? echo $arItem['NAME']; ?>">
                </a>
	        </div>
	    <? $i = $i + 1; endforeach; ?>
    </div>

   <!--  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a> -->
<!-- </div> -->