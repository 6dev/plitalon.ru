<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

$strReturn = '';

	$strReturn .= '<section class="bredcrumbs">
		<div class="container">
			<div class="row">
				<div class="col-md-12">';


$strReturn .= '<div class="bx-breadcrumb" itemprop="http://schema.org/breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	$arrow = ($index > 0? '<i class="fa fa-angle-right"></i>' : '');

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
		$strReturn .= '<a href="'.$arResult[$index]["LINK"] .'">'.$arResult[$index]["TITLE"].'</a>';
	}
	else
	{
		$strReturn .= '<span>'.$arResult[$index]["TITLE"].'</span>';
	}
}

$strReturn .= '</div>
			</div>
		</div>
	</section>';

return $strReturn;


