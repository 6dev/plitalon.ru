<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);
?>

<section class="card">
	<div class="container">
		<div class="row">
			<div class="print_logo">
				<div class="col-xs-6">
					<img src="https://plitalon.ru/local/templates/plita/images/logo.png" alt="">
				</div>
				<div class="col-xs-6">
					<p>+7965-100-00-90</p>
				</div>
			</div>
			<div class="col-md-11">
				<h1 class="title2"><? echo $arResult['NAME']; ?> <?=$arResult['PROPERTIES']['CML2_ARTICLE']['VALUE'];?></h1>
				<div style="color: #888888;font-size: 36px;" class="dop_name">
					<?=$arResult['PROPERTIES']['NAZVANIE_DOPOLNITELNOE']['VALUE'];?>	
				</div>
				<div class="code_card">
					<? if($arResult['FIX_NEXT_PREV'] == true): ?>
						<div class="links">
						
							<? if(strlen($arResult['CUR_ELEMENT']['PREV']['NAME']) > 0): ?>
								<a href="<? echo $arResult['CUR_ELEMENT']['PREV']['DETAIL_PAGE_URL']; ?>" class="lprev active"><? echo GetMessage('PREV'); ?></a>
							<? else: ?>
								<a href="javascript:void(0);" class="lprev" rel="nofollow noreferrer noopener"><? echo GetMessage('PREV'); ?></a>
							<? endif; ?>

							<? if(strlen($arResult['CUR_ELEMENT']['NEXT']['NAME']) > 0): ?>				
								<a href="<? echo $arResult['CUR_ELEMENT']['NEXT']['DETAIL_PAGE_URL']; ?>" class="lnext active"><? echo GetMessage('NEXT'); ?></a>
							<? else: ?> 						
								<a href="javascript:void(0);" class="lnext" rel="nofollow noreferrer noopener"><? echo GetMessage('NEXT'); ?></a>
							<? endif; ?>
						
						</div>

					<? endif; ?>
					<div class="article">
						<p>код <?=$arResult['PROPERTIES']['KOD_TOVARA']['VALUE'];?></p>
					</div>
				</div>
			</div>
		</div>

		<div class="row">

			<div class="col-lg-7">
				<div class="slider slider-single slickcard" style="margin: 0 0 40px; text-align: center;">
					<? if ($arResult['DETAIL_PICTURE']['ID']!='') { ?>	
						<div>
							<a data-lightbox="example-set" href="<?=$arResult['DETAIL_PICTURE']['SRC'];?>">
								<img src="<?=$arResult['BIG_PICTURE']['src'];?>" alt="">
							</a>
						</div>  
					<? } ?>
				</div>
				<div class="slider slider-nav navcard">
					<? if (count($arResult['SMALL_PICTURE']['src']) > 1) { ?>	
						<div>
							<img src="<?=$arResult['SMALL_PICTURE']['src'];?>" alt="">
						</div>  
					<? } ?>
				</div>

				
			</div>


			<div class="col-lg-5">
				<div class="specification-wr">
					<div class="specification">
						<? if (strlen($arResult['PRICES']['Старая цена продажи']['VALUE']) > 0) {  ?>				
						
							<p class="cost">
								<span>Цена со скидкой:</span>
								<strong><?=$arResult['PRICES']['Проба Цена продажи']['VALUE'];?> руб./<?=$arResult['ITEM_MEASURE']['TITLE']; ?></strong>
							</p>
							<p class="cost costold">
								<span>Цена:</span>
								<del><?=$arResult['PRICES']['Старая цена продажи']['VALUE'];?> руб./<?=$arResult['ITEM_MEASURE']['TITLE']; ?></del>
							</p>
						<? }  else { ?>		 
							<p class="cost">
								<span>Цена:</span>
								<strong><?=$arResult['PRICES']['Проба Цена продажи']['VALUE'];?> руб./<?=$arResult['ITEM_MEASURE']['TITLE']; ?></strong>
							</p>
						<? } ?>

						<? if($arResult['PROPERTIES']['SROK_POSTAVKI']['VALUE'] != 'Снято с производства'): ?>
							<div class="cardcalc clearfix">
								<div class="cartcalc">
									<button class="ccalc-minus">-</button>
									<input type="text" value="<?=str_replace(',','.',$arResult['PROPERTIES']['MINIMALNAYA_KRATNOST_ZAKAZA']['VALUE']);?>"  dd="<?=str_replace(',','.',$arResult['PROPERTIES']['MINIMALNAYA_KRATNOST_ZAKAZA']['VALUE']);?>" >
									<button class="ccalc-plus">+</button>
									<span class="hcolval"><?=$arResult['ITEM_MEASURE']['TITLE']; ?></span>
								</div>
								<button class="cardun1-button cardgreen" title="<?=$arResult['NAME'];?>" index-id="<?=$arResult['ID'];?>">
									В корзину
								</button>
								<div class="det_el_like">
									<a href="javascript:void(0);" class="liked" index-id2="<? echo $arResult['ID']; ?>" rel="nofollow noreferrer noopener"></a>
								</div>
							</div>
						<? endif; ?>

						

						<div class="contents">
							<? if($arResult['PROPERTIES']['SROK_POSTAVKI']['VALUE'] != 'Снято с производства'): ?>
								<p>
									<span>
										Продается кратно:
									</span>
									<?=str_replace(',','.',$arResult['PROPERTIES']['MINIMALNAYA_KRATNOST_ZAKAZA']['VALUE']);?> <?=$arResult['ITEM_MEASURE']['TITLE']; ?>
								</p>
							<? endif; ?>

							<button class="btnmod" data-toggle="modal" data-id="<?=$arResult['ID'];?>" data-target="#searchchep">
								Нашли дешевле? Сообщите!
							</button>
						</div>
						<div class="contents">
							<? if ($arResult['SECTION']['PATH']['1']['NAME']!='') { ?>					
								<p>
									<span>Производитель:</span>
									<a href="<?=$arResult['SECTION']['PATH']['1']['SECTION_PAGE_URL'];?>"><span style="color:#40a23c; text-decoration: underline;" ><?=$arResult['SECTION']['PATH']['1']['NAME'];?></span></a>
								</p>
							<? } ?>	
							<? if ($arResult['SECTION']['PATH']['2']['NAME']!='') { ?>
								<p>
									<span>Коллекция:</span>
									<a href="<?=$arResult['SECTION']['PATH']['2']['SECTION_PAGE_URL'];?>"><span style="color:#40a23c; text-decoration: underline;" ><?=$arResult['SECTION']['PATH']['2']['NAME'];?></span></a>
								</p>
							<? } ?>	
							<? foreach ($arResult['DISPLAY_PROPERTIES'] as $asP) { ?>
								<? if(strlen($asP['VALUE']) > 0){ ?>
									<p><span><?=$asP['NAME'];?>:</span> <span <? if($asP['VALUE'] == 'Снято с производства'): ?>style="color:#ff1a22;" <? else: ?> style="color:#000000;" <? endif; ?>><?=$asP['VALUE'];?></span></p> 
								<? } ?>
							<? } ?>
						</div>
						<div class="contents socials">
							<?
							$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/includes/socials.php", Array(), Array(
							    "MODE"      => "html",                                           // будет редактировать в веб-редакторе
							    "NAME"      => "Политика конфиденциальности",      // текст всплывающей подсказки на иконке
							    ));
							?>		
						</div>
						<div class="downloads">
							<? $p=$arResult['DETAIL_PAGE_URL']; $p=explode('/',$p); ?>	
							<a href="/download/<?=$p[3];?>/" target="_blank">Скачать каталог в PDF</a>
							<a OnCLick="PrintElem('#printblock'); return false;" href="">Распечатать</a>
						</div>

						<div id="pageControl"></div>
					</div>
				</div>
			</div>
		</div>


		<div class="row">
			<div class="col-md-12">
				<div class="card-text">
					<?=$arResult["~DETAIL_TEXT"];?>	
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<?// if(count($arResult["CUR_SECTION"]["TIP_TOVARA"]) > 1): ?>
					<p class="cardtabtitle"><span>Товары коллекции</span></p>
					<div class="card-tabs">

					
						<div class="buttons__tabs">
							<a href="<? echo $arResult['DETAIL_PAGE_URL']; ?>#pageControl" class="<? if((isset($_GET['filtr'])) || !empty($_GET['filtr'])): else: ?>active<? endif; ?>">Все</a>
							<? 
							$i = 1;
							foreach ($arResult["CUR_SECTION"]["TIP_TOVARA"] as $valTipTovara){ 

								$getUrl = "filtr=" . $valTipTovara;
								?>
								<a href="<?echo $APPLICATION->GetCurPageParam($getUrl, array('filtr'));?>#pageControl" <?if($_GET['filtr'] == $valTipTovara):?> class="active"<? endif; ?> ><? echo $valTipTovara; ?></a>

								<? if ($_GET['filtr'] == $valTipTovara){

									$GLOBALS['arrFilters'] = array("IBLOCK_SECTION_ID"=> $arResult['IBLOCK_SECTION_ID'], "=PROPERTY_TIP_TOVARA_VALUE" => $valTipTovara);
								}
								else{
									$GLOBALS['arrFilters'] = array("IBLOCK_SECTION_ID"=> $arResult['IBLOCK_SECTION_ID'], "><ID" => $arResult['ID']);
								}?>
							<? $i = $i + 1; }

							//if (isset($_GET['filtr']) && !empty($_GET['filtr'])){ $category = $_GET['category'];}
									
							?>
						</div>

						<div class="tab-content">
							<? if((isset($_GET['filtr'])) || !empty($_GET['filtr'])):

								$GLOBALS['arrFilters'] = array("IBLOCK_SECTION_ID"=> $arResult['IBLOCK_SECTION_ID'], "!ID"=> $arResult['ID'], "=PROPERTY_TIP_TOVARA_VALUE" => $_GET['filtr']);
								
							else:
								$GLOBALS['arrFilters'] = array("IBLOCK_SECTION_ID"=> $arResult['IBLOCK_SECTION_ID'], "!ID"=> $arResult['ID']);
							endif;
							

							$APPLICATION->IncludeComponent(
								"bitrix:catalog.section",
								"pilaton_product",
								array(
									"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
									"IBLOCK_ID" => $arParams["IBLOCK_ID"],
									"ELEMENT_SORT_FIELD" => "propertysort_TIP_TOVARA",
									"ELEMENT_SORT_ORDER" => "asc",
									"ELEMENT_SORT_FIELD2" => "CATALOG_PRICE_10",
									"ELEMENT_SORT_ORDER2" => "asc",
									"PROPERTY_CODE" => (isset($arParams["LIST_PROPERTY_CODE"]) ? $arParams["LIST_PROPERTY_CODE"] : []),
									"PROPERTY_CODE_MOBILE" => $arParams["LIST_PROPERTY_CODE_MOBILE"],
									"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
									"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
									"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
									"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
									"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
									"BASKET_URL" => $arParams["BASKET_URL"],
									"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
									"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
									"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
									"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
									"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
									"FILTER_NAME" => "arrFilters",
									"CACHE_TYPE" => $arParams["CACHE_TYPE"],
									"CACHE_TIME" => $arParams["CACHE_TIME"],
									"CACHE_FILTER" => $arParams["CACHE_FILTER"],
									"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
									"SET_TITLE" => $arParams["SET_TITLE"],
									"MESSAGE_404" => $arParams["~MESSAGE_404"],
									"SET_STATUS_404" => $arParams["SET_STATUS_404"],
									"SHOW_404" => $arParams["SHOW_404"],
									"FILE_404" => $arParams["FILE_404"],
									"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
									"PAGE_ELEMENT_COUNT" => 40,
									"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
									"PRICE_CODE" => $arParams["~PRICE_CODE"],
									"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
									"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

									"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
									"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
									"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
									"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
									"PRODUCT_PROPERTIES" => (isset($arParams["PRODUCT_PROPERTIES"]) ? $arParams["PRODUCT_PROPERTIES"] : []),

									"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
									"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
									"PAGER_TITLE" => $arParams["PAGER_TITLE"],
									"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
									"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
									"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
									"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
									"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
									"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
									"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
									"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
									"LAZY_LOAD" => $arParams["LAZY_LOAD"],
									"MESS_BTN_LAZY_LOAD" => $arParams["~MESS_BTN_LAZY_LOAD"],
									"LOAD_ON_SCROLL" => $arParams["LOAD_ON_SCROLL"],

									"OFFERS_CART_PROPERTIES" => (isset($arParams["OFFERS_CART_PROPERTIES"]) ? $arParams["OFFERS_CART_PROPERTIES"] : []),
									"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
									"OFFERS_PROPERTY_CODE" => (isset($arParams["LIST_OFFERS_PROPERTY_CODE"]) ? $arParams["LIST_OFFERS_PROPERTY_CODE"] : []),
									"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
									"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
									"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
									"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
									"OFFERS_LIMIT" => (isset($arParams["LIST_OFFERS_LIMIT"]) ? $arParams["LIST_OFFERS_LIMIT"] : 0),

									"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
									"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
									"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
									"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
									"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
									'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
									'CURRENCY_ID' => $arParams['CURRENCY_ID'],
									'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
									'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],

									'LABEL_PROP' => $arParams['LABEL_PROP'],
									'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
									'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
									'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
									'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
									'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
									'PRODUCT_ROW_VARIANTS' => $arParams['LIST_PRODUCT_ROW_VARIANTS'],
									'ENLARGE_PRODUCT' => $arParams['LIST_ENLARGE_PRODUCT'],
									'ENLARGE_PROP' => isset($arParams['LIST_ENLARGE_PROP']) ? $arParams['LIST_ENLARGE_PROP'] : '',
									'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
									'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
									'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',

									'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
									'OFFER_TREE_PROPS' => (isset($arParams['OFFER_TREE_PROPS']) ? $arParams['OFFER_TREE_PROPS'] : []),
									'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
									'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
									'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
									'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
									'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
									'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
									'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
									'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
									'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
									'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
									'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
									'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
									'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
									'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
									'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),

									'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
									'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
									'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),

									'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
									"ADD_SECTIONS_CHAIN" => Y,
									'ADD_TO_BASKET_ACTION' => $basketAction,
									'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
									'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
									'COMPARE_NAME' => $arParams['COMPARE_NAME'],
									'USE_COMPARE_LIST' => 'Y',
									'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
									'COMPATIBLE_MODE' => (isset($arParams['COMPATIBLE_MODE']) ? $arParams['COMPATIBLE_MODE'] : ''),
									'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : '')
								),
								$component
							);
							?>
						</div>
					
					</div>
				<? //endif; ?>
			</div>
		</div>

	</div>
</section>




<div id="printblock" style="display:none;"></div>


<script type="text/javascript">
	
	function PrintElem(elem)
    {
        Popup($(elem).html());
    }


    function Popup(data){


		var url = window.location.href + '?';

		url=url.substr(0,url.indexOf('#'));
		url=url.substr(0,url.indexOf('?'))+'?print=Y';

        var mywindow = window.open(url,'','height='+$(window).height()+',width='+$(window).width());

        mywindow.focus(); // necessary for IE >= 10
        mywindow.print();
	
		setTimeout(function(){
	
       		mywindow.close();
	
	
		},1000);

       	return true;
    }

</script>


<!-- <pre>
	<? //print_r($arResult); ?>
</pre> -->