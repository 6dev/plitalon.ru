<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

$key = 0;



$arFileTmp = CFile::ResizeImageGet(
    $arResult['DETAIL_PICTURE'],
    array("width" => 248, "height" => 130),
    BX_RESIZE_IMAGE_PROPORTIONAL,
    true,
    false
);

$arResult['SMALL_PICTURE'] = $arFileTmp;

$arFileTmpBig = CFile::ResizeImageGet(
    $arResult['DETAIL_PICTURE'],
    array("width" => 784, "height" => 500),
    BX_RESIZE_IMAGE_PROPORTIONAL,
    true,
    false
);

$arResult['BIG_PICTURE'] = $arFileTmpBig;


$newImg = NewImageR($arResult['DETAIL_PICTURE']['ID'], $arResult['DETAIL_PICTURE']['WIDTH'], $arResult['DETAIL_PICTURE']['HEIGHT'], $waterk=true);


//print_r($newImg);
// $arWaterMark = Array(
//     array(
//         "name" => "watermark",
//         "position" => "bottomright", // Положение
//         "type" => "image",
//        // "size" => "small",
//        "coefficient" => $arResult["DETAIL_PICTURE"]["WIDTH"] * 0.005,
//         "file" => $_SERVER["DOCUMENT_ROOT"].'/local/templates/plita/images/water.png', // Путь к картинке
//         "fill" => "resize",
//     )
// );
// $arFileTmpReal = CFile::ResizeImageGet(
//     $arResult['DETAIL_PICTURE'],
//     array("width" => $arResult['DETAIL_PICTURE']['WIDTH'], "height" => $arResult['DETAIL_PICTURE']['HEIGHT']),
//     BX_RESIZE_IMAGE_EXACT,
//     true,
//     $arWaterMark
// );

$arResult['DETAIL_PICTURE']['SRC'] = $newImg;


$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "DETAIL_PAGE_URL", "PROPERTY_*");
$arFilter = Array("IBLOCK_ID"=>$arResult["IBLOCK_ID"], "IBLOCK_SECTION_ID"=>$arResult["IBLOCK_SECTION_ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>300), $arSelect);
while($ob = $res->GetNextElement()){ 

	$arFields[] = $ob->GetFields();  

	$arProps = $ob->GetProperties();

	$tip_tovara[] = $arProps['TIP_TOVARA']['VALUE'];
}

foreach ($arFields as $key => $valElements){
	if($valElements['ID'] == $arResult['ID']){
		$arResult['CUR_ELEMENT'] = $valElements;

		if($key == 0): 
			$arResult['CUR_ELEMENT']['NEXT'] = $arFields[$key + 1];
			$arResult['CUR_ELEMENT']['PREV'] = end($arFields);
		elseif($valCurSect == end($massSect)):
			$arResult['CUR_ELEMENT']['NEXT'] = $arFields[0];
			$arResult['CUR_ELEMENT']['PREV'] = $arFields[$key - 1];

		else:
			$arResult['CUR_ELEMENT']['NEXT'] = $arFields[$key + 1];
			$arResult['CUR_ELEMENT']['PREV'] = $arFields[$key - 1];
		endif;
	}
}
if($massSect[1]): $arResult['FIX_NEXT_PREV'] = true; else:  $arResult['FIX_NEXT_PREV'] = false; endif;

$tiptovaraDelDublikat = array_unique($tip_tovara);


$arResult["CUR_SECTION"]["TIP_TOVARA"] = $tiptovaraDelDublikat;


\Bitrix\Main\Loader::includeModule('dev2fun.opengraph');
\Dev2fun\Module\OpenGraph::Show($arResult['ID'],'element');
?>

