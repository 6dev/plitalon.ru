<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);
?>

<section class="card">
	<div class="container">
		<div class="row">
			<div class="col-md-11">
				<h1 class="title2"><? $APPLICATION->ShowTitle(false);?> <?=$arResult['PROPERTIES']['CML2_ARTICLE']['VALUE'];?></h1>
				<div style="color: #888888;font-size: 36px;">
					<?=$arResult['PROPERTIES']['NAZVANIE_DOPOLNITELNOE']['VALUE'];?>	
				</div>
				<div class="code_card">
					<div class="links">
						<a href="" class="lprev">Пред.</a>
						<a href="" class="lnext active">След.</a>
					</div>
					<div class="article">
						<? if ($arResult['PROPERTIES']['KOD_TOVARA']['VALUE']!='') { ?> <p>код <?=$arResult['PROPERTIES']['KOD_TOVARA']['VALUE'];?></p>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-7">
				<div class="slick slickcard">
					<? if ($arResult['DETAIL_PICTURE']['ID']!='') { ?>	
						<div>
							<a class="fancybox" href="<?=NewImageR($arResult['DETAIL_PICTURE']['ID'],0,0,true);?>">
								<img src="<?=NewImageR($arResult['DETAIL_PICTURE']['ID'],784,500,true);?>" alt="">
							</a>
						</div>  
					<? } ?>
				</div>
				<div class="slick navcard">
					<? if ($arResult['DETAIL_PICTURE']['ID']!='') { ?>	
						<div>
							<img src="<?=NewImageR($arResult['DETAIL_PICTURE']['ID'],248,130,false);?>" alt="">
						</div>  
					<? } ?>
				</div>
			</div>
			<div class="col-lg-5">
				<div class="specification-wr">
					<div class="specification">
						<? if (($arResult['PRICES']['BASE']['DISCOUNT_DIFF']>0) || ($arResult['PRICES']['OLD']['VALUE']>0)) {
							$arResult['PRICES']['BASE']['PRINT_VALUE_VAT'] = $arResult['PRICES']['OLD']['PRINT_VALUE_VAT']; ?>				
						
							<p class="cost">
								<span>Цена со скидкой:</span>
								<strong><?=$arResult['PRICES']['BASE']['PRINT_DISCOUNT_VALUE_VAT'];?>/<?=$arResult['ITEM_MEASURE']['TITLE']; ?></strong>
							</p>
							<p class="cost costold">
								<span>Цена:</span>
								<del><?=$arResult['PRICES']['BASE']['PRINT_VALUE_VAT'];?>/<?=$arResult['ITEM_MEASURE']['TITLE']; ?></del>
							</p>
						<? }  else { ?>		
							<p class="cost">
								<span>Цена:</span>
								<strong><?=$arResult['PRICES']['BASE']['PRINT_VALUE_VAT'];?>/<?=$arResult['ITEM_MEASURE']['TITLE']; ?></strong>
							</p>
						<? } ?>
						<div class="cardcalc clearfix">
							<div class="cartcalc">
								<button class="ccalc-minus">-</button>
								<input type="text" value="<?=str_replace(',','.',$arResult['PROPERTIES']['MINIMALNAYA_KRATNOST_ZAKAZA']['VALUE']);?>"  dd="<?=str_replace(',','.',$arResult['PROPERTIES']['MINIMALNAYA_KRATNOST_ZAKAZA']['VALUE']);?>" >
								<button class="ccalc-plus">+</button>
								<span class="hcolval"><?=$arResult['ITEM_MEASURE']['TITLE']; ?></span>
							</div>
							<button class="cardun1-button cardgreen" title="<?=$arResult['NAME'];?>" index-id="<?=$arResult['ID'];?>">
								В корзину
							</button>
						</div>
						<div class="contents">
							<p>
								<span>
									Продается кратно:
								</span>
								<?=str_replace(',','.',$arResult['PROPERTIES']['MINIMALNAYA_KRATNOST_ZAKAZA']['VALUE']);?> <?=$arResult['ITEM_MEASURE']['TITLE']; ?>
							</p>
							<button class="btnmod" data-toggle="modal" data-id="<?=$arResult['ID'];?>" data-target="#searchchep">
								Нашли дешевле? Сообщите!
							</button>
						</div>
						<div class="contents">
							
							
					<?

if ($arResult['SECTION']['PATH']['1']['NAME']!='') { ?>					
							
								<p>
									<span>
										Производитель:
									</span>
									<span><a style="color:#40a23c; text-decoration: underline;" href="<?=$arResult['SECTION']['PATH']['1']['SECTION_PAGE_URL'];?>"><?=$arResult['SECTION']['PATH']['1']['NAME'];?></a>
</span>
								</p>
										
<? } ?>	



<? if ($arResult['SECTION']['PATH']['2']['NAME']!='') { ?>					
							
								<p>
									<span>
										Коллекция:
									</span>
									<span><a style="color:#40a23c; text-decoration: underline;" href="<?=$arResult['SECTION']['PATH']['2']['SECTION_PAGE_URL'];?>"><?=$arResult['SECTION']['PATH']['2']['NAME'];?></a></span>
								</p>
										
<? } ?>	






				


<?
foreach ($arParams['OFFERS_CART_PROPERTIES'] as $asP) {
?>
							
								
								
									<? if ($arResult['PROPERTIES'][$asP]['VALUE']!='') { ?> 
						
							<p>
									<span>
										<?=$arResult['PROPERTIES'][$asP]['NAME'];?>:
										
									</span>	
										
									<span style="color:#000000;">
									<?=$arResult['PROPERTIES'][$asP]['VALUE'];?>
									</span>
						 </p> <? } ?>	
						 
<? } ?>
								
								
				

					
								
								
							</div>
							<div class="contents socials">

	<?
// включаемая область для раздела
$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH."/includes/socials.php", Array(), Array(
    "MODE"      => "html",                                           // будет редактировать в веб-редакторе
    "NAME"      => "Политика конфиденциальности",      // текст всплывающей подсказки на иконке
    ));
?>		

								
							</div>
							<div class="downloads">
							
<?
$p=$arResult['DETAIL_PAGE_URL'];
$p=explode('/',$p);
?>	
							
								<a href="/download/<?=$p[3];?>/">Скачать каталог в PDF</a>
								<a OnCLick="PrintElem('#printblock'); return false;" href="">Распечатать</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="card-text">
						
					<?=$arResult["~DETAIL_TEXT"];?>	
						
					</div>
				</div>
			</div>

		
		
		
		
		
		
		
		
	
		
		
		
		
		
			
			
			
			
		</div>
	</section>

<? } ?>