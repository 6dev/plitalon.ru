<?//if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

//use Bitrix\Main;
//use Bitrix\Main\Localization\Loc;

// $this->setFrameMode(true); ?>
<?

if ($_REQUEST['delall']=='Y') CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());

if (count($arResult['GRID']['ROWS'])>0) {

?>



		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p class="title2">
						<span>
							Корзина
						</span>
					</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="scrwr">
							<table class="cart-table table-striped table-responsive">
								<thead>
									<tr>
										<th class="thw1">Наименование товара/код</th>
										<th class="thw2">Цена</th>
										<th class="thw3">Колличество/ед. изм.</th>
										<th class="thw4">Стоимость/вес</th>
										<th class="thw5">Удалить</th>
									</tr>
								</thead>
								<tbody>
	
<?

$ITOGI=0;
$WE=0;

 foreach ($arResult['GRID']['ROWS'] as $arItem) { 
 
$res = CIBlockElement::GetProperty(CATALOG_IBLOCK_ID, $arItem['PRODUCT_ID'], "sort", "asc", array("CODE" => "SROK_POSTAVKI"));
if ($ob = $res->GetNext()) $SROK_POSTAVKI = $ob;
 
$res = CIBlockElement::GetProperty(CATALOG_IBLOCK_ID, $arItem['PRODUCT_ID'], "sort", "asc", array("CODE" => "MINIMALNAYA_KRATNOST_ZAKAZA"));
if ($ob = $res->GetNext()) $MINIMALNAYA_KRATNOST_ZAKAZA = $ob; 

$res = CIBlockElement::GetProperty(CATALOG_IBLOCK_ID, $arItem['PRODUCT_ID'], "sort", "asc", array("CODE" => "STRANA"));
if ($ob = $res->GetNext()) $STRANA = $ob; 


$res = CIBlockElement::GetProperty(CATALOG_IBLOCK_ID, $arItem['PRODUCT_ID'], "sort", "asc", array("CODE" => "KOD_TOVARA"));
if ($ob = $res->GetNext()) $KOD_TOVARA = $ob; 

$res = CIBlockElement::GetByID($arItem['PRODUCT_ID']);
$ar_res = $res->GetNext();

   $file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_EXACT, true);  
   

   
$ITOGI=$ITOGI+$arItem['PRICE']*$arItem['QUANTITY'];
$WE=$WE+$arItem['WEIGHT']*$arItem['QUANTITY'];   


?>

									<tr>
										<td>
											<div class="df">
												<div class="dfimg">
													<a target="_blank" href="<?=$ar_res['DETAIL_PAGE_URL'];?>"><img src="<?=NewImageR($arItem['DETAIL_PICTURE'],150,150);?>" alt=""></a>
												</div>
												<div class="dcont">
													<p class="t1">
														<?=$ar_res['NAME'];?> <span><? if ($STRANA['VALUE']!='') echo '('.$STRANA['VALUE_ENUM'].')';?></span>
													</p>
													<p class="t2">
												<? if ($KOD_TOVARA['VALUE']!='') { ?>		код <?=$KOD_TOVARA['VALUE'];?> <? } ?>
													</p>
													<p class="t2 mt4">
														<?=$SROK_POSTAVKI['VALUE_ENUM'];?>
													</p>
												</div>
											</div>
										</td>
										<td>
											<p><?=$arItem['PRICE_FORMATED'];?>/<?=$arItem['MEASURE_TEXT'];?></p>
										</td>
										<td>
											<div class="dfcalc">
												<div class="cartcalc">
													<button class="ccalc-minus basketajax">-</button>
													<input class="basketid" index-id="<?=$arItem['ID'];?>" type="text" value="<?=$arItem['QUANTITY'];?>" dd="<?=str_replace(',','.',$MINIMALNAYA_KRATNOST_ZAKAZA['VALUE']);?>">
													<button class="ccalc-plus basketajax">+</button>
												</div>
												<div class="el">
													<p><?=$arItem['MEASURE_TEXT'];?></p>
													<p><?=GetMassa($arItem['WEIGHT_FORMATED']);?> / <?=$arItem['MEASURE_TEXT'];?></p>
												</div>
											</div>
										</td>
										<td>
											<p><?=$arItem['SUM'];?></p>
											<p class="t3">
												<?=GetMassa($arItem['WEIGHT']*$arItem['QUANTITY']);?>
											</p>
										</td>
										<td>
											<div class="text-center">
												<a href="#del" index-id="<?=$arItem['ID'];?>"  Onclick="del=<?=$arItem['ID'];?>" class="cart-remove">
													<img src="<?=SITE_TEMPLATE_PATH;?>/images/cartremove.png" alt="">
												</a>
											</div>
										</td>
									</tr>

	<? } ?>								

								</tbody>
							</table>
					</div>

					<div class="cartresult">
						<div class="info">
							<p>
								Общий вес заказа:
								<span><?=GetMassa($WE);?></span>
							</p>
							<p>
								Итого (без учета доставки):
					
								<strong><?=number_format($ITOGI, 2, '.', ' ');;?> руб.</strong>
							</p>
						</div>
						<div class="text-right">
							<a OnClick="$.cookie('steps',0, { expires: 7, path: '/'});" href="<?=$arParams['PATH_TO_ORDER'];?>">
							<button class="btngreen">
								оформить заказ
							</button></a>
						</div>
					</div>

				</div>
			</div>
		</div>

<script>

$('.basketajax').click(function()  { 

var id=$(this).parent('.cartcalc').find('.basketid').attr('index-id');


setTimeout(function(){
	
var q=$('[index-id='+id+']').val();

 $.get( '/local/ajax/basket.php?edit=Y&id='+id+"&q="+q, function(data) {
  
})
  .done(function(data) {
    $('#basket').html(data);
	hCartOpen();
	
	// Обновить саму страницу  с товаров
	
	$.get( '/basket/basket.php', function(data2) {
  
})
  .done(function(data2) {
	  
	  
	  console.log('333');
	  
    $('#basketfull').html(data2);
cartCalc();
	
	// Обновить саму страницу  с товаров	
	
  });
	
	///
	
	
	
  });


	
//BX.ajax.insertToNode(',"basket");
// BX.ajax.insertToNode('/local/basket/basket.php',"basketfull");

}, 100);





});

function UpBa4(aaa)
{
	
setTimeout(function(){
	

var q=$('[index-id='+aaa+']').val();

 $.get( '/local/ajax/basket.php?edit=Y&id='+aaa+"&q="+q, function(data) {
  
})
  .done(function(data) {
    $('#basket').html(data);
	hCartOpen();
	
	// Обновить саму страницу  с товаров
	
	$.get( '/basket/basket.php', function(data2) {
  
})
  .done(function(data2) {
	  
	  
	  console.log('333');
	  
    $('#basketfull').html(data2);
cartCalc();
	
	// Обновить саму страницу  с товаров	
	
  });
	
	///
	
	
	
  });


	
//BX.ajax.insertToNode(',"basket");
// BX.ajax.insertToNode('/local/basket/basket.php',"basketfull");

}, 100);		
	
}




$('.basketid').blur(function() {
	
var id=$(this).attr('index-id');
	
UpBa4(id); 	
	
	
});

$('.basketid').keypress(function(event){
	
var id=$(this).parent('.cartcalc').find('.basketid').attr('index-id');
	
		
	
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
       
	  

UpBa4(id);
   
	   
	   
	   
    }
});





$('.cart-remove').click(function()  { 

var id=$(this).attr('index-id');


setTimeout(function(){
	
var q=0;

 $.get( '/local/ajax/basket.php?edit=Y&id='+id+"&q="+q, function(data) {
  
})
  .done(function(data) {
    $('#basket').html(data);
	hCartOpen();
	
	// Обновить саму страницу  с товаров
	
	$.get( '/basket/basket.php', function(data2) {
  
})
  .done(function(data2) {
	  
	  
	  console.log('333');
	  
    $('#basketfull').html(data2);
cartCalc();
	
	// Обновить саму страницу  с товаров	
	
  });
	
	///
	
	
	
  });


	
//BX.ajax.insertToNode(',"basket");
// BX.ajax.insertToNode('/local/basket/basket.php',"basketfull");

}, 100);





});






</script>

<? }  else { ?>


<section class="thanks">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="text-center">
						<img src="images/404.png" alt="">
						<p class="tit">Корзина</p>
						<div class="content2">
							<p>
Ваша корзина пустая!
</p>

	<a href="/catalog/" class="tdecn">
									ПЕРЕЙТИ В КАТАЛОГ <img src="<?=SITE_TEMPLATE_PATH;?>/images/onmain.png" alt="">
								</a>

</div>
</div>
</div>
</div>
</div>
</section>



<? }  ?>
