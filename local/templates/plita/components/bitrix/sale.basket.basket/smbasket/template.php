<? $this->setFrameMode(true); ?>

<a href="#" class="hidden-sm hidden-xs">
    <button class="hi hi4">
        <i></i>
        <span class="countNumm"><?= count($arResult['GRID']['ROWS']); ?></span>
    </button>
</a>
<a href="/basket/" class="hidden-lg hidden-md">
    <button class="hi hi5">
        <i></i>
        <span class="countNumm"><?= count($arResult['GRID']['ROWS']); ?></span>
    </button>
</a>
<div class="cartheaderlist">
    <div class="js-customscroll">
        <?
        foreach ($arResult['GRID']['ROWS'] as $arItem) {
            $res = CIBlockElement::GetProperty(CATALOG_IBLOCK_ID, $arItem['PRODUCT_ID'], "sort", "asc", array("CODE" => "SROK_POSTAVKI"));
            if ($ob = $res->GetNext()) $SROK_POSTAVKI = $ob;
            $res = CIBlockElement::GetProperty(CATALOG_IBLOCK_ID, $arItem['PRODUCT_ID'], "sort", "asc", array("CODE" => "MINIMALNAYA_KRATNOST_ZAKAZA"));
            if ($ob = $res->GetNext()) $MINIMALNAYA_KRATNOST_ZAKAZA = $ob;
            $res = CIBlockElement::GetProperty(CATALOG_IBLOCK_ID, $arItem['PRODUCT_ID'], "sort", "asc", array("CODE" => "STRANA"));
            if ($ob = $res->GetNext()) $STRANA = $ob;
            $res = CIBlockElement::GetProperty(CATALOG_IBLOCK_ID, $arItem['PRODUCT_ID'], "sort", "asc", array("CODE" => "KOD_TOVARA"));
            if ($ob = $res->GetNext()) $KOD_TOVARA = $ob;
            $res = CIBlockElement::GetByID($arItem['PRODUCT_ID']);
            $ar_res = $res->GetNext();
            $file = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width' => 100, 'height' => 100), BX_RESIZE_IMAGE_EXACT, true);
            ?>
            <div class="unit" basket-id="<?= $arItem['ID']; ?>">
                <div class="cimg">
                    <a href="<?= $ar_res['DETAIL_PAGE_URL']; ?>"> <img
                                src="<?= NewImageR($arItem['DETAIL_PICTURE'], 150, 150); ?>" alt=""> </a>
                </div>
                <div class="cval">
                    <p>
                        <?= $ar_res['NAME']; ?>
                    </p>
                    <?
                    // Перебор размеров и
                    $res = CIBlockElement::GetProperty(CATALOG_IBLOCK_ID, $arItem['PRODUCT_ID'], "sort", "asc", array("CODE" => "POVERKHNOST"));
                    if ($ob = $res->GetNext()) {
                        $VALUE = $ob['VALUE'];
                        if ($VALUE != '') echo '<p>' . $ob['VALUE_ENUM'] . '</p>';
                    }
                    $res = CIBlockElement::GetProperty(CATALOG_IBLOCK_ID, $arItem['PRODUCT_ID'], "sort", "asc", array("CODE" => "FORMAT"));
                    if ($ob = $res->GetNext()) {
                        $VALUE = $ob['VALUE'];
                        if ($VALUE != '') echo '<p>' . $ob['VALUE_ENUM'] . '</p>';
                    }
                    ?>
                </div>
                <div class="hcalc">
                    <div class="hcost">
                        <span><?= $arItem['PRICE']; ?></span> руб./<?= $arItem['MEASURE_TEXT']; ?>
                    </div>
                    <div class="cartcalc clearfix">
                        <button class="ccalc-minus basketajax2">-</button>
                        <input class="basketid basketajax2" type="text" value="<?= $arItem['QUANTITY']; ?>"
                               basket-id="<?= $arItem['ID']; ?>"
                               dd="<?= str_replace(',', '.', $MINIMALNAYA_KRATNOST_ZAKAZA['VALUE']); ?>">
                        <button class="ccalc-plus basketajax2">+</button>
                        <span class="hcolval"><?= $arItem['MEASURE_TEXT']; ?></span>
                    </div>
                </div>
                <div class="hremove">
                    <button class="reset" basket-id="<?= $arItem['ID']; ?>"></button>
                </div>
            </div>

        <? } ?>


    </div>
    <div class="hcart-result">
        <div class="flex">
            <div class="el">
                <p>
                    Всего товаров:
                    <strong id="alltov">
                        <?= count($arResult['GRID']['ROWS']); ?>
                    </strong>
                </p>
            </div>
            <div class="el">
                <p>
                    На сумму:
                    <strong class="colorred">
                        <?= $arResult['allSum_FORMATED']; ?><span></span>
                    </strong>
                </p>
            </div>
        </div>
        <div class="linkmore">
            <a href="<?= $arParams['PATH_TO_ORDER']; ?>">
                Перейти в корзину
            </a>
        </div>
    </div>

    <script>
        $('.basketajax2').click(function () {

            console.log('Указать колличество');

            var id = $(this).parent('.cartcalc').find('.basketid').attr('basket-id');


            setTimeout(function () {


                var q = $('.basketajax2[basket-id=' + id + ']').val();

                $.get('/local/ajax/basket.php?edit=Y&id=' + id + "&q=" + q, function (data) {

                })
                    .done(function (data) {

                        // <strong class="colorred"> <span>
                        var pos = data.indexOf('<strong class="colorred">') + 26;
                        var ns = data.substr(pos, data.length - pos);

                        var pos = ns.indexOf('<span>');
                        var ns = ns.substr(2, pos - 2);


                        $('.colorred').html(ns);
                        hCartOpen();

                        ///


                    });


//BX.ajax.insertToNode(',"basket");
// BX.ajax.insertToNode('/local/basket/basket.php',"basketfull");

            }, 100);


        });


        function UpBasket3(aaa) {

            setTimeout(function () {

                var q = $('[basket-id=' + aaa + ']').find('.basketid').val();


                $.get('/local/ajax/basket.php?edit=Y&id=' + aaa + "&q=" + q, function (data) {

                })
                    .done(function (data) {


                        var pos = data.indexOf('<strong class="colorred">') + 26;
                        var ns = data.substr(pos, data.length - pos);

                        var pos = ns.indexOf('<span>');
                        var ns = ns.substr(2, pos - 2);

                        console.log('222222' + ns);

                        $('.colorred').html(ns);
                        hCartOpen();

                    });


//BX.ajax.insertToNode(',"basket");
// BX.ajax.insertToNode('/local/basket/basket.php',"basketfull");

            }, 100);
        }

        $('.basketid').focusout(function () {

            var id = $(this).parent('.cartcalc').find('.basketid').attr('basket-id');

            UpBasket3(id);


        });


        $('.basketid').keypress(function (event) {

            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {


                console.log('131313');

                var id = $(this).parent('.cartcalc').find('.basketid').attr('basket-id');


                UpBasket3(id);


            }
        });


        $('.reset').click(function () {

            var id = $(this).attr('basket-id');


            setTimeout(function () {

                var q = 0;

                var q = $('[basket-id=' + id + ']').val();

                $.get('/local/ajax/basket.php?edit=Y&id=' + id + "&q=" + q, function (data) {

                })
                    .done(function (data) {

                        $('.unit[basket-id=' + id + ']').remove();


                        var pos = data.indexOf('<strong class="colorred">') + 26;
                        var ns = data.substr(pos, data.length - pos);

                        var pos = ns.indexOf('<span>');
                        var ns = ns.substr(2, pos - 2);
                        $('.colorred').html(ns);


                        var pos = data.indexOf('<strong id="alltov">') + 23;
                        var ns = data.substr(pos, data.length - pos);


                        var pos = ns.indexOf('</strong>');
                        var ns = ns.substr(2, pos - 2);
                        var alltov = ns;


                        $('#alltov').html(alltov);

                        $('.countNumm').html(alltov);


                        hCartOpen();


//

                    });


//BX.ajax.insertToNode(',"basket");
// BX.ajax.insertToNode('/local/basket/basket.php',"basketfull");

            }, 100);


        });


    </script>