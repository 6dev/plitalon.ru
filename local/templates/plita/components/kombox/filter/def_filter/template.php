<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(method_exists($this, 'setFrameMode')) 
	$this->setFrameMode(true);
	
if(count($arResult['ELEMENTS']) > 1 && $arResult["ITEMS_COUNT_SHOW"]):
$arParams['MESSAGE_ALIGN'] = isset($arParams['MESSAGE_ALIGN']) ? $arParams['MESSAGE_ALIGN'] : 'LEFT';
$arParams['MESSAGE_TIME'] = intval($arParams['MESSAGE_TIME']) >= 0 ? intval($arParams['MESSAGE_TIME']) : 5;

include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/functions.php");

CJSCore::Init(array("ajax", "popup"));
$APPLICATION->AddHeadScript("/bitrix/js/kombox/filter/jquery.filter.js");
$APPLICATION->AddHeadScript("/bitrix/js/kombox/filter/ion.rangeSlider.js");
$APPLICATION->AddHeadScript("/bitrix/js/kombox/filter/jquery.cookie.js");
$cntClosedProperty = 0;
?>
<div class="kombox-filter" id="kombox-filter">
	<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get"<?if($arResult['IS_SEF']):?> data-sef="yes"<?endif;?>>
		<div class="it-filter-parameters-box lvl1 kombox-column col-xs-12 col-sm-6 col-md-6 col-lg-3">		
			<div class="kombox-filter-property-head">

				<span class="kombox-filter-property-name">Раздел</span>

				<span class="for_modef"></span>
			</div>

			<? 
			$page_catalog = $APPLICATION->GetCurPage();
			foreach ($arResult['SECTION'] as $valSect) { 
				if($page_catalog == $valSect['SECTION_PAGE_URL']){
					$name_r = $valSect['NAME']; 
				}
				?>
			<? } ?>


			<div class="drobdown" data-declname="<? echo $arItem['NAME']; ?>">

				<a class="parameters-box-hint" data-target="#" href="javascript:void(0);" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="display: block; border: 1px solid #ff1a22;">
            		<? if(strlen($name_r) > 0): ?>
            			<span class="subfilter-align" style="color: #000; width: 1000px; display: block;"><?echo $name_r;?></span>
            		<?else:?> 
                		<span class="subfilter-align" style="color: #000;">Выберите категорию</span>
                	<? endif; ?>
                </a>

				<div class="dropdown-menu" aria-labelledby="dLabel">
					<a href="/catalog/" class="checkbox_href">Все</a>
					<? foreach ($arResult['SECTION'] as $valSect) { ?>
						<? if($valSect['NAME'] == $name_r){ }else{?>
							<a href="<? echo $valSect['SECTION_PAGE_URL']; ?>" class="checkbox_href">
								<? echo $valSect['NAME']; ?> <span class="kombox-cnt">(<?=$valSect['COUNT_ELEMENT'];?>)</span>	
							</a>
						<? } ?>
					<? } ?>
				</div>

				<div class="kombox-clear">&nbsp;</div>

			 </div>

			
		</div>


		<?foreach($arResult["HIDDEN"] as $arItem):?>
			<input
				type="hidden"
				name="<?echo $arItem["CONTROL_NAME"]?>"
				id="<?echo $arItem["CONTROL_ID"]?>"
				value="<?echo $arItem["HTML_VALUE"]?>"
			/>
		<?endforeach;?>	
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?if($arItem['SETTINGS']['VIEW'] == "SLIDER"):?>
				<?if($arItem["CLOSED"])$cntClosedProperty++;?>
				<?if(isset($arItem["VALUES"]["MIN"]["VALUE"]) && isset($arItem["VALUES"]["MAX"]["VALUE"]) && $arItem["VALUES"]["MAX"]["VALUE"] > $arItem["VALUES"]["MIN"]["VALUE"]):?>
					<div class="it-filter-parameters-box lvl1 kombox-column<?if($arItem["CLOSED"]):?> kombox-closed<?endif;?> col-xs-12 col-sm-6 col-md-6 col-lg-3" data-id="<?echo $arItem["CODE_ALT"].'-'.$arItem["ID"]?>">
						
						<div class="kombox-filter-property-head" data-declname="<? echo $arItem['NAME']; ?>">

							<span class="kombox-filter-property-name">
								<? if($arItem['ID'] == 6): ?>
									Цена
								<? else: ?>
									<?echo $arItem['NAME']?>
								<? endif; ?>	
							</span>

							


							<?if(strlen($arItem['HINT'])):?>
								<span class="kombox-filter-property-hint"></span>
								<div class="kombox-filter-property-hint-text">
									<?echo $arItem['HINT']?>
								</div>
							<?endif;?>

							<span class="for_modef"></span>	
						</div>

						<a class="parameters-box-hint" style="display: block;">
                        	<span class="subfilter-align">Выберите: <? echo $arItem['NAME']; ?></span>
                   		</a>


                   		<div class="kombox-clear">&nbsp;</div>

						<!-- <div class="drobdown noclose" data-declname="<?// echo $arItem['NAME']; ?>"> -->
							<!-- <a class="parameters-box-hint " data-target="#" href="javascript:void(0);" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="display: block;">
			            		<?// if($arItem['CHECK_ITEMS']): ?>
									<? //if(count($arItem['CHECK_ITEMS']) > 1): ?>
										<span class="subfilter-align" style="color: #000; width: 1000px; display: block;">Выбрано: <?php //foreach ($arItem['CHECK_ITEMS'] as $valCheck): ?><?// echo $valCheck['VALUE']; if($valCheck != end($arItem['CHECK_ITEMS'])) { echo ', ';} ?><?php //endforeach ?></span>
									<? //else: ?>
										<span class="subfilter-align" style="color: #000;">Выбрано: <?// echo $arItem['CHECK_ITEMS'][0]['VALUE']; ?></span>
									<?// endif; ?>
								<? //else: ?>
	                        		<span class="subfilter-align">Выберите: <? //echo $arItem['NAME']; ?></span>
	                        	<?// endif; ?>
			                </a> -->

		                	<!-- <div class="dropdown-menu" aria-labelledby="dLabel"> -->
							
								<?//komboxShowField($arItem);?>
							
							<!-- </div> -->
							
							<!-- <div class="kombox-clear">&nbsp;</div> -->
						<!-- </div>	 -->


					</div>
				<?endif;?>
			<?endif;?>
		<?endforeach;?>	
		<?foreach($arResult["ITEMS"] as $key => $arItem):?>
			<?if(!empty($arItem["VALUES"]) && $arItem['SETTINGS']['VIEW'] != "SLIDER"):?>
				<?if($arItem["CLOSED"])$cntClosedProperty++;?>
				<div class="it-filter-parameters-box lvl1 kombox-column<?if($arItem["CLOSED"]):?> kombox-closed<?endif;?> col-xs-12 col-sm-6 col-md-6 col-lg-3" data-id="<?echo $arItem["CODE_ALT"].'-'.$arItem["ID"]?>"<?if($arItem["CLOSED"]):?> style="display:none;"<?endif;?>>
					
					<div class="kombox-filter-property-head">

						<span class="kombox-filter-property-name">
							<?echo $arItem["NAME"]?>	
						</span>

						<?if(strlen($arItem['HINT'])):?>
							<span class="kombox-filter-property-hint"></span>
							<div class="kombox-filter-property-hint-text">
								<?echo $arItem['HINT']?>
							</div>
						<?endif;?>

						<span class="for_modef"></span>
					</div>

					<div class="drobdown noclose" data-declname="<? echo $arItem['NAME']; ?>">
						<a class="parameters-box-hint" data-target="#" href="javascript:void(0);" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="display: block;">
							<? if($arItem['CHECK_ITEMS']): ?>
								<? if(count($arItem['CHECK_ITEMS']) > 1): ?>
									<span class="subfilter-align" style="color: #000; width: 1000px; display: block;">Выбрано: <?php foreach ($arItem['CHECK_ITEMS'] as $valCheck): ?><? echo $valCheck['VALUE']; if($valCheck != end($arItem['CHECK_ITEMS'])) { echo ', ';} ?><?php endforeach ?></span>
								<? else: ?>
									<span class="subfilter-align" style="color: #000;">Выбрано: <? echo $arItem['CHECK_ITEMS'][0]['VALUE']; ?></span>
								<? endif; ?>
							<? else: ?>
                        		<span class="subfilter-align">Выберите: <? echo $arItem['NAME']; ?></span>
                        	<? endif; ?>
                   		</a>
                   		<!-- <pre>
                   			<?// print_r($arItem); ?>
                   		</pre>
 -->
				<!-- 		<div class="dropdown-menu" aria-labelledby="dLabel">
							<?//komboxShowField($arItem);?>
						</div> -->

						<div class="kombox-clear">&nbsp;</div>

					 </div>

					

				</div>
			<?endif;?>
		<?endforeach;?>
		<div class="kombox-footer">
			<div class="kombox-filter-submit">
				<?//if($arResult['SET_FILTER']):?>
				<a href="<?=$arResult["DELETE_URL"]?>" class="kombox-del-filter"><?=GetMessage("KOMBOX_CMP_FILTER_DEL_FILTER")?></a>
				<?//endif;?>
				<input type="submit" id="set_filter" value="<?=GetMessage("KOMBOX_CMP_FILTER_SET_FILTER")?>" />
			</div>
			<div class="kombox-filter-show-properties kombox-show">
				<?if($cntClosedProperty):?>
				<a href=""><span class="kombox-show"><?=GetMessage("KOMBOX_CMP_FILTER_SHOW_FILTER")?></span><span class="kombox-hide"><?=GetMessage("KOMBOX_CMP_FILTER_HIDE_FILTER")?></span></a>
				<?endif;?>
			</div>
			<div class="kombox-clear">&nbsp;</div>
		</div>
		<div class="modef" id="modef" style="display:none">
			<!-- <div > -->
				<a href="<?echo $arResult["FILTER_URL"]?>" class="modef-wrap">
					<?echo GetMessage("KOMBOX_CMP_FILTER_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?>
					<?echo GetMessage("KOMBOX_CMP_FILTER_FILTER_SHOW")?>
					<span class="ecke"></span>
				</a>
			<!-- </div> -->
		</div>
	</form>
</div>
<script>
	$(function(){
		komboxFilterJsInit();
		$('#kombox-filter').komboxHorizontalSmartFilter({
			ajaxURL: '<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>',
			urlDelete: '<?echo CUtil::JSEscape($arResult["DELETE_URL"])?>',
			align: '<?echo $arParams['MESSAGE_ALIGN']?>',
			modeftimeout: <?echo $arParams['MESSAGE_TIME']?>,
			columns: <?echo intval($arParams['COLUMNS']) > 0 ? intval($arParams['COLUMNS']) : 3;?>
		});
	});
</script>
<?endif;?>

<script type="text/javascript">
$(document).on("click.bs.dropdown.data-api", ".noclose", function (e) { e.stopPropagation() });
</script>

<style>

	.it-filter-parameters-box .drobdown{position: relative;}
	.it-filter-parameters-box .dropdown-menu{top: 29px; width: 100%; border-radius: 0;}

	.parameters-box-hint{
		border: 1px solid #d0ccce;
	    padding: 6px 5px;
	    font-size: 16px;
	    position: relative;
	    cursor: pointer;
	    overflow: hidden;
	    text-decoration: none;
	}
	.parameters-box-hint:hover, .parameters-box-hint:active, .parameters-box-hint:focus, .parameters-box-hint:visited{text-decoration: none;}
	.parameters-box-hint:before{
	    content: "";
	    position: absolute;
	    width: 9px;
	    height: 5px;
	    right: 5px;
	    top: 50%;
	    margin-top: -3px;
	    z-index: 2;
	    background: url(/local/templates/plita/css/../images/selbef.png) no-repeat center center;
	    -webkit-transition: .3s;
	    transition: .3s;
	}
	.it-parameters-box.active .parameters-box-hint:before{
	    -webkit-transform: rotate(180deg);
	    transform: rotate(180deg);
	}
    
	.it-parameters-box .checkbox--wrapper{
		display: none;
		position: absolute;
		background: #fff;
		z-index: 99;
		border: 1px solid #d0ccce;
		padding: 10px 0;
		margin: 2px 0 0;
		width: calc(100% - 30px);
	}
	.it-parameters-box.active .checkbox--wrapper{
		display: block;
	}
	.parameters-box-hint .subfilter-align{
	    font-size: 16px;
	    color: rgb(208, 204, 206);
	}
	a.kombox-del-filter{
	    margin-right: 20px;
	    float: left;
	    padding: 10px 0px;
	}
	a.kombox-del-filter:hover{color: #40a23c;}

	.kombox-filter-submit input[type="submit"]{
	    background: #40a23c;
	    color: #fff;
	    border: none;
	    text-transform: uppercase;
	    font-size: 20px;
	    font-family: 'fb';
	    height: 36px;
	    margin-top: 12px;
	    width: 160px;
	    margin: 0;
	}
	.kombox-filter-submit input[type="submit"]:hover{
		background: #333;
	}
	.kombox-filter .checkbox_href{
		font-size: 14px;
		display: block;
		padding: 0px 15px;
		margin: 0 0 10px;
	}
	.kombox-filter .checkbox_href:hover{
		color: #40a23c;
	}
	#kombox-filter .kombox-combo .lvl2 label:hover, #kombox-filter .kombox-radio .lvl2 label:hover{color: #40a23c;}
	#kombox-filter .kombox-combo .lvl2.kombox-disabled label:hover, #kombox-filter .kombox-radio .lvl2.kombox-disabled label:hover{color: #333;}
	#kombox-filter .kombox-combo .lvl2.kombox-disabled.kombox-checked label:hover{color: #40a23c;}

	#kombox-filter .kombox-num .kombox-input{
		width: 50px;
		font-size: 12px;
		height: 32px;
		padding: 5px;
		border: 1px solid #d0ccce;
	}
	#kombox-filter .kombox-num .kombox-range{padding: 0 10px;}
	#kombox-filter .lvl2{width: 300px;}
	@media(max-width: 1200px){
			.sortingfilter .el1{padding-right: 15px;}
	}
	@media(max-width: 992px){
		.sortingfilter{display: none;}
		.catalog-filters .filterblock .infoval{
			margin: 0px auto 50px !important;
		}
	}
	@media(max-width: 450px){
		.catalog-filters .filterblock .info ul li.active .infofoods, .catalog-filters .filterblock .info ul li .infofoods{font-size: 14px;}
	}
</style>