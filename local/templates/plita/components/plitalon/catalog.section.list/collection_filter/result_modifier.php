<?
foreach($arResult["SECTIONS"] as $key => $arSection):
	if($arSection["DEPTH_LEVEL"] == 3):
		$arMassThreeLvl[] = $arSection; 
	endif;
endforeach;

foreach ($arMassThreeLvl as $valThreeLvl){
	foreach ($arParams['PARENT_ID'] as $valFiltSect) {
		if($valFiltSect == $valThreeLvl["ID"]){
			$massFinCollection[] = $valThreeLvl;
		}
	}
}



foreach($massFinCollection as $k => $v){
    $subArr[$k] = $v["DATE_CREATE"];
}
natsort($subArr);
$subArrTmp = $massFinCollection;
unset($massFinCollection);
foreach($subArr as $k => $v) {
    $massFinCollection[$k] = $subArrTmp[$k];
}



$i = 0;

foreach($massFinCollection as $key => $arSection):


		$arFile = CFile::GetFileArray($arSection["UF_PICS"][0]);
		if($arFile){
			$arImgSect = CFile::ResizeImageGet(
	            $arFile,
	            array("width" => 308, "height" => 250),
	            BX_RESIZE_IMAGE_EXACT,
	            true,
	            false
	        );
		} else{
			$arImgSect['src'] = '/local/templates/plita/images/no_img.jpg';
		}
		$arSection["RES_IMG"] = $arImgSect;

		$arResult["FILT_ITEMS"][$i] = $arSection;

		$arMass[] = $arSection['ID'];

		$arSort = array('propertysort_TIP_TOVARA' => 'ASC', 'catalog_PRICE_6' => 'ASC');
		$arSelect = Array("PREVIEW_PICTURE", "CATALOG_GROUP_6");
		$arFilter = Array("IBLOCK_ID"=>18, "SECTION_ID"=>$arSection['ID'], "ACTIVE"=>"Y");
		$res = CIBlockElement::GetList($arSort, $arFilter, false, Array("nTopCount"=>14), $arSelect);
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();

			$arFile = CFile::GetFileArray($arFields["PREVIEW_PICTURE"]);
			$arImgEl = CFile::ResizeImageGet(
	            $arFile,
	            array("width" => 34, "height" => 30),
	            BX_RESIZE_IMAGE_EXACT,
	            true,
	            false
	        );
			$arFields['RES_IMG'] = $arImgEl;

			$arResult["FILT_ITEMS"][$i]['ELEMENT'][] = $arFields;
		}

		$i = $i + 1;

endforeach;


foreach($arResult["SECTIONS"] as $keys => $arSectionTwo):
	if($arSectionTwo["DEPTH_LEVEL"] == 2){
		$arSectTwo[] = $arSectionTwo;
	}
endforeach;

foreach($arResult["FILT_ITEMS"] as $kk => $arNewSect):
	foreach ($arSectTwo as $valSection) {
		if($arNewSect['IBLOCK_SECTION_ID'] == $valSection['ID']){
			$arResult["FILT_ITEMS"][$kk]['PARENT_SECTION'] = $valSection;
		}
	}
endforeach;


?>

<!-- <pre>
	<?// print_r($massFinCollection); ?>
</pre> -->