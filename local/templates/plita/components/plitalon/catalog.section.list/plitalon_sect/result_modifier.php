<? 
$i = 0;

foreach($arResult["SECTIONS"] as $key => $arSection):
	if(($arSection["DEPTH_LEVEL"] == 2) && ($arSection["UF_BR"] == 1)):
	
		$arFile = CFile::GetFileArray($arSection["UF_PICS"][0]);
		if($arFile){
			$arImgSect = CFile::ResizeImageGet(
	            $arFile,
	            array("width" => 175, "height" => 175),
	            BX_RESIZE_IMAGE_EXACT,
	            true,
	            false
	        );
		} else{
			$arImgSect['src'] = '/local/templates/plita/images/no_img.jpg';
		}
			


		$arSection["RES_IMG"] = $arImgSect;
		$arResult["FILT_ITEMS"][$i] = $arSection; 

		$i = $i + 1;
	endif;
endforeach;

?>