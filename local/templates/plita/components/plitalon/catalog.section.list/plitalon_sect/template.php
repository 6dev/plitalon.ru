<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="partners wow fadeIn" data-wow-offset="200">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="slick slickpartners">
					<? foreach($arResult["FILT_ITEMS"] as $arSection){
						$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
						$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'))); ?>
						<div id="<?=$this->GetEditAreaId($arSection['ID']);?>">
							<a href="<?=$arSection['SECTION_PAGE_URL'];?>"><img src="<?=$arSection['RES_IMG']['src'];?>" alt="<?=$arSection['NAME'];?>"></a>
						</div>
					<? } ?>
                </div>
            </div>
        </div>
    </div>
</section>