<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

// print_r($arResult);
?>

<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p class="title">
						Рекомендуем
					</p>
					<div class="slick slick4">


<?

foreach ($arResult['SECTIONS'] as $arSection)
{

$UF=array();	
$arFilter = Array('IBLOCK_ID'=>$arSection['IBLOCK_ID'], 'ID'=>$arSection['ID']);
$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true,array('UF_*'),true);
  while($ar_result = $db_list->GetNext())
  {
	$UF= $ar_result; 
  }
	
	
	
	
	
	
	
	
	
	
	
	
	
	CModule::IncludeModule('iblock');
	
	$res = CIBlockSection::GetByID($arSection['IBLOCK_SECTION_ID']);
	$brands = $res->GetNext();


// Получить кардинку дл товара

$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_","PROPERTY_KARTINKA_KOLLEKTSII");
$arFilter = Array("IBLOCK_ID"=>$arSection['IBLOCK_ID'],"SECTION_ID"=>$arSection['ID'], "!PROPERTY_KARTINKA_KOLLEKTSII"=>'', "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1), $arSelect);
while($ob = $res->GetNextElement())
{
 
  $arFields = $ob->GetFields();
 
  $MORE_PHOTO = array();
    $res2 = CIBlockElement::GetProperty(CATALOG_IBLOCK_ID, $arFields['ID'], "sort", "asc", array("CODE" => "MORE_PHOTO"));
    while ($ob2 = $res2->GetNext())
    {
		
		$ob2['DESCRIPTION']=mb_strtolower($ob2['DESCRIPTION']);
		
        $MORE_PHOTO[] = $ob2;
    }

$e=explode(',',mb_strtolower($arFields['PROPERTY_KARTINKA_KOLLEKTSII_VALUE']));

$files=$e;

$fp=array();

foreach ($MORE_PHOTO as $file)
{

if (in_array($file['DESCRIPTION'].'.jpg',$files)) $fp[]=$file;
if (in_array($file['DESCRIPTION'].'.jpeg',$files)) $fp[]=$file;

// Смотрим все файлы для данного товара
	
}
 
 
}

// Товар с минимальной ценой 

$arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM","PROPERTY_","PROPERTY_KARTINKA_KOLLEKTSII","CATALOG_PRICE_4");
$arFilter = Array("IBLOCK_ID"=>$arSection['IBLOCK_ID'],"SECTION_ID"=>$arSection['ID'], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("CATALOG_PRICE_6"=>"ASC"), $arFilter, false, Array("nPageSize"=>1), $arSelect);
while($ob = $res->GetNextElement())
{
	  $arFields = $ob->GetFields();
	  
$price=0;
	  
$db_res = CPrice::GetList(
        array(),
        array(
                "PRODUCT_ID" => $arFields['ID'],
                "CATALOG_GROUP_ID" => 6
            )
    );
if ($ar_res = $db_res->Fetch())
{
	$price=$ar_res;
}

	
$ar_resP = CCatalogProduct::GetByID($arFields['ID']);	

 $res_measure = CCatalogMeasure::getList(array(),array("ID"=>$ar_resP['MEASURE']));
        while($measure = $res_measure->Fetch()) {
            $me=$measure['SYMBOL'];
        }   



$arPrice = CCatalogProduct::GetOptimalPrice($arFields['ID'], 1) ;


	
	  
}


$akcia='';

if ($arPrice['DISCOUNT_PRICE']<$price['PRICE']) $akcia=' akcia ';


?>


<div class="unitcard1 <?=$akcia;?> wow flipInY" data-wow-offset="100">
							<div class="uimg">
								<div class="imgwr">
									<a href="<?=$arSection['SECTION_PAGE_URL'];?>">
										<? if ($fp[0]['VALUE']>0) { ?><img src="<?=CFile::GetPath($fp[0]['VALUE']);?>" alt=""> <? } ?>
									</a>
								</div>
								<div class="uimg-abs">
									<div class="inform clearfix">
									
		<? if ($UF['UF_HIT']>0) { ?>
									
									
										<p class="hit">
											<span>Хит</span>
										</p>
										
		<? } ?>		

        <? if ($UF['UF_NEW']>0) { ?>		
										
										<p class="new">
											<span>Новинка</span>
										</p>
										
		<? } ?>									
										
										
										<a href="" class="liked " index-id2="<?=$arSection['ID'];?>">
											<img src="<?=SITE_TEMPLATE_PATH;?>/images/liked.png" alt="">
										</a>
									</div>
									<p class="cost">
									
			<? if ($arPrice['DISCOUNT_PRICE']<$price['PRICE']) { ?>				
									
									<? if ($price['PRICE']>0) { ?>	<span>от <strong><?=number_format($arPrice['DISCOUNT_PRICE'], 0, '.', ' ');?></strong> руб./<?=$me;?></span> <? } ?>
									<del><?=number_format($price['PRICE'], 0, '.', ' ');?> руб./<?=$me;?></del>
			<? } else { ?>							
			
			<? if ($price['PRICE']>0) { ?>	<span>от <strong><?=number_format($price['PRICE'], 0, '.', ' ');?></strong> руб./<?=$me;?></span> <? } ?>
			
			<? } ?>
										
										
									</p>
								</div>
							</div>
							<div class="unval">
								<p class="tit">
									<a href="<?=$arSection['SECTION_PAGE_URL'];?>">
										<?=$arSection['NAME'];?>
									</a>
								</p>
								<p class="model">
								<a href="<?=$brands['SECTION_PAGE_URL'];?>">
									<?=$brands['NAME'];?>
								</a>
								</p>
								<div class="example">
									<span>
										<img src="images/ex1.png" alt="">
									</span>
									<span>
										<img src="images/ex2.png" alt="">
									</span>
									<span>
										<img src="images/ex3.png" alt="">
									</span>
									<span>
										<img src="images/ex4.png" alt="">
									</span>
									<span>
										<img src="images/ex5.png" alt="">
									</span>
									<span>
										<img src="images/ex6.png" alt="">
									</span>
									<span>
										<img src="images/ex7.png" alt="">
									</span>
									<span>
										<img src="images/ex1.png" alt="">
									</span>
									<span>
										<img src="images/ex2.png" alt="">
									</span>
									<span>
										<img src="images/ex3.png" alt="">
									</span>
									<span>
										<img src="images/ex4.png" alt="">
									</span>
									<span>
										<img src="images/ex5.png" alt="">
									</span>
									<span>
										<img src="images/ex6.png" alt="">
									</span>
									<span>
										<img src="images/ex7.png" alt="">
									</span>
								</div>
							</div>
						</div>


<?	
	
	
}

?>

					</div>
				</div>
			</div>
		</div>