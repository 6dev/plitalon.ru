<?
global $APPLICATION;

use Bitrix\Main\Page\Asset; ?>

    <!DOCTYPE html>
    <html lang="ru">
    <head>
        <meta charset="utf-8">
        <title><?= $APPLICATION->ShowTitle(); ?></title>
        <?
        $APPLICATION->ShowMeta("keywords");
        $APPLICATION->ShowMeta("description");

        Asset::getInstance()->addString('<link href="' . SITE_TEMPLATE_PATH . '/css/fonts/Lato-Regular_0.woff2"  as="font" rel="preload" crossorigin="anonymous"/>');
        Asset::getInstance()->addString('<link href="' . SITE_TEMPLATE_PATH . '/css/fonts/Lato-Semibold.woff2"  as="font" rel="preload" crossorigin="anonymous"/>');
        Asset::getInstance()->addString('<link href="' . SITE_TEMPLATE_PATH . '/css/fonts/Lato-Regular_0.woff2"  as="font" rel="preload" crossorigin="anonymous"/>');
        Asset::getInstance()->addString('<link href="' . SITE_TEMPLATE_PATH . '/css/fonts/Lato-Medium.woff2"  as="font" rel="preload" crossorigin="anonymous"/>');
        Asset::getInstance()->addString('<link href="' . SITE_TEMPLATE_PATH . '/css/fonts/Lato-Light.woff2"  as="font" rel="preload" crossorigin="anonymous"/>');
        Asset::getInstance()->addString('<link href="' . SITE_TEMPLATE_PATH . '/css/fonts/Lato-Heavy.woff2"  as="font" rel="preload" crossorigin="anonymous"/>');
        Asset::getInstance()->addString('<link href="' . SITE_TEMPLATE_PATH . '/css/fonts/Lato-Bold_0.woff2"  as="font" rel="preload" crossorigin="anonymous"/>');
        Asset::getInstance()->addString('<link href="' . SITE_TEMPLATE_PATH . '/css/fonts/Lato-Black_1.woff2"  as="font" rel="preload" crossorigin="anonymous"/>');

        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/pitalon.css", true);
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/FontAwe.css", true);
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/glyphicon.css", true);
        Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/owl.carousel.min.css", true);

        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jq.js");
//        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery-ui.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/bootstrap_old.min.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/adapter.min.js");
//        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/touch-punch.min.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.mCustomScrollbar.concat.min.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jcoo.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/lightbox.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/fancybox/source/jquery.fancybox.pack.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/jquery.nice-select.min.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/slick.min.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/wow.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/owl.carousel.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/script.js");
        Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/main.js");


        Asset::getInstance()->addString('<meta http-equiv="X-UA-Compatible" content="IE=edge" />');
        Asset::getInstance()->addString('<meta name="format-detection" content="telephone=no" />');
        Asset::getInstance()->addString('<meta name="viewport" content="width=device-width, initial-scale=1">');
        Asset::getInstance()->addString('<link rel="shortcut icon" href="' . SITE_TEMPLATE_PATH . '/images/favicon.ico" type="image/ico">');
        Asset::getInstance()->addString('<link rel="apple-touch-icon" sizes="180x180" href="' . SITE_TEMPLATE_PATH . '/images/apple-touch-icon.png">');
        Asset::getInstance()->addString('<link rel="icon" type="image/png" sizes="32x32" href="' . SITE_TEMPLATE_PATH . '/images/favicon-32x32.png">');
        Asset::getInstance()->addString('<link rel="icon" type="image/png" sizes="16x16" href="' . SITE_TEMPLATE_PATH . '/images/favicon-16x16.png">');
        Asset::getInstance()->addString('<link rel="icon" type="image/png" sizes="192x192" href="' . SITE_TEMPLATE_PATH . '/images/favicon-192x192.png">');
        Asset::getInstance()->addString('<link rel="icon" type="image/png" sizes="512x512" href="' . SITE_TEMPLATE_PATH . '/images/favicon-512x512.png">');
        Asset::getInstance()->addString('<link rel="manifest" href="' . SITE_TEMPLATE_PATH . '/images/site.webmanifest">');
        Asset::getInstance()->addString('<link rel="mask-icon" href="' . SITE_TEMPLATE_PATH . '/images/safari-pinned-tab.svg" color="#5bbad5">');
        Asset::getInstance()->addString('<meta name="msapplication-TileColor" content="#da532c">');
        Asset::getInstance()->addString('<meta name="yandex-verification" content="f1493e8644ba439c" />');
        Asset::getInstance()->addString('<meta name="google-site-verification" content="3OK-PMmaTUhBsKfQMraQQgBrFzy9XstwW0QOctkM92w" />');
        $APPLICATION->ShowCSS();
        $APPLICATION->ShowHeadStrings();
        $APPLICATION->ShowHeadScripts();
        ?>
        <!--  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&display=swap" rel="stylesheet">-->
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
<!--        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>-->
<!--        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
        <![endif]-->
    </head>
<body>
    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-524J6VZ');
    </script>
    <!-- End Google Tag Manager -->
    <!-- Yandex.Metrika counter -->
    <script>
        (function (m, e, t, r, i, k, a) {
            m[i] = m[i] || function () {
                (m[i].a = m[i].a || []).push(arguments)
            };
            m[i].l = 1 * new Date();
            k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
        })
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(66466192, "init", {
            clickmap: true,
            trackLinks: true,
            accurateTrackBounce: true,
            webvisor: true
        });
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/66466192" style="position:absolute; left:-9999px;" alt=""/></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-524J6VZ"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
<? if ($USER->IsAdmin()): ?>
    <div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
<? endif ?>
<? if ($_REQUEST['print'] != 'Y') { ?>
    <?
    ob_start();
// включаемая область для раздела
    include($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/includes/phone.php");
    $d = ob_get_contents();
    ob_end_clean();
    ?>
    <header class="header css-topnav">
        <div class="bars">
            <button class="c-hamburger c-hamburger--htx cd-nav-trigger" data-cdnav="1">
                <span class="cd-nav-icon"></span>
            </button>
        </div>
        <div class="container-fluid footerc">
            <div class="flex">
                <div class="logo">
                    <a href="/" class="logo-link">
                        <img src="https://plitalon.ru/local/templates/plita/images/logo.png" alt="">
                        <span style="position: absolute;opacity: 0">Logo</span>
                    </a>
                </div>
                <div class="hsearch">
                    <a href="#" class="jsclosesearch">
                        <img src="<?= SITE_TEMPLATE_PATH; ?>/images/cartremove.png" alt="">
                    </a>
                    <form action="/search/">
                        <input id="header-search" name="q" type="text" placeholder="Поиск" autocomplete='off'>
                        <label for="header-search" style="position: absolute; opacity: 0;z-index: -1">Поиск</label>
                        <button type="submit">
                            <span style="position: absolute; opacity: 0;z-index: -1">Поиск</span>
                        </button>
                    </form>
                    <div id="interactive_result" class="new" style='display:none; z-index:10000;'></div>
                </div>
                <div class="hsearch hsearchmob">
                    <a href="#" class="jsclosesearch">
                        <i class="">&times;</i>
                    </a>
                    <form action="/search/">
                        <label for="header-search-mobile"></label>
                        <input id="header-search-mobile" type="text" name='q' placeholder="Поиск" autocomplete='off'>
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                <div class="hcontact">
                    <p class="phone">
                        <a href="tel:<?= str_replace('-', '', $d); ?>">
                            <? $dn = str_replace('-00', '-<span>00</span>', $d);
                            echo $dn; ?>
                        </a>
                    </p>
                    <p class="email">
                        <button data-toggle="modal" OnClick="$('#send').modal('show');" data-target="#myModal">
                            Заказать звонок
                        </button>
                    </p>
                </div>
                <div class="htime">
                    <?
                    // включаемая область для раздела
                    $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/includes/work.php", array(), array(
                        "MODE" => "html",                                           // будет редактировать в веб-редакторе
                        "NAME" => "Режим работы",      // текст всплывающей подсказки на иконке
                    ));
                    ?>
                </div>
                <div class="hicons">
                    <button class="button searchmob hi">
                        <i class="fa fa-search"></i>
                    </button>
                    <a href="tel:<?= str_replace('-', '', $d); ?>" class="hi hi0">
                        <i></i>
                        <span style="position: absolute;opacity: 0">Phone</span>
                    </a>
                    <a href="/personal/" class="hi hi1">
                        <i></i>
                        <span style="position: absolute;opacity: 0">Personal</span>
                    </a>
                    <a href="/contacts/" class="hi hi2">
                        <i></i>
                        <span style="position: absolute;opacity: 0">Contacts</span>
                    </a>
                    <a href="/favorites/" class="hi hi3">
                        <i></i>
                        <span id="likeh"><? require_once($_SERVER['DOCUMENT_ROOT'] . "/local/ajax/like.php"); ?></span>
                    </a>
                    <span class="hi98 js-cartopen" id="basket">
                        <? require_once($_SERVER['DOCUMENT_ROOT'] . "/local/ajax/basket.php"); ?>
                    </span>
                </div>
            </div>
        </div>
    </header>
    <div class="fix_height"></div>
    <section class="navigate hnavigate">
        <div class="navigate-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "top",
                            array(
                                "ROOT_MENU_TYPE" => "top",
                                "MAX_LEVEL" => "1",
                                "CHILD_MENU_TYPE" => "top",
                                "USE_EXT" => "Y",
                                "DELAY" => "N",
                                "ALLOW_MULTI_SELECT" => "Y",
                                "MENU_CACHE_TYPE" => "A",
                                "MENU_CACHE_TIME" => "3600000",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "MENU_CACHE_GET_VARS" => array(),
                                "COMPONENT_TEMPLATE" => "top",
                                "COMPOSITE_FRAME_MODE" => "A",
                                "COMPOSITE_FRAME_TYPE" => "AUTO"
                            ),
                            false
                        ); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="phonemob">
            <a href="tel:<?= str_replace('-', '', $d); ?>">
                <?= $dn; ?>
            </a>
        </div>
    </section>
    <? if ($APPLICATION->GetCurDir() != '/'): ?>
        <? $APPLICATION->IncludeComponent(
            "bitrix:breadcrumb",
            "plitalon",
            array(
                "START_FROM" => "0",
                "PATH" => "",
                "SITE_ID" => "s1",
                "COMPONENT_TEMPLATE" => "plitalon"
            ),
            false
        ); ?>
    <? endif; ?>
<? } ?>
<? if (TYPEPAGE == 'text') { ?>
    <section class="allbrands">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title2">
						<span>
							<?= $APPLICATION->ShowTitle(false); ?>
						</span>
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <section class="exposition">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
<? } ?>