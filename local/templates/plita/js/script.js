function sliderStarter(){
  $('.slick4').slick({
    slidesToShow: 4,
    responsive: [
      {
        breakpoint: 1440,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 660,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });
  
  $('.slick3').slick({
    slidesToShow: 3,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      }
    ]
  });
  
  $('.slick4dot').slick({
    arrows: false,
    slidesToShow: 4,
    dots: true,
    responsive: [
      {
        breakpoint: 1440,
        settings: {
          arrows: false,
          slidesToShow: 3
        }
      },
      {
        breakpoint: 992,
        settings: {
          arrows: false,
          slidesToShow: 2
        }
      },
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          slidesToShow: 2
        }
      },
      {
        breakpoint: 650,
        settings: {
          arrows: false,
          slidesToShow: 1
        }
      }
    ]
  });
  
  $('.slickpartners').slick({
    arrows: false,
    slidesToShow: 4,
    slidesToScroll: 4,
    dots: true,
    autoplay: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 3
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      }
    ]
  });

  // $('.slickcard').slick({
  //   slidesToShow: 1,
  //   slidesToScroll: 1,
  //   arrows: false,
  //   fade: true,
  //   asNavFor: '.navcard'
  // });
  // $('.navcard').slick({
  //   slidesToShow: 3,
  //   slidesToScroll: 1,
  //   asNavFor: '.slickcard',
  //   arrows: false,
  //   dots: false,
  //   // centerMode: true,
  //   focusOnSelect: true
  // });


  $('.slickport').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.portnaver'
  });
  $('.portnaver').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slickport',
    dots: false,
    centerMode: true,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 3
        }
      },
      {
        breakpoint: 650,
        settings: {
          arrows: false,
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 2
        }
      }
    ]
  });


  var i = 1;
  $('.units3 .el').each(function(){

    $(this).find('.portslick'+i).slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.portnav'+i
    });
    $(this).find('.portnav'+i).slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: '.portslick'+i,
      dots: false,
      // centerMode: true,
      focusOnSelect: true
    });
    i++;
  });
  
  
  
 
   $('.sliderexpo').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: '.portslick',
      dots: false,
      // centerMode: true,
      focusOnSelect: true
    });
  
  
  
  
  
  
  
  
}

function  moreInfo() {
  $('.infomore button').click(function(){
    $(this).closest('.val').find('p.info:nth-child(n+4)').slideToggle();
  });
}


function SetButLicked(vvv)
{
	
	
  

 $('.liked').click(function() { 
  
  var id=$(this).attr('index-id2');
  
  

  

  
  if ($.cookie('likes')!=undefined) {
	  
	  
	  
	    var likes=$.cookie('likes').split(',');
		
		
	if ($.inArray(id, likes) < 0) likes.push(id);
	
	
	 if (!$(this).hasClass('active')) {
	  	if ($.inArray(id, likes) < 0) likes.push(id);
  } else {
	  likes.splice(likes.indexOf(id), 1);
 
  }
	
	
	
	
	
	
	

	
  } else {
	likes=[id];  
  }
  
  
  
  
    if (!$(this).hasClass('active')) {
	  $(this).addClass('active');
  } else {
	  $(this).removeClass('active');
  }
  
  
  liken='';
  
  

  
 likes.forEach(function(item, i, arr) { 
 if (item!="") 
 {
	 liken=liken+item; 
	 if (i<(likes.length-1)) liken=liken+","; 
 }
 });


$.cookie('likes',liken,{ expires: 7, path: '/'});

$.get('/local/ajax/like.php').done(function( data ) { 
		
$('#likeh').html(data);
		
		});






  
  return false;
  
  });
  
} 
  
  
 function SetButBasket(vvv) 
 {
	 
	 
	 
$('.cardun1-button').click(function(){
	
	

    $(this).removeClass('cardgreen');
	$(this).html('в корзине');

var id=$(this).attr('index-id');
var tit=$(this).attr('title');
$('#greate').find('#nametovar').html(tit);
$('#greate').modal('show');	

var q=$(this).parent('.cardcalc').find('input').val();		

 $.get('/local/ajax/basket.php?id='+id+"&q="+q).done(function( data ) { 
		

		
$('#basket').html(data);

  cardTabs();
  cartCalc();
  hCartOpen();
  
 $(".js-customscroll,.lk .unit2 .val").mCustomScrollbar();
  headerScroll();

	
		
		});
 
	  
});

 } 

function hCartOpen(){
	
  $('.js-cartopen button.hi4').click(function(){
    var a = $(this).closest('.js-cartopen').attr('data-b');
      $(this).closest('.js-cartopen').find('.cartheaderlist').slideToggle();
      $(this).closest('.js-cartopen').find('.hi4').toggleClass('active');
      $(this).closest('.js-cartopen').find('.hi4 img').toggleClass('open');
      $(this).closest('.js-cartopen').attr('data-b','1');
  });
  $(document).mouseup(function (e){ 
    
    var t = $('.js-cartopen').attr('data-b');
    if(t == '1'){
    var div = $(".js-cartopen"); 
      if (!div.is(e.target) 
          && div.has(e.target).length === 0) { 
        
        $('.js-cartopen .cartheaderlist').slideUp();
        $('.js-cartopen').find('.hi4').removeClass('active');
        $('.js-cartopen').find('.hi4 img').toggleClass('open');
        $('.js-cartopen').attr('data-b','0');
      }
    }
  });
}

function cartCalc(vvv){
	

  if ($.cookie('likes')!=undefined) {
	  
	  
	  var likes=$.cookie('likes').split(',');
		
		console.log('likes'+$.cookie('likes'));
		
 likes.forEach(function(item, i, arr) { 
 
 console.log('item'+item);
 
if (item!="") $('[index-id2='+item+']').addClass('active');

 
 });	
	
		
		
		
  

  } 
  

  
  
  

	
	
	
	
		
basket.forEach(function(item, i, arr) {	



$('[index-id='+item+']').removeClass('cardgreen');
$('[index-id='+item+']').html('в корзине');

});
	
	
$('.cartcalc').focusout(function(){
 
    var a = $(this).closest('.cartcalc').find('input').val();
	var dd= $(this).closest('.cartcalc').find('input').attr('dd');
	
	var b=Math.ceil(Number(a)/Number(dd))*Number(dd);	
   $(this).closest('.cartcalc').find('input').val(b.toFixed(3).replace(/0*$/,""));
 
 
});


$('.cartcalc').keypress(function(event){
	
	
	
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
	
	
       
    var a = $(this).closest('.cartcalc').find('input').val();
	var dd= $(this).closest('.cartcalc').find('input').attr('dd');
	
	var b=Math.ceil(Number(a)/Number(dd))*Number(dd);	
   $(this).closest('.cartcalc').find('input').val(b.toFixed(3).replace(/0*$/,""));
 
	   
	   
	   
	   
    }
});







	
  $('.cartcalc .ccalc-minus').click(function(){
    var a = $(this).closest('.cartcalc').find('input').val();
	var dd= $(this).closest('.cartcalc').find('input').attr('dd');
    if(Number(a)>Number(dd)){
      var b = Number(a)-Number(dd);
      $(this).closest('.cartcalc').find('input').val(b.toFixed(3).replace(/0*$/,""));
    }else{
      $(this).closest('.cartcalc').find('input').val(a.toFixed(3).replace(/0*$/,""));
    }
  });
  $('.cartcalc .ccalc-plus').click(function(){
	  
    var a = $(this).closest('.cartcalc').find('input').val();
	var dd= $(this).closest('.cartcalc').find('input').attr('dd');
    var b = Number(a)+Number(dd);
    $(this).closest('.cartcalc').find('input').val(b.toFixed(3).replace(/0*$/,""));
  });
}

function menuBars(){
	$('.c-hamburger').click(function(e){
		e.preventDefault();
		$('.c-hamburger').toggleClass('is-active');
		$('body').toggleClass('openmobmenu');
	});

  $(document).mouseup(function (e){ 
    var div = $(".navigate,.bars"); 
    if (!div.is(e.target) 
        && div.has(e.target).length === 0) { 
      
      $('.c-hamburger').removeClass('is-active');
      $('body').removeClass('openmobmenu');
    }
  });
}

function headerScroll(){
  var ch = $('.sidebar').html();
  if(ch){
    var a = $('.checkout .sidebar').offset();
    var b = a.top;
    var c = $('.header').height();
    var col = $('.col-md-8').offset();
    var co = col.top;
    var u = $('.footer').height();
    var d = $('.navigate.hnavigate').height();
    var e = $('.navigate.botnav').offset();
    var g = $('.sidebar').height();
    var m = $('body').height();
    var f = e.top;
    
  }

  $(window).scroll(function(){
	  
	  WidSize();
	  
	  
	  $('#interactive_result').hide();
	  
	  
	  
	  var ch = $('.sidebar').html();
  if(ch){
    var a = $('.checkout .sidebar').offset();
    var b = a.top;
    var c = $('.header').height();
    var col = $('.col-md-8').offset();
    var co = col.top;
    var u = $('.footer').height();
    var d = $('.navigate.hnavigate').height();
    var e = $('.navigate.botnav').offset();
    var g = $('.sidebar').height();
    var m = $('body').height();
    var f = e.top;  
	  
  }
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	 
	  
    if ($(this).scrollTop() > 100) {
      $('body').addClass('fixheader');
    } else {
      $('body').removeClass('fixheader');
    }
    if(ch){
		 
		
      if (($(this).scrollTop() > ($('.allbrands').height()+$('.bredcrumbs').height()+$('.header').height()+$('.navigate-wrap').height())) && ($(this).scrollTop()<$('#orderfull').height()-g+$('.allbrands').height())){
		  
        $('.checkout .sidebar').addClass('fix').removeClass('fixbot');
        $('.sidebar').css('top', 150);
      } 
	  
	  else if($(this).scrollTop() > ($('#orderfull').height()-g+$('.allbrands').height())){
		  
        $('.checkout .sidebar').addClass('fixbot');
        $('.checkout .sidebar').css('top', $('#orderfull').height()-g-$('.allbrands').height()-$('.bredcrumbs').height());
      }
	  
	   if ($(this).scrollTop() < ($('.allbrands').height()+$('.bredcrumbs').height()+$('.header').height()+$('.navigate-wrap').height()) ) {
		  
        $('.checkout .sidebar').addClass('fixbot');
        $('.checkout .sidebar').css('top', $('.allbrands').height());
      } 
	  
	  
	  
    }
  });
}



function outputLabelFilter(){
  $('.selecter p.labwr:not(.nothis)').click(function(){
    var a = $(this).find('label').text();
    $(this).closest('.unit').find('.input').text(a);
    var p = $(this).position();
    $(this).closest('.unit').find('.window-result').css('top', p.top-18);
    $(this).closest('.unit').find('.window-result').addClass('active');
  });
  $('.slrange').mouseup(function(){
    var p = $(this).position();
    $(this).closest('.unit').find('.window-result').css('top', p.top-28);
    $(this).closest('.unit').find('.window-result').addClass('active');
  });

}

function filterOpenOption(){
  $('.unit .input').attr('data-open', 1);
  $('.unit .input').click(function(){
    var a = $(this).attr('data-open');
    if(a == 1){
      $('.unit .input').removeClass('active');
      $('.js-opener').slideUp(0);
      $('.unit .input').attr('data-open', 1);
      $(this).toggleClass('active');
      $(this).closest('.unit').find('.js-opener').slideDown(300);
      $(this).addClass('active');
      $(this).attr('data-open',0);
    }else{
      $(this).toggleClass('active');
      $(this).closest('.unit').find('.js-opener').slideUp(100);
      $(this).removeClass('active');
      $(this).attr('data-open',1);
    }
    $(document).mouseup(function (e){ 
      var div = $(".js-opener");
      if (!div.is(e.target)
          && div.has(e.target).length === 0) { 
        div.hide(); 
      }
    });
  });
}

function cardTabs(){
  $(".buttons a").click(function(e){
    e.preventDefault();
    $(this).tab('show');
    $(".buttons a").removeClass('active');
    $(this).addClass('active');
  });

  $("#myTab a").click(function(e){
    e.preventDefault();
    $(this).tab('show');
    $("#myTab a").removeClass('active');
    $(this).addClass('active');
  });
}

function tabCheckout(vvv){
	
	
	
  $('.tabdel').click(function(){
    $(this).parent('.df').find('.tabdel').removeClass('active');
    $(this).addClass('active');
    var a = $(this).attr('data-tab');
    $(this).closest('.unitdouble').find('.delres').removeClass('active');
    $(this).closest('.unitdouble').find('.delres'+a).addClass('active');
  });
}

function resizeCard(){
  $('.unitcard1wr').each(function(){
    var a = $(this).height();
    $(this).css('height', a);
    
  });
}

function searchOpen(){
  $('.searchmob,.jsclosesearch').click(function(e){
    e.preventDefault();
    $('.hsearchmob').slideToggle();
  });

  $(document).mouseup(function (e){ 
    var div = $(".hsearchmob"); 
    if (!div.is(e.target) 
        && div.has(e.target).length === 0) { 
      
      $('.hsearchmob').slideUp();
    }
  });
}





$(document).ready(function() {
	
$('.pagin_numbcard').eq(0).hide();

  searchOpen();
  cardTabs();
	menuBars();
  sliderStarter();
  cartCalc();
  hCartOpen();
  headerScroll();
  outputLabelFilter();
  filterOpenOption();
  tabCheckout();
  SetButBasket();
  SetButLicked();


  var b = $(window).height();



   $('select').niceSelect();

   $(window).on("load",function(){
      $(".js-customscroll,.lk .unit2 .val").mCustomScrollbar();
   });

   $(window).resize(function(){
    var b = $(window).height();
    if(b<350){
	  $(".js-customscroll,.lk .unit2 .val").mCustomScrollbar();
    }
    headerScroll();
   });
   
   
   $( ".recopassform" ).submit(function( event ) {
  
var email=$('.recopassform').find('[name=USER_LOGIN]').val();
  
 var stat=$('.recopassform').find('[name=stat]').val();
 
 
  
  if (stat=='recove') {
  
 $.get( "/local/ajax/get.php", {stat:stat,email:email} )
	    .done(function( data ) { 

if (data=="0") { $('.shmess').hide();$('.showerror').show();   }
if (data=="1") { $('.shmess').hide();$('.showmessage').show(); }

		});	
  }
  
  
  if (stat=='save') {
	  

  var email=$('.recopassform').find('[name=USER_LOGIN]').val();
  var pass=$('.recopassform').find('[name=PASSWORD]').val();
  var code=$('.recopassform').find('[name=CODEP]').val();
  var pass2=$('.recopassform').find('[name=PASSWORD2]').val();
  
 $.get( "/local/ajax/get.php", {stat:stat,email:email,pass:pass,code:code,pass2:pass2} )
	    .done(function( data ) { 

if (data=="0") { $('.shmess').hide();$('.showerror1').show();   }
if (data=="1") { $('.shmess').hide();$('.showmessage1').show(); }
if (data=="2") { $('.shmess').hide();$('.showerror2').show();   }

		});	
	  
	  
	  
  }
  
   
  
  
  return false;
  
});
   
   
      $( ".authpassform" ).submit(function( event ) {
 
  var email=$('.authpassform').find('[name=USER_LOGIN]').val();
  var pass=$('.authpassform').find('[name=PASSWORD]').val(); 
  var stat='auth';
 
   $.get( "/local/ajax/get.php", {email:email,pass:pass,stat:stat} )
	    .done(function( data ) { 
  
if (data=='0') setTimeout("$('.autherror').show();", 1000);

if (data=='1') location.reload();
  
  		});	

});
 
 

$('[name="PersonType"]').change(function(){
	
	$('.personaltypes').hide();

	$('.personaltype_'+$(this).val()).show();
	
	
	 
});

$( "[name=formreg]" ).submit(function( event ) {
	
	var ptype=$('[name=PersonType]:checked').val();
	
	

	
	
	
	dd="/local/ajax/get.php?formreg=Y&ptype="+ptype+"&"+$(this).serialize();
	
	dd.replace('&NAME_ORG=&INN=&UR_ADD=&BANK_NAME=&RSCHET=&KSCHET=&BIK=&FIO=&EMAIL=&PHONE=&FAX=&password=&password2=',''); // РЈРґР°Р»СЏРµРј РєРѕРґ РѕС‚ РІС‚РѕСЂРѕР№ С„РѕСЂРјС‹
	
	
	

 $.get(dd).done(function( data ) { 
		
	
		
		if (data=="OK")
		{
			
			$('.btnformsend').after('<div style="color:#00ff00;">Р’С‹ СѓСЃРїРµС€РЅРѕ Р·Р°СЂРµРіРёСЃС‚СЂРёСЂРѕРІР°Р»РёСЃСЊ</div>');
			
			window.location.href="/personal/";
			
			$('.btnformsend').remove();
			
		} else {
		
if ((data=="YEMAIL") && (!$('#erroremail').length)) {
	$('[name=EMAIL]').after('<div style="color:#ff0000;" id="erroremail">РўР°РєРѕР№ РµРјР°Р№Р» СѓР¶Рµ СЃСѓС‰РµСЃС‚РІСѓРµС‚</div>');

} else {
	
	$('#erroremail').remove();
	
	$('[name=EMAIL]').after('<div style="color:#ff0000;" id="erroremail">'+data+'</div>');
	
	
}

 }
		
		});


	
return false;	
	
});


$( ".work" ).click(function( event ) {
	
	$('.work').removeClass('active');
	
	// $('.work').css('color','#000000');
	
	$('.work').each(function(index,value){
var idw=$(this).attr('idw');
//if ($('.work'+idw).length<=0) $(this).css('color','#d0ccce');	
//if ($('.work'+idw).length>0) $(this).css('color','#000000');	
});
	
	$(this).addClass('active');
//	$(this).css('color','#40a23c');	 
	
	idw = $(this).attr('idw');
	$('.el').hide();
	$('.work'+idw).show();
	
	
	
});


$( ".alph2" ).find('.option').click(function( event ) {
city=$(this).attr('data-value');	

if (city!='') {	
$('.el').hide();
$('.city'+city).show();
} else {
	$('.el').show();
}
	
});



$( ".titfav a" ).click(function( event ) {
	
	
	$( ".titfav" ).show();
	$(this).parent('.titfav').hide();
	
	var idv=$(this).attr('idv');
	
	
	
	

	
	
 $('.catalog_container').hide();
 $('#'+idv).show();
	
	
	return false;
	
	
});



$( ".formorder" ).submit(function( event ) {
		
	var ptype=$('[name=PersonType]:checked').val();
	
	
	var loc=$('[name="LOCATION"]').val();
	
	$.cookie('LOCATION',loc,{ expires: 7, path: '/'});	
	
	
	dd="/local/ajax/get.php?formorder=Y&ptype="+ptype+"&"+$(this).serialize();
	

 $.get(dd).done(function( data ) { 
		


location.href="/basket/order/?ORDER_ID="+data;
		
		});


	
return false;	
	
});



$("[name=q]").keyup(function() {


var q=$("[name=q]").val();

console.log('q='+q);

 $.get( "/local/ajax/search.php", {q:q} )
	    .done(function( data ) { 
  $('#interactive_result').show();
  
  $('#interactive_result').css('index-x',1000);
  $('#interactive_result').html(data);
  
		});	

  
});


$(document).mouseup(function (e) {
    var container = $("#interactive_result");
    if (container.has(e.target).length === 0){
        container.hide();
    }
});







$( "#loadfile" ).change(function() {
	
	
	
	 if ($(this).val().lastIndexOf('\\')) {
        var i = $(this).val().lastIndexOf('\\') + 1;
    } else {
        var i = $(this).val().lastIndexOf('/') + 1;
    }
    var fileName = $(this).val().slice(i);
	  $('#loadfile2').html(fileName);

});


$( ".loadfile input" ).change(function() {
	
	
	
	 if ($(this).val().lastIndexOf('\\')) {
        var i = $(this).val().lastIndexOf('\\') + 1;
    } else {
        var i = $(this).val().lastIndexOf('/') + 1;
    }
    var fileName = $(this).val().slice(i);

	  $('#loadfile3').html(fileName);

});


$('.formgreyres').click(function() {

$('#loadfile3').html('Прикрепить файл');	
$('#idfile').val('');

$.cookie('idfile','', { expires: -7, path: '/'});

	
});




WidSize();

$(window).resize(function() {

WidSize();	
	
});

});

function WidSize()
{
	
// футер и хедер	
	
if ($(window).width()>1920)
{
	$('.footerc').width(1920);
	
	var  dd=13+($(window).width()-1920)/2;
	$('.cartheaderlist').css('right',dd+'px');
	
	
} else {
	$('.footerc').width($(window).width()-30);
	$('.cartheaderlist').css('right','13px');

}

// код уровнялки по высоте
// в каталоге (разделы)

var cr=1;	
if ($('.flex_val').find('.tit').length) {
var oo=$('.flex_val').find('.tit').eq(0).offset();
var pr=oo.top;
}
var maxc=0;
var hi=[];
var maxh=0;
var cc=0;

$('.flex_val').find('.tit').each(function(i,elem) {
	
var offset = $(this).offset();

	if ((offset.top>pr))
	{
			
	if (cc>maxc) maxc=cc;
    if (maxh>0) hi.push(maxh);
	
	if (cr>cc) cc=cr-1;
	
		cr=1;
		maxh=$(this).height();
		pr=offset.top;
		
	} else {
		cr=cr+1;

	if ($(this).height()>maxh) maxh=$(this).height();
		
	}
				
	
});

if (cr<cc) hi.push(maxh); 

console.log('cr:'+cr);
console.log('cc:'+cc);
console.log('maxh:'+maxh);
console.log('hi:'+hi);

var i=0;
var ii=0;
var j=0;


$.each(hi, function( key, value ) {
	
	for (ii=0; ii<cc; ii++) {
	
	id=key*(cc)+ii;
	
	console.log('id:'+id+' : heiht='+value);

 $('.flex_val').find('.tit').eq(id).height(value);	


	}


});

// на главной рекомендуемые 

// var maxh=0;
// var maxh2=0;

// $('.slick-list.draggable').each(function(i,elem) {
	
// $(this).find('.unitcard1').each(function(i2,elem2) {

// if ($(this).find('.tit').height()>maxh) maxh=$(this).find('.tit').height();
// if ($(this).find('.tit2').height()>maxh2) maxh2=$(this).find('.tit2').height();



	
// });

// console.log('Максимум: '+maxh+' максимум2: '+maxh2);

// });




// $('.slick-list.draggable').find('.unitcard1').find('.tit').height(maxh);
// $('.slick-list.draggable').find('.unitcard1').find('.tit2').height(maxh);


// Карточка товары


$('.tab-content').find('.tab-pane.fade.in.active').each(function(i,elem) {
	
	

var cr=1;	
if ($(this).find('.flex_val').find('.tit2').length) {
var oo=$(this).find('.flex_val').find('.tit2').eq(0).offset();
var pr=oo.top;
}
var maxc=0;
var hi=[];
var maxh=0;
var cc=0;

$(this).find('.flex_val').find('.tit2').each(function(i,elem) {
	
	
	
var offset = $(this).offset();

	if ((offset.top>pr))
	{
	
	
	if (cc>maxc) maxc=cc;
    if (maxh>0) hi.push(maxh);
	
	if (cr>cc) cc=cr-1;
	
		cr=1;
		maxh=$(this).height();
		console.log('eee'); 
		pr=offset.top;
		
	} else {
		cr=cr+1;

	if ($(this).height()>maxh) maxh=$(this).height();
		
	}
				
	
});

if (cr<cc) hi.push(maxh);

console.log('cr22:'+cr);
console.log('cc22:'+cc);
console.log('maxh22:'+maxh);
console.log('hi22:'+hi);

var i=0;
var ii=0;
var j=0;

var idp=$(this).attr('id');

$.each(hi, function( key, value ) {
	
	for (ii=0; ii<cc; ii++) {
	
	id=key*(cc)+ii;


// if (value>0) {
// 	$('#'+idp).find('.flex_val').find('.tit2').eq(id).height(value);	
// 	console.log('idp:'+idp+',id:'+id+' : heiht='+value);
// }



	}


});


});

}

$('#filedo').on('change', function(){
	files = this.files;
	
	
	
	
	
	
	
	// ничего не делаем если files пустой
	if( typeof files == 'undefined' ) return;

	// создадим объект данных формы
	var data = new FormData();

	// заполняем объект данных файлами в подходящем для отправки формате
	$.each( files, function( key, value ){
		data.append( key, value );
	});

	// добавим переменную для идентификации запроса
	data.append( 'my_file_upload', 1 );

	// AJAX запрос
	$.ajax({
		url         : '/local/ajax/file.php',
		type        : 'POST', // важно!
		data        : data,
		cache       : false,
		dataType    : 'json',
		// отключаем обработку передаваемых данных, пусть передаются как есть
		processData : false,
		// отключаем установку заголовка типа запроса. Так jQuery скажет серверу что это строковой запрос
		contentType : false, 
		// функция успешного ответа сервера
		success     : function( respond, status, jqXHR ){

			// ОК - файлы загружены
			if( typeof respond.error === 'undefined' ){
				// выведем пути загруженных файлов в блок '.ajax-reply'
				var files_path = respond.files;
				var html = '';
				$.each( files_path, function( key, val ){
				$('#idfile').val(val);
				} )

				
				// ;
			}
			// ошибка
			else {
				console.log('ОШИБКА: ' + respond.data );
			}
		},
		// функция ошибки ответа сервера
		error: function( jqXHR, status, errorThrown ){
			console.log( 'ОШИБКА AJAX запроса: ' + status, jqXHR );
		}

	});	
	
	
	
	
	
	
	
	
	
	
	
	
	
});
