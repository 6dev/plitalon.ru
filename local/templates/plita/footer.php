<? if (TYPEPAGE == 'text') { ?>
            </div>
            </div>
        </div>
    </section>
<? } ?>
<?
if ($_REQUEST['print'] != 'Y') { ?>
    <section class="navigate botnav">
        <div class="navigate-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "bottom",
                            array(
                                "ROOT_MENU_TYPE" => "bottom",
                                "MAX_LEVEL" => "1",
                                "CHILD_MENU_TYPE" => "bottom",
                                "USE_EXT" => "Y",
                                "DELAY" => "N",
                                "ALLOW_MULTI_SELECT" => "Y",
                                "MENU_CACHE_TYPE" => "A",
                                "MENU_CACHE_TIME" => "3600000",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "MENU_CACHE_GET_VARS" => array(),
                                "COMPONENT_TEMPLATE" => "bottom",
                                "COMPOSITE_FRAME_MODE" => "A",
                                "COMPOSITE_FRAME_TYPE" => "AUTO"
                            ),
                            false
                        ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="footer">
        <? $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "services",
            array(
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "AJAX_MODE" => "N",
                "IBLOCK_TYPE" => "main",
                "IBLOCK_ID" => "20",
                "NEWS_COUNT" => "4",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_ORDER1" => "DESC",
                "SORT_BY2" => "ID",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "arMain",
                "FIELD_CODE" => array(
                    0 => "ID",
                    1 => "DETAIL_TEXT",
                    2 => "DETAIL_PICTURE",
                    3 => "",
                ),
                "PROPERTY_CODE" => array(
                    0 => "",
                    1 => "DESCRIPTION",
                    2 => "",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_BROWSER_TITLE" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_META_DESCRIPTION" => "Y",
                "SET_LAST_MODIFIED" => "Y",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600000",
                "CACHE_FILTER" => "Y",
                "CACHE_GROUPS" => "Y",
                "DISPLAY_TOP_PAGER" => "Y",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "Y",
                "PAGER_TEMPLATE" => "",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "Y",
                "PAGER_BASE_LINK_ENABLE" => "Y",
                "SET_STATUS_404" => "Y",
                "SHOW_404" => "Y",
                "MESSAGE_404" => "",
                "PAGER_BASE_LINK" => "",
                "PAGER_PARAMS_NAME" => "arrPager",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "N",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "COMPONENT_TEMPLATE" => "services",
                "STRICT_SECTION_CHECK" => "N",
                "FILE_404" => "/404.php",
                "COMPOSITE_FRAME_MODE" => "A",
                "COMPOSITE_FRAME_TYPE" => "AUTO"
            ),
            false
        ); ?>
        <div class="f2">
            <div class="container-fluid footerc">
                <div class="flex">
                    <div class="leftf">
                        <p class="addr">
                            <?
                            ob_start();
                            // включаемая область для раздела
                            include($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/includes/phone.php");

                            $d = ob_get_contents();

                            ob_end_clean();


                            ?>
                            <?
                            $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/includes/addr.php", array(), array(
                                "MODE" => "php",                                           // будет редактировать в веб-редакторе
                                "NAME" => "Адрес",      // текст всплывающей подсказки на иконке
                            ));; ?>
                        </p>
                        <p class="phone">
                            <a href="tel:<?= str_replace('-', '', $d); ?>"><?
                                $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/includes/phone.php", array(), array(
                                    "MODE" => "php",                                           // будет редактировать в веб-редакторе
                                    "NAME" => "Телефон",      // текст всплывающей подсказки на иконке
                                ));; ?></a>
                        </p>
                        <p class="email">
                            <?
                            // включаемая область для раздела
                            $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/includes/email.php", array(), array(
                                "MODE" => "html",                                           // будет редактировать в веб-редакторе
                                "NAME" => "Email",      // текст всплывающей подсказки на иконке
                            ));
                            ?>
                        </p>
                        <p class="fmap">
                            <?
                            // включаемая область для раздела
                            $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/includes/maps.php", array(), array(
                                "MODE" => "html",                                           // будет редактировать в веб-редакторе
                                "NAME" => "Карта сайта",      // текст всплывающей подсказки на иконке
                            ));
                            ?>
                        </p>
                    </div>
                    <div class="logo">
                        <?
                        $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/includes/logo.php", array(), array(
                            "MODE" => "php",                                           // будет редактировать в веб-редакторе
                            "NAME" => "Логотип",      // текст всплывающей подсказки на иконке
                        ));; ?>
                        <?
                        include($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/includes/include.php"); ?>
                    </div>
                    <div class="fright">
                        <div class="social">
                            <? $APPLICATION->IncludeComponent("bitrix:menu", "social", array(
                                    "ROOT_MENU_TYPE" => "social",
                                    "MAX_LEVEL" => "1",
                                    "CHILD_MENU_TYPE" => "social",
                                    "USE_EXT" => "Y",
                                    "DELAY" => "N",
                                    "ALLOW_MULTI_SELECT" => "Y",
                                    "MENU_CACHE_TYPE" => "N",
                                    "MENU_CACHE_TIME" => "3600",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "MENU_CACHE_GET_VARS" => ""
                                )
                            ); ?>
                        </div>
                        <p>
                            <?
                            // включаемая область для раздела
                            $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/includes/pol.php", array(), array(
                                "MODE" => "html",                                           // будет редактировать в веб-редакторе
                                "NAME" => "Политика конфиденциальности",      // текст всплывающей подсказки на иконке
                            ));
                            ?>
                        </p>
                        <p class="last-p">
                            <?
                            // включаемая область для раздела
                            $APPLICATION->IncludeFile(SITE_TEMPLATE_PATH . "/includes/of.php", array(), array(
                                "MODE" => "html",                                           // будет редактировать в веб-редакторе
                                "NAME" => "Политика конфиденциальности",      // текст всплывающей подсказки на иконке
                            ));
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
<? } ?>
<? if ($_REQUEST['print'] != 'Y') { ?>
    <div class="modal fade" id="greate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body clearfix">
                    <div class="gratefull text-center">
                        <div class="img">
                            <img src="<?= SITE_TEMPLATE_PATH; ?>/images/greate.png" alt="">
                        </div>
                        <p class="tit">
                            Товар добавлен в корзину
                        </p>
                        <p class="text" id="nametovar">

                        </p>
                        <div class="col-md-6">
                            <button class="btn btnle" data-dismiss="modal">Продолжить покупки</button>
                        </div>
                        <div class="col-md-6">
                            <a href="/basket/" class="btn btnre">Перейти в корзину</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="send" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body clearfix">
                    <div class="gratefull text-center">
                        <p class="tit">Обратный звонок</p>
                        <p class="text" id="nametovar"></p>
                        <div class="col-md-12">
                            <form class="form" method="POST" onsubmit="return recall_ajax();" id="recall_ajax_form">
                                <label for="u_phone">*Телефон</label>
                                <input type="text" name="user_phone" required="" id="u_phone">
                                <div id="user_phone_error" class="error_message col-xs-12" style="color: red;"></div>

                                <input type="text" name="user_name" style="display: none;">
                                <div id="user_name_error" class="error_message col-xs-12"></div>

                                <input type="submit" name="contact" value="Отправить" id="submit_recall"
                                       class="btnformsend">

                            </form>
                            <div style="display:none;" id="recall_success" class="success_block col-xs-12">
              <span class="success_message" style="text-align:center">
                <? echo "Успешно отправленно" ?>
              </span>
                            </div>
                            <script>
                                function recall_show() {
                                    $('input[name="user_phone"]').val('');
                                    $('input[name="user_name"]').val('');
                                    return false;
                                }

                                function show_message_recall(id_message, message) {
                                    $('#' + id_message + '_error').html(message).show();
                                    $("#" + id_message).focus();
                                    $("#" + id_message).addClass('input_error');
                                    return false;
                                }

                                function recall_ajax() {
                                    var form_data = $('#recall_ajax_form').serialize();
                                    $('#submit_recall').hide();
                                    $.ajax({
                                        type: "POST",
                                        url: "/local/templates/plita/call.php",
                                        data: form_data,
                                        dataType: 'json',
                                        success: function (json) {
                                            $('#load_recall').hide();
                                            $('#submit_recall').show();
                                            $('.recall_input').removeClass('input_error');
                                            $('.error_message').html('').hide();
                                            switch (json['result']) {
                                                case 'success':
                                                    $('#recall_message2').hide();
                                                    $('#recall_ajax_form').hide();
                                                    $('#recall_success').show();
                                                    recall_show();
                                                    break;
                                                case 'error':
                                                    $.each(json['errors'],
                                                        function (index, value) {
                                                            show_message_recall(index, value);
                                                        });

                                                    break;
                                            }
                                        }
                                    });
                                    return false;
                                }
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <? if ($_REQUEST['add2'] != "") {

        $stok = "<a href='" . $_SERVER['HTTP_REFERER'] . "'>" . $_SERVER['HTTP_REFERER'] . "</a>";

        $arEventFields = array(
            "NAME" => $_REQUEST['name'],
            "PHONE" => $_REQUEST['phone'],
            "URL" => $_REQUEST['url'],
            "ISTOK" => $stok
        );
        $uploaddir = $_SERVER["DOCUMENT_ROOT"] . '/upload/files/';
        $fname = time() . '_' . basename($_FILES['userfile']['name']);
        $uploadfile = $uploaddir . $fname;
        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
            $f = 'http://' . str_replace(':80', '', $_SERVER['HTTP_HOST']) . '/upload/files/' . $fname;
            $arEventFields['FILE'] = "Ссылка на файл:<br> <a href='" . $f . "'>" . $f . "</a>";
        }
        CEvent::Send("ZAYVKA2", 's1', $arEventFields);
    }
    ?>


    <div class="modal fade" id="searchchep" tabindex="-1" role="dialog" aria-labelledby="searchchep">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body clearfix">
                    <button type="button" class="close" data-dismiss="modal"></button>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="contentwr">
                                <p class="tit">
                                    вы где-то видели такой же товар дешевле?
                                    сообщите нам и мы сделаем еще дешевле.
                                </p>
                                <p class="tit2">заявка на скидку</p>
                                <form action="" method="POST" enctype="multipart/form-data">
                                    <label for="contentwr-name">Ваше имя</label>
                                    <input id="contentwr-name" type="text" name="name">
                                    <input type="hidden" name="add2" value='ok'>
                                    <label for="contentwr-phone">Телефон</label>
                                    <input id="contentwr-phone" type="text" name="phone">
                                    <label for="contentwr-url">Ссылка на сайт, где этот товар дешевле:</label>
                                    <input id="contentwr-url" type="text" name="url">
                                    <span>
                                        Прикрепите фото ценника или прайса на этот товар с более низкой ценой
                                    </span>


                                    <label for="loadfile" class="loadfile">
                                        <input id="loadfile" type="file" name="userfile">
                                        <i id="loadfile2">Прикрепить файл</i>
                                    </label>


                                    <button class="btnsubm">
                                        Отправить
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<? } ?>
</body>
</html>