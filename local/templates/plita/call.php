<?
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
 	
 	use Bitrix\Main\Mail\Event;

$errors = array(); 
$json = array();
if(isset($_POST)){

$user_name = $_POST['user_name'];
$user_phones = $_POST['user_phone'];

	if(strlen($user_name) > 0){
		$errors['user_phone'] =  "Ошибка";
	}
 	if (strlen($user_phones) < 5) {
 		$errors['user_phone'] =  "Не верно указан телефон";
 	} 
 	if (count($errors)) {
 		$json['result'] = 'error';
 		$json['errors'] = $errors;
 	} else {	

		Event::send(array(
		    "EVENT_NAME" => "CALL",
		    "LID" => "s1",
		    "C_FIELDS" => array(
		        "USER_PHONE" => $user_phones
		    ),
		)); 

	  	$json['result'] = 'success';

  	}
} else {
	$errors['empty_errors'] =  "Заполните данные";
	$json['result'] = 'error';
 	$json['errors'] = $errors;
}
echo json_encode($json);
?>